SET DEFINE OFF;
ALTER SESSION SET PLSQL_WARNINGS='DISABLE:ALL';
set serveroutput on

--nothing needs to happen to roll back this app, as it's the initial deployment.
--@drop_stuff.sql;
--@delete_auth_stuff.sql;

PROMPT
PROMPT --------------- sql rollback complete ---------------
PROMPT
