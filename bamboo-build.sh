./gradlew --no-daemon build pushToUdeploy \
-Pmode=bamboo \
-PudeployAuthToken=$bamboo_ud_authToken \
-PremoteUrl=$GIT_URL \
-Dorg.gradle.java.home=$bamboo_jdk1_8_91 \
--stacktrace
