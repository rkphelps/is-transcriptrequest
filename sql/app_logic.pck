create or replace package ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC is

  -- Author  : RKPHELPS
  -- Created : 5/24/2018 10:17:40 AM
  -- Purpose : To handle the application logic for the apex application
  
  /**********************************************************************************************************************
  *  UPDATES
  *  -October 2018 by RKPHELPS. 
  *           -Made small changes to account for the minor tweaks to the app as requested by the RO.
  *           -See TKT0317316 for details, or the JIRA page: https://atlassian.liberty.edu/jira/secure/RapidBoard.jspa?rapidView=713&view=planning.nodetail Sprint 4
  * 
  *  -January/February 2019 by RKPHELPS.
  *           -added ability to print a bunch of transcripts at once directly from the APEX form. 
  *           -See TKT0389997 for details, or the JIRA Page: https://jira.os.liberty.edu/secure/RapidBoard.jspa?rapidView=713&projectKey=TR&view=detail&selectedIssue=TR-110
  **********************************************************************************************************************/

  
  -- Public type declarations
  TYPE    t_cost         IS RECORD(cost          ztranscript_request.requests.rq_cost%TYPE,
                                   standard_cost ztranscript_request.requests.rq_cost%TYPE,
                                   shipping_cost ztranscript_request.requests.rq_cost%TYPE,
                                   explanation   VARCHAR2(4000),
                                   formula       VARCHAR2(4000));
  SUBTYPE t_dm           IS ztranscript_request.delivery_method.dm_code%TYPE;
  SUBTYPE t_pidm         IS saturn.spriden.spriden_pidm%TYPE;
  SUBTYPE t_pk           IS INT;
  SUBTYPE t_status       IS ZTRANSCRIPT_REQUEST.statuses.ST_CODE%TYPE;
  SUBTYPE t_sbgi         IS saturn.stvsbgi.stvsbgi_code%TYPE;
  SUBTYPE t_city         IS saturn.sobsbgi.sobsbgi_city%TYPE;
  SUBTYPE t_stat         IS saturn.stvstat.stvstat_code%TYPE;
  SUBTYPE t_term         IS saturn.stvterm.stvterm_code%TYPE;

  /* October 2018 Updates: */
  SUBTYPE t_status_desc  IS ZTRANSCRIPT_REQUEST.statuses.ST_DESC%TYPE;
  SUBTYPE t_stu_name     IS VARCHAR2(200);
  SUBTYPE t_req_desc     IS ztranscript_request.requests.rq_description%TYPE;
  /* end October 2018 Updates */

  /* JanFeb 2019 Updates: */
  SUBTYPE t_printers     IS general.gtvprnt.gtvprnt_code%TYPE;
  SUBTYPE t_text_text    IS ztranscript_request.text.tx_text%TYPE;
  SUBTYPE t_text_numb    IS ztranscript_request.text.tx_number%TYPE;
  SUBTYPE t_tprt_code    IS saturn.stvtprt.stvtprt_code%TYPE;
  TYPE t_pk_rec          IS RECORD (pk t_pk);
  TYPE t_pk_tab          IS TABLE OF t_pk_rec;
  /* end JanFeb 2019 Updates */
  
  -- Public constant declarations
  c_log_level_threshold       CONSTANT INT := CASE WHEN SYSDATE < to_date('04/01/2019','MM/DD/YYYY') THEN 100 ELSE 10 END;        /*threshold for what to delete. Entries above this level will be deleted (but not entries at this level)*/  
                                                        /*any levels less than the log level threshold will be logged. 
                                                          errors are 10
                                                          functions and procedures are 30
                                                          sub-processes of functions and procedures are 40
                                                          page processes are 60
                                                          page loads are 80*/
  c_delete_threshold_log_days CONSTANT INT := 28;        /*logs beyond this amount of days will be deleted if they are above the delete threshold */
  c_delete_log_threshold      CONSTANT INT := 10; 
  --October 2018 Updates: added c_delete_log_days as a constant 
  c_delete_log_days           CONSTANT INT := 150;       /* logs beyond this amount will be deleted, period, no exceptions */

  /**********************************************************************************************************************
  * procedure that sets a user as the one processing the request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_claim_request (p_rq_id IN t_pk);
       
  /**********************************************************************************************************************
  * procedure that allows the user to claim requests (so that multiple people don't process the same requests
  * 
  **********************************************************************************************************************/
  PROCEDURE p_claim_requests;
     
  /**********************************************************************************************************************
  * procedure that sets the user's data in the base table
  * 
  **********************************************************************************************************************/
  PROCEDURE p_create_update_request (p_pidm     IN     t_pidm,
                                     p_desc     IN OUT t_req_desc,
                                     p_email    IN     VARCHAR2 DEFAULT NULL,
                                     p_comments IN     VARCHAR2 DEFAULT NULL,
                                     p_pk       IN OUT t_pk);
       
  /**********************************************************************************************************************
  * procedure that allows a user to mark multiple requests as completed
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_complete_requests;
      
  /**********************************************************************************************************************
  * procedure that deletes old logs that are no longer needed (anything above the deleting threshold and past the days, see constants)
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_old_logs;
     
  /**********************************************************************************************************************
  * procedure that deletes a specific request
  * October 2018 Updates: edited this to delete documents and to catch all old requests (was only catching some)
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_request(p_rq_id IN t_pk);
     
  /**********************************************************************************************************************
  * procedure that deletes any requests that are unpaid and are older than the amount of days marked in the form.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_unpaid;
   
  /**********************************************************************************************************************
  * procedure that finds the description of a request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_find_req_description (p_rq_id IN t_pk);
   
  /**********************************************************************************************************************
  * function that gets the address for the user. used on page 15 if the user wants to use their listed address to pay
  * 
  **********************************************************************************************************************/
  PROCEDURE p_get_address (p_rq_id IN t_pk);
       
  /**********************************************************************************************************************
  * procedure that logs actions as they happen
  * 
  **********************************************************************************************************************/
  PROCEDURE p_log_action (p_action IN VARCHAR2, 
                          p_level  IN INT);
  /**********************************************************************************************************************
  * procedure that allows a user to mark multiple requests as waiting for pickup
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_mark_requests_for_pickup;
    
  /**********************************************************************************************************************
  * procedure that marks a record as having paid for the transcript.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_mark_upay_completion (upay_json IN  VARCHAR2,
                                    results   OUT VARCHAR2);
   
  /**********************************************************************************************************************
  * procedure that sets the cost of a request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_payment_prep (p_rq_id IN t_pk);
  
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: added
  * this allows printing transcripts using a baseline Banner job
  * 
  **********************************************************************************************************************/
  PROCEDURE p_print_transcripts (p_printer     IN  t_printers,
                                 p_req_id      IN  t_pk DEFAULT NULL);
                                 
  /**********************************************************************************************************************
  * procedure that saves comments
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_save_comments (p_rq_id       IN t_pk,
                             p_rq_comments IN ztranscript_request.requests.rq_comments%TYPE);
 
  /**********************************************************************************************************************
  * procedure that sets the city/state/nation for US zip codes when the zip code is entered
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_city_state (p_zip_code IN ztranscript_request.destinations.ds_addr_zip%TYPE,
                              p_city     IN ztranscript_request.destinations.ds_addr_city%TYPE,
                              p_state    IN ztranscript_request.destinations.ds_addr_stat_code%TYPE,
                              p_natn     IN ztranscript_request.destinations.ds_addr_natn_code%TYPE);
                              
  /**********************************************************************************************************************
  * procedure that sets the address when a college is selected from the list provided
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_coll_addr (p_stat_code IN t_stat,
                             p_sbgi_code IN t_sbgi);

  /**********************************************************************************************************************
  * procedure that sets the cost, explanation of the cost, and the formula to calculate the cost
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_cost (p_rq_id     IN t_pk,
                        p_cost_item IN VARCHAR2,
                        p_expl_item IN VARCHAR2,
                        p_math_item IN VARCHAR2);

  /**********************************************************************************************************************
  * procedure that sets the text items throughout the form
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_text (p_page IN INT);

  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * procedure that updates the status of printed requests from In Processing (PC) to Completed (CM)
  * after the allotted amount of time.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_update_printed_PC_to_CM;
  
  /**********************************************************************************************************************
  * procedure that updates the status of a single request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_update_status (p_pk          IN t_pk,
                             p_new_status  IN t_status,
                             p_ro_comments IN VARCHAR2);

  /**********************************************************************************************************************
  * procedure that uploads the attachment for the destination
  * 
  **********************************************************************************************************************/
  PROCEDURE p_upload_attachment (p_ds_id              IN  t_pk,
                                 p_doc_upload         IN  VARCHAR2,
                                 p_ds_doc_id          OUT INT,
                                 p_doc_status         OUT VARCHAR2,
                                 p_doc_status_message OUT VARCHAR2);
  
  /**********************************************************************************************************************
  * Added during October 2018 Updates
  * Function that calculates whether the exp_ship_ind should be able to be selected. 
  * If so, set the value that was selected. 
  * Otherwise, null.
  * 
  **********************************************************************************************************************/
  FUNCTION f_calculate_exp_ship_ind (p_dm_code      IN t_dm, 
                                     p_exp_ship_ind IN VARCHAR2) RETURN VARCHAR2;

  /**********************************************************************************************************************
  * function that determines if this request can be edited.
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_edit (p_pk IN t_pk DEFAULT NULL) RETURN VARCHAR2;
  
  /**********************************************************************************************************************
  * function that determines if the cost can be edited
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_edit_cost (p_rq_id IN t_pk) RETURN VARCHAR2;
     
  /**********************************************************************************************************************
  * function that determines if a student can order their transcripts
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_request (p_pidm IN t_pidm) RETURN VARCHAR2;
    
  /**********************************************************************************************************************
  * determines what the current cost of a request is (used to see if it's being changed)
  * procedure that determines if the cost for this request has been changed (only the RO has permissions to override/change, in which case it locks for the form for that record).
  * if so, it marks the "cost Override" indicator in the table.
  * 
  **********************************************************************************************************************/
  FUNCTION f_check_cost_update (p_rq_id    IN t_pk, 
                                p_new_cost IN ztranscript_request.requests.rq_cost%TYPE) RETURN VARCHAR2;

  /**********************************************************************************************************************
  * function that determines whether or not the college address fields should be editable
  *
  **********************************************************************************************************************/
  FUNCTION f_coll_addr_condit (p_ds_id        IN t_pk, 
                               p_part_of_addr IN VARCHAR2) RETURN BOOLEAN;    
  
  /**********************************************************************************************************************
  * function that determines if the cost for this request has been overridden 
  * (only the RO has permissions to override, and only before payment occurs, in which case it locks for the form for that record.
  * 
  **********************************************************************************************************************/
  FUNCTION f_cost_overridden (p_rq_id IN t_pk) RETURN VARCHAR2;
    
  /**********************************************************************************************************************
  * function that determines what the next sequence number should be for the request
  * 
  **********************************************************************************************************************/
  FUNCTION f_destination_seq_no (p_pk IN t_pk) RETURN INT;
  
  /**********************************************************************************************************************
  * function that determines hide/show conditions for items on the add destinations form page (5)
  * 
  **********************************************************************************************************************/
  FUNCTION f_dest_page_item_condit (p_dm_code     IN t_dm, 
                                    p_condit_type IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN;
  
  /**********************************************************************************************************************
  * function that determines how many copies (if there is a set number) for a specific type of delivery method
  * 
  **********************************************************************************************************************/
  FUNCTION f_dm_code_copies (p_dm_code IN t_dm) RETURN INT;

  /**********************************************************************************************************************
  * function that determines if a user doesn't have to pay for their transcripts
  * 
  **********************************************************************************************************************/
  FUNCTION f_fee_waiver (p_pidm IN t_pidm) RETURN VARCHAR2;
      
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if a student currently has an MD hold on their account. 
  * 
  **********************************************************************************************************************/
  FUNCTION f_has_md_hold (p_pidm  IN  t_pidm) RETURN VARCHAR2;
      
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if sending to this destination is available via escrip.
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_escrip (p_sbgi_code IN  t_sbgi DEFAULT NULL,
                        p_ds_id     IN  t_pk   DEFAULT NULL) RETURN VARCHAR2; 
                          
  /**********************************************************************************************************************
  * function (mainly for authorization) that determines if someone has ever been a student
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_student (p_user IN VARCHAR2) RETURN VARCHAR2;
       
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if the student entered their own e-mail address
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_users_email (p_entered_email  IN general.goremal.goremal_email_address%TYPE)
                             RETURN VARCHAR2;

  /**********************************************************************************************************************
  * function that determines if a document upload is a .pdf (which it must be)
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_valid_file (p_doc_upload IN ZAPEX.WWV_FLOW_FILE_OBJECTS$.NAME%TYPE) RETURN BOOLEAN;
        
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that returns a pidm based on an ID number
  * 
  **********************************************************************************************************************/
  FUNCTION f_lookup_pidm (p_lu_id  IN VARCHAR2) RETURN t_pidm;
        
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that returns a name based on pidm
  * 
  **********************************************************************************************************************/
  FUNCTION f_lookup_name (p_pidm  IN t_pidm, 
                          p_type  IN VARCHAR2 DEFAULT 'FULLNM') RETURN VARCHAR2;
   
  /**********************************************************************************************************************
  * function that determines how much a student needs to pay for their transcripts
  * 
  **********************************************************************************************************************/
  FUNCTION f_payment_amount (p_pk IN t_pk) RETURN t_cost;

  /**********************************************************************************************************************
  * JanFeb 2019 Updates: added
  * this creates a table from all the request IDs selected on the Processing page so they can be queried in a cursor.
  * 
  **********************************************************************************************************************/
  FUNCTION f_printing_prep_req_ids (p_rq_id  IN  t_pk DEFAULT NULL) RETURN t_pk_tab PIPELINED;
  
  /**********************************************************************************************************************
  * function that determines if the continue button on page 4 should show
  * 
  **********************************************************************************************************************/
  FUNCTION f_ready_to_pay (p_rq_id IN t_pk) RETURN BOOLEAN;
    
  /**********************************************************************************************************************
  * function that finds how many (if any) destinations a request has
  * 
  **********************************************************************************************************************/
  FUNCTION f_req_ttl_destinations (p_pk IN t_pk) RETURN INT;

  /**********************************************************************************************************************
  * function that determines if the address entered for the destination is not a PO Box or an APO address (if it's a standard address)
  * 
  **********************************************************************************************************************/
  FUNCTION f_standard_address (p_dm_code IN t_dm,
                               p_addr_line1 IN VARCHAR2,
                               p_addr_line2 IN VARCHAR2) RETURN VARCHAR2;
                                   
  /**********************************************************************************************************************
  * function that finds how many (if any) requests a student has
  * 
  **********************************************************************************************************************/
  FUNCTION f_stu_ttl_requests (p_pidm IN t_pidm) RETURN INT;
  
  /**********************************************************************************************************************
  * procedure that ensures everything is submitted correctly in the destination form (apex 1214, page 5)
  * 
  **********************************************************************************************************************/
  FUNCTION f_submit_destination (p_message_out OUT VARCHAR2) RETURN BOOLEAN;
  
  /**********************************************************************************************************************
  * function that retrieves the tprt_code for this destination
  * added in JanFeb 2019 updates for printing
  * 
  **********************************************************************************************************************/
  FUNCTION f_tprt_code (p_ds_id   IN  t_pk) return t_tprt_code;                   
end APEX_APP_LOGIC;
/
create or replace package body ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC is
  
  --type declaration
  TYPE t_college_record IS RECORD(    --taken from ZLUWEBAPP.college_record_type and cleaned up quite a bit
       ceeb_code      saturn.stvsbgi.stvsbgi_code%TYPE,
       type_ind       saturn.stvsbgi.stvsbgi_type_ind%TYPE,
       srce_ind       saturn.stvsbgi.stvsbgi_srce_ind%TYPE,
       description    VARCHAR2(500),
       desc_extended  VARCHAR2(500),
       homeschool     zsaturn.szriext.szriext_hmsc_ind%TYPE,
       activity_date  saturn.stvsbgi.stvsbgi_activity_date%TYPE,
       admr_code      saturn.stvsbgi.stvsbgi_admr_code%TYPE,
       street_line1   saturn.sobsbgi.sobsbgi_street_line1%TYPE,
       street_line2   saturn.sobsbgi.sobsbgi_street_line2%TYPE,
       street_line3   saturn.sobsbgi.sobsbgi_street_line3%TYPE,
       street_line4   saturn.sobsbgi.sobsbgi_street_line4%TYPE,
       housenumber    saturn.sobsbgi.sobsbgi_house_number%TYPE,
       city           saturn.sobsbgi.sobsbgi_city%TYPE,
       stat_code      saturn.sobsbgi.sobsbgi_stat_code%TYPE,
       cnty_code      saturn.sobsbgi.sobsbgi_cnty_code%TYPE,
       zip            saturn.sobsbgi.sobsbgi_zip%TYPE,
       natn_code      saturn.sobsbgi.sobsbgi_natn_code%TYPE,
       alternate_name varchar2(100));
  TYPE t_dm_hide_show IS RECORD(      --record that stores all the information concerning what should show and hide based on the delivery method chosen
       emal ztranscript_request.delivery_method.dm_email_ind%TYPE,
       coll ztranscript_request.delivery_method.dm_college_ind%TYPE,
       addr ztranscript_request.delivery_method.dm_address_ind%TYPE,
       cmmt ztranscript_request.delivery_method.dm_comments_ind%TYPE,
       atch ztranscript_request.delivery_method.dm_attachment_ind%TYPE,
       exsp ztranscript_request.delivery_method.dm_exp_ship_ind%TYPE,
       escp ztranscript_request.delivery_method.dm_escrip_ind%TYPE,
       cpys VARCHAR2(1));             --this isn't in the table, but it is always required (at least for now)
       
  c_data_origin   CONSTANT  VARCHAR2(50) := 'ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC';
  
  /**/-------------------------------------------------------------------------------------------------------------------
  /**/---------------------------------------PRIVATE PROCEDURES AND FUNCTIONS--------------------------------------------
  /**/-------------------------------------------------------------------------------------------------------------------
  /**/ 
  /**/ /*****************************************************************************************************************
  /**/ /* private procedure that logs errors
  /**/ /* 
  /**/ /*****************************************************************************************************************/
  /**/ PROCEDURE p_log_error (p_object_params VARCHAR2) IS
  /**/   
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action(p_object_params || ' resulted in an error: ' || SQLERRM || '. This occured on page ' || v('APP_PAGE_ID') || '. ', 10);
  /**/   EXCEPTION
  /**/     WHEN OTHERS 
  /**/       THEN p_log_action(p_object_params || ' resulted in an error: ' || SQLERRM || '. ', 10);
  /**/            raise_application_error(-20000,SQLERRM);
  /**/ END p_log_error;
  /**/ 
  /**/ /*****************************************************************************************************************
  /**/ /* private procedure that deletes any requests that are unpaid and are older than the amount of days specified
  /**/ /* 
  /**/ /****************************************************************************************************************/
  /**/ PROCEDURE p_delete_unpaid_days (p_days INT) IS 
  /**/   CURSOR c_deletions IS SELECT zz.rq_id rq_id,
  /**/                                zz.pidm pidm,
  /**/                                zz.seq seq,
  /**/                                zz.descr descr,
  /**/                                zz.crtd crtd
  /**/                           FROM (SELECT rq.rq_id rq_id,
  /**/                                        rq.rq_pidm pidm,
  /**/                                        rq.rq_seq_no seq,
  /**/                                        rq.rq_description descr,
  /**/                                        rq.rq_created_on crtd,
  /**/                                        greatest(COALESCE(rq.rq_modified_on,rq.rq_created_on,to_date('08/01/2018','MM/DD/YYYY')),COALESCE(MAX(ds.ds_modified_on),MAX(ds.ds_created_on),to_date('08/01/2018','MM/DD/YYYY'))) last_act  /*this finds the last activity on the request. October 2018 Updates: The final argument in the coalesce is the date the form went live. If there's a null value, this date will be returned and the greatest function can appropriately find the most recent date something was edited instead of returning null.*/
  /**/                                   FROM ztranscript_request.requests rq
  /**/                                   JOIN ztranscript_request.statuses st
  /**/                                     ON st.st_code = rq.rq_st_code
  /**/                                    AND st.st_delete_ind = 'Y'
  /**/                                   LEFT JOIN ztranscript_request.destinations ds
  /**/                                     ON ds.ds_rq_id = rq.rq_id
  /**/                                  GROUP BY rq.rq_id,
  /**/                                           rq.rq_pidm,
  /**/                                           rq.rq_seq_no,
  /**/                                           rq.rq_description,
  /**/                                           rq.rq_created_on,
  /**/                                           rq.rq_modified_on,
  /**/                                           rq.rq_created_on
  /**/                                ) zz
  /**/                          WHERE zz.last_act <= (SYSDATE - p_days);
  /**/   r_deletions   c_deletions%ROWTYPE;
  /**/   ttl_deletions INT := 0;
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('p_delete_unpaid_days(p_days => ' || p_days || ')', 30);
  /**/ --loop through the records
  /**/   FOR r_deletions IN c_deletions
  /**/     LOOP
  /**/     ttl_deletions := ttl_deletions + 1;
  /**/     --log a deletion for each record
  /**/       p_log_action('record deleted. Rd_id was '        || r_deletions.rq_id ||
  /**/                                  '. Pidm was '         || r_deletions.pidm  ||
  /**/                                  '. Seq_no was '       || r_deletions.seq   ||
  /**/                                  '. Description was '  || r_deletions.descr ||
  /**/                                  '. Created date was ' || r_deletions.crtd  || '.', 40);
  /**/     --delete each record
  /**/       p_delete_request(p_rq_id => r_deletions.rq_id);
  /**/     END LOOP;  
  /**/   p_log_action('Total Requests Deleted: ' || ttl_deletions, 40);
  /**/   EXCEPTION 
  /**/     WHEN OTHERS
  /**/       THEN p_log_error('p_delete_unpaid_days(p_days => ' || p_days || ')');
  /**/            RAISE;
  /**/ END p_delete_unpaid_days;
  /**/       
  /**/ /**********************************************************************************************************************
  /**/ /* JanFeb 2019 Updates: added
  /**/ /* private procedure that inserts a record into the prints table, indicating something is about to be printed.
  /**/ /* 
  /**/ /**********************************************************************************************************************/
  /**/ PROCEDURE p_prints_insert (p_prnt_pk  OUT t_pk) IS 
  /**/
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('p_prints_insert(p_prnt_pk => ' || p_prnt_pk || ')', 30);
  /**/ -- insert the record and return the pk
  /**/   INSERT INTO ztranscript_request.prints (prnt_user) VALUES (v('APP_USER'))
  /**/   RETURNING prnt_id INTO p_prnt_pk;
  /**/ EXCEPTION WHEN OTHERS THEN 
  /**/   p_log_error('p_prints_insert(p_prnt_pk => ' || p_prnt_pk || ')');
  /**/   RAISE;
  /**/ END;
  /**/           
  /**/ /**********************************************************************************************************************
  /**/ /* JanFeb 2019 Updates: added
  /**/ /* private procedure that update a record into the prints table with notes about what happened. 
  /**/ /* 
  /**/ /**********************************************************************************************************************/
  /**/ PROCEDURE p_prints_update (p_prnt_pk  IN  t_pk,
  /**/                            p_notes    IN  VARCHAR2) IS 
  /**/
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('p_prints_update(p_prnt_pk => ' || p_prnt_pk || ', ' ||
  /**/                                'p_notes => '   || p_notes   || ')', 30);
  /**/ -- update the record
  /**/   UPDATE ztranscript_request.prints p
  /**/      SET p.prnt_notes = p_notes
  /**/    WHERE p.prnt_id = p_prnt_pk;
  /**/ EXCEPTION WHEN OTHERS THEN 
  /**/   p_log_error('p_prints_update(p_prnt_pk => ' || p_prnt_pk || ', ' ||
  /**/                                'p_notes => '   || p_notes   || ')');
  /**/   RAISE;
  /**/ END;
  /**/  
  /**/ /**********************************************************************************************************************
  /**/ /* private procedure that sends an e-mail to the student updating them on the status of their request.
  /**/ /* 
  /**/ /**********************************************************************************************************************/
  /**/ PROCEDURE p_send_email (p_pk IN t_pk, 
  /**/                         p_status IN t_status) IS 
  /**/   v_to           zcampent.zcrmque.zcrmque_to_email_address%TYPE;
  /**/   v_fname        saturn.spriden.spriden_first_name%TYPE;
  /**/   v_lname        saturn.spriden.spriden_last_name%TYPE;
  /**/   v_lu_id        saturn.spriden.spriden_id%TYPE;
  /**/   v_pidm         t_pidm;
  /**/   v_subject      zcampent.zcrmque.zcrmque_subject%TYPE;
  /**/   v_display_text zcampent.zcrmque.zcrmque_clob1%TYPE;
  /**/   v_already_sent INT;   --1 means a record was found that indicates this has already been sent; 0 indicates it hasn't been sent.
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('p_send_email(p_pk => '     || p_pk     ||
  /**/                           ' ,p_status => ' || p_status || ')', 30);
  /**/ --calculate the variables needed for the insert
  /**/   --subject line for the e-mail
  /**/   BEGIN 
  /**/     SELECT 'Transcript Request (' || rqs.descr || ') - ' || txts.status,
  /**/            'Dear ' || rqs.fname || ',  <p>' || txts.text,
  /**/            rqs.fname,
  /**/            rqs.lname,
  /**/            rqs.lu_id,
  /**/            rqs.pidm,
  /**/            rqs.email
  /**/       INTO v_subject,
  /**/            v_display_text,
  /**/            v_fname,
  /**/            v_lname,
  /**/            v_lu_id,
  /**/            v_pidm,
  /**/            v_to
  /**/       FROM (SELECT rq.rq_description descr,
  /**/                    s.spriden_first_name fname,
  /**/                    s.spriden_last_name lname,
  /**/                    s.spriden_id lu_id,
  /**/                    rq.rq_pidm pidm,
  /**/                    rq.rq_email email
  /**/               FROM ztranscript_request.requests rq
  /**/               JOIN saturn.spriden s
  /**/                 ON s.spriden_pidm = rq.rq_pidm
  /**/                AND s.spriden_change_ind IS NULL
  /**/              WHERE rq.rq_id = p_pk
  /**/            ) rqs
  /**/      CROSS JOIN (SELECT st.st_desc status,
  /**/                         txt.tx_text text
  /**/                    FROM ztranscript_request.statuses st
  /**/                    JOIN ztranscript_request.text txt
  /**/                      ON txt.tx_item = st.st_code || '_EMAIL_TEXT'
  /**/                   WHERE st.st_code = p_status
  /**/                 ) txts;
  /**/   EXCEPTION
  /**/     WHEN NO_DATA_FOUND
  /**/       THEN p_log_action('p_send_email(p_pk => '     || p_pk     ||
  /**/                                   ' ,p_status => ' || p_status || ') resulted in an error: ' || SQLERRM, 10);
  /**/            raise_application_error(-20003, 'Looks like someone made a boo-boo. There is no text for the e-mail that should be sent. Please contact ADS or the Registrar''s Office so they can add in some text for this e-mail. ' || SQLERRM);
  /**/   END;
  /**/ --ensure the e-mail hasn't already been sent for this specific record
  /**/     SELECT COUNT('X')
  /**/       INTO v_already_sent
  /**/       FROM zcampent.zcrmque q
  /**/      WHERE q.zcrmque_id_num = 50116
  /**/        AND q.zcrmque_to_email_address = v_to
  /**/        AND q.zcrmque_from_email_address = 'registrar@liberty.edu'
  /**/        AND q.zcrmque_subject = v_subject;
  /**/   IF v_already_sent = 0
  /**/     THEN --insert into the e-mail tables
  /**/          --October 2018 Updates: changed e-mail addresses below from registrar@liberty.edu to lutranscripts@liberty.edu
  /**/          insert into zcampent.zcrmque (zcrmque_campaign_number, zcrmque_source, zcrmque_last_name, zcrmque_first_name,
  /**/                                        zcrmque_id, zcrmque_pidm, zcrmque_from_email_address, 
  /**/                                        zcrmque_to_email_address, zcrmque_date_added, zcrmque_sent_ind, zcrmque_log_comment_ind, 
  /**/                                        zcrmque_clob1, zcrmque_reply_to_addr,zcrmque_subject, zcrmque_from_name)
  /**/          values (50116, 'APEX_' || v('APP_ID') || '_' || v('APP_PAGE_ID'), v_lname, v_fname, v_lu_id, v_pidm,
  /**/                  'lutranscripts@liberty.edu', v_to, 
  /**/                  SYSDATE, 'N', 'N', v_display_text, 'lutranscripts@liberty.edu', v_subject, 'Registrar''s Office');
  /**/     ELSE p_log_action('E-mail not sent for "p_send_email(p_pk => '     || p_pk     ||
  /**/                                                       ' ,p_status => ' || p_status || ')" because this status e-mail has already been sent for this request.', 30);
  /**/   END IF;
  /**/   EXCEPTION
  /**/     WHEN OTHERS
  /**/       THEN p_log_error('p_send_email(p_pk => '     || p_pk     ||
  /**/                                   ' ,p_status => ' || p_status || ')');
  /**/            RAISE;
  /**/ END p_send_email;
  /**/ 
  /**/ /*****************************************************************************************************************
  /**/ /* private function that determines if a student has a transcript hold
  /**/ /* 
  /**/ /*****************************************************************************************************************/
  /**/ FUNCTION f_current_transcript_hold (p_pidm IN t_pidm) RETURN VARCHAR2 IS 
  /**/   v_hold_ind VARCHAR2(1);
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('f_current_transcript_hold (p_pidm => ' || p_pidm || ')', 30);
  /**/ --check if they have a transcript hold right now. If so, return 'Y'
  /**/   SELECT 'Y'
  /**/     INTO v_hold_ind
  /**/     FROM saturn.sprhold h
  /**/     -- added join to stvhldd during JanFeb 2019 Updates to properly check for transcript holds.
  /**/     JOIN saturn.stvhldd hldd
  /**/       ON hldd.stvhldd_code = h.sprhold_hldd_code
  /**/      AND hldd.stvhldd_trans_hold_ind = 'Y'
  /**/    WHERE h.sprhold_pidm = p_pidm
  /**/      --AND h.sprhold_hldd_code = 'NT' removed during JanFeb 2019 Updates; added join to stvhldd to check for all transcript holds
  /**/      AND h.sprhold_to_date > SYSDATE
  /**/    FETCH FIRST ROW ONLY;
  /**/   -- if the person has an MD hold, due to the form's logic, make sure that this returns Y. 
  /**/   -- It should catch an MD hold in the query above, but just in case.
  /**/   IF f_has_md_hold(p_pidm) = 'Y' THEN 
  /**/      v_hold_ind := 'Y';
  /**/   END IF;
  /**/ --log what's being returned and return it
  /**/   p_log_action('f_current_transcript_hold (p_pidm => ' || p_pidm || ') returned ' || v_hold_ind, 30);
  /**/   RETURN v_hold_ind;
  /**/ --if they don't have a transcript-related hold right now, they can order their transcripts. So if nothing was found, return 'N'
  /**/   EXCEPTION
  /**/     WHEN NO_DATA_FOUND
  /**/       THEN --log what's being returned and return it
  /**/            p_log_action('f_current_transcript_hold (p_pidm => ' || p_pidm || ') returned N', 30);
  /**/            RETURN 'N';
  /**/     WHEN OTHERS
  /**/       THEN p_log_error('f_current_transcript_hold (p_pidm => ' || p_pidm || ')');
  /**/            RAISE;
  /**/ END f_current_transcript_hold;
  /**/
  /**/ /*****************************************************************************************************************
  /**/ /* private function that finds a number value in the text table
  /**/ /*
  /**/ /*****************************************************************************************************************/
  /**/ FUNCTION f_get_text_number (p_item IN ztranscript_request.text.tx_item%TYPE) RETURN t_text_numb IS 
  /**/   v_numb t_text_numb;
  /**/ BEGIN
  /**/ --find the text item
  /**/   SELECT txt.tx_number
  /**/     INTO v_numb
  /**/     FROM ztranscript_request.text txt
  /**/    WHERE txt.tx_item = p_item;
  /**/ --log what's being returned and return is
  /**/   p_log_action('f_get_text_number (p_item => ' || p_item || ') returned ' || v_numb || '. ', 30);
  /**/   RETURN v_numb;
  /**/   EXCEPTION
  /**/     WHEN OTHERS 
  /**/       THEN p_log_error('f_get_text_number (p_item => ' || p_item || ')');
  /**/            RAISE;
  /**/ END f_get_text_number;
  /**/
  /**/ /*****************************************************************************************************************
  /**/ /* private function that finds a text value in the text table
  /**/ /*
  /**/ /*****************************************************************************************************************/
  /**/ FUNCTION f_get_text_varchar (p_item IN ztranscript_request.text.tx_item%TYPE) RETURN t_text_text IS 
  /**/   v_text t_text_text;
  /**/ BEGIN
  /**/ --find the text item
  /**/   SELECT txt.tx_text
  /**/     INTO v_text
  /**/     FROM ztranscript_request.text txt
  /**/    WHERE txt.tx_item = p_item;
  /**/ --log what's being returned and return is
  /**/   p_log_action('f_get_text_varchar (p_item => ' || p_item || ') returned ' || v_text || '. ', 30);
  /**/   RETURN v_text;
  /**/   EXCEPTION
  /**/     WHEN OTHERS 
  /**/       THEN p_log_error('f_get_text_varchar (p_item => ' || p_item || ')');
  /**/            RAISE;
  /**/ END f_get_text_varchar;
  /**/ 
  /**/ /*****************************************************************************************************************
  /**/ /* private function that determines all the hide/show scenarios for the destinations form
  /**/ /* 
  /**/ /*****************************************************************************************************************/
  /**/ FUNCTION f_dm_hide_show (p_dm_code IN t_dm) RETURN t_dm_hide_show IS 
  /**/   v_dm_hs_record t_dm_hide_show;
  /**/ BEGIN
  /**/ --lookup the values
  /**/   SELECT dm.dm_email_ind,
  /**/          dm.dm_college_ind,
  /**/          dm.dm_address_ind,
  /**/          dm.dm_comments_ind,
  /**/          dm.dm_attachment_ind,
  /**/          dm.dm_exp_ship_ind,
  /**/          dm.dm_escrip_ind,
  /**/          'Y' --copies are always required (for now)
  /**/     INTO v_dm_hs_record
  /**/     FROM ztranscript_request.delivery_method dm
  /**/    WHERE dm.dm_code = p_dm_code;
  /**/ --log and return
  /**/   p_log_action('f_dm_hide_show (p_dm_code => ' || p_dm_code || ') returned ' 
  /**/                                                             || v_dm_hs_record.emal || ' & ' 
  /**/                                                             || v_dm_hs_record.coll || ' & ' 
  /**/                                                             || v_dm_hs_record.addr || ' & ' 
  /**/                                                             || v_dm_hs_record.cmmt || ' & ' 
  /**/                                                             || v_dm_hs_record.atch || ' & ' 
  /**/                                                             || v_dm_hs_record.exsp || '.' , 40);
  /**/   RETURN v_dm_hs_record;
  /**/   EXCEPTION
  /**/     WHEN OTHERS
  /**/       THEN p_log_error('f_dm_hide_show (p_dm_code => ' || p_dm_code || ')');
  /**/            RAISE;
  /**/ END f_dm_hide_show;
  /**/ 
  /**/ /*****************************************************************************************************************
  /**/ /* private function that determines if a student has a grade for a class, or if they've registered.
  /**/ /* 
  /**/ /* when this function was created (5/30/2018) the registrar didn't process medical school or academy classes. This may change.
  /**/ /* --October 2018 Updates: check for a student who has registered for but not completed a course. renamed from f_completed_course to f_stu_has_course
  /**/ /*****************************************************************************************************************/
  /**/ FUNCTION f_stu_has_course (p_pidm IN t_pidm) RETURN VARCHAR2 IS   
  /**/   v_graded_courses       INT;
  /**/   v_reg_courses          INT;
  /**/   v_any_course_ind VARCHAR2(1);
  /**/ BEGIN
  /**/ --log
  /**/   p_log_action('f_stu_has_course (p_pidm => ' || p_pidm || ')', 30);
  /**/ --find out how many courses they have a grade for
  /**/   SELECT COUNT('X')
  /**/     INTO v_graded_courses
  /**/     FROM saturn.shrtckn ckn
  /**/     JOIN saturn.shrtckg ckg
  /**/       ON ckn.shrtckn_pidm = ckg.shrtckg_pidm
  /**/      AND ckn.shrtckn_term_code = ckg.shrtckg_term_code
  /**/      AND ckn.shrtckn_seq_no = ckg.shrtckg_tckn_seq_no
  /**/      AND ckg.shrtckg_seq_no = (SELECT MAX(ckg2.shrtckg_seq_no)
  /**/                                  FROM saturn.shrtckg ckg2
  /**/                                 WHERE ckg2.shrtckg_pidm = ckg.shrtckg_pidm
  /**/                                   AND ckg2.shrtckg_term_code = ckg.shrtckg_term_code
  /**/                                   AND ckg2.shrtckg_tckn_seq_no = ckg.shrtckg_tckn_seq_no)
  /**/    WHERE ckg.shrtckg_grde_code_final IS NOT NULL
  /**/      AND ckn.shrtckn_pidm = p_pidm;
  /**/ --figure out if they have a completed course. If so, set the return value as 'Y'
  /**/   IF v_graded_courses > 0 
  /**/      THEN v_any_course_ind := 'Y';
  /**/      ELSE SELECT COUNT('X')
  /**/             INTO v_reg_courses
  /**/             FROM saturn.sfrstcr cr
  /**/             JOIN saturn.stvrsts rsts
  /**/               ON rsts.stvrsts_code = cr.sfrstcr_rsts_code
  /**/              AND rsts.stvrsts_incl_assess = 'Y'
  /**/            WHERE cr.sfrstcr_pidm = p_pidm;
  /**/           IF v_reg_courses > 0
  /**/              THEN v_any_course_ind := 'Y';
  /**/              ELSE v_any_course_ind := 'N';
  /**/           END IF;
  /**/   END IF;
  /**/ --log the return and return it
  /**/   p_log_action('f_stu_has_course (p_pidm => ' || p_pidm || ') returned ' || v_any_course_ind, 40);
  /**/   RETURN v_any_course_ind;  
  /**/   EXCEPTION 
  /**/     WHEN OTHERS
  /**/       THEN p_log_error('f_stu_has_course (p_pidm => ' || p_pidm || ')');
  /**/            RAISE;
  /**/ END f_stu_has_course;
  /**/
  /**/-------------------------------------------------------------------------------------------------------------------
  /**/-----------------------------------END OF PRIVATE PROCEDURES AND FUNCTIONS-----------------------------------------
  /**/-------------------------------------------------------------------------------------------------------------------
  
  /**********************************************************************************************************************
  * procedure that sets a user as the one processing the request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_claim_request (p_rq_id IN t_pk) IS 
  
  BEGIN
  --log
    p_log_action('p_claim_request(p_rs_id => ' || p_rq_id || ')' , 30);
  --see if the record should be marked with processor 
  
    UPDATE ztranscript_request.requests rq
       SET rq.rq_processor = v('APP_USER'),
           rq.rq_modified_by = v('APP_USER'),
           rq.rq_modified_on = SYSDATE
     WHERE rq.rq_id = p_rq_id;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_claim_request(p_rs_id => ' || p_rq_id || ')');
             RAISE;
  END p_claim_request;
   
  /**********************************************************************************************************************
  * procedure that allows the user to claim requests (so that multiple people don't process the same requests
  * 
  **********************************************************************************************************************/
  PROCEDURE p_claim_requests IS
  
  BEGIN
  --log
    p_log_action('p_claim_requests', 30);
  --process the claim
    FOR i IN 1..APEX_APPLICATION.G_F01.COUNT 
      LOOP
      --log
        p_log_action('p_claim_requests procedure, record ' || APEX_APPLICATION.G_F01(i) || ' claimed by ' || v('APP_USER') || '.', 40);
      --claim the record
        p_claim_request(p_rq_id => APEX_APPLICATION.G_F01(i));
    END LOOP;
    EXCEPTION 
      WHEN OTHERS
        THEN p_log_error('p_claim_requests');
             RAISE;
  END p_claim_requests;
     
  /**********************************************************************************************************************
  * procedure that sets the user's data in the base table
  *
  * p_pidm - the pidm of the person making the request
  * p_pk   - the primary key that is returned in order to pass the value through to the add destations page (to enter into destinations)
  * 
  **********************************************************************************************************************/
  PROCEDURE p_create_update_request (p_pidm     IN     t_pidm,
                                     p_desc     IN OUT t_req_desc,
                                     p_email    IN     VARCHAR2 DEFAULT NULL,
                                     p_comments IN     VARCHAR2 DEFAULT NULL,
                                     p_pk       IN OUT t_pk) IS 
    v_seq_no         INT;
    v_prev_used_desc INT; 
    v_error_message  VARCHAR2(4000);
  BEGIN
  --log
    p_log_action('p_create_update_request(p_pidm => '     || p_pidm     || 
                                       ', p_desc => '     || p_desc     ||
                                       ', p_email => '    || p_email    ||
                                       ', p_comments => ' || p_comments ||
                                       ', p_pk => '       || p_pk       || ')', 30);   
  --find the sequence number to use and if this description has been used by this student before
    BEGIN
      SELECT MAX(rq_seq_no) + 1,
             COUNT(CASE WHEN rq_description = p_desc THEN 'X' ELSE NULL END)
        INTO v_seq_no,
             v_prev_used_desc
        FROM ztranscript_request.requests
       WHERE rq_pidm = p_pidm
         AND (rq_id != p_pk OR p_pk IS null);
    --if this description has been used before, just tack on the sequence number to it.
      IF v_prev_used_desc > 0
        THEN p_desc := p_desc || ' ' || v_seq_no;
      END IF;
    --set description 
      p_desc := nvl(p_desc, 'Request ' || nvl(v_seq_no, 1));
      EXCEPTION 
        WHEN OTHERS
          THEN p_log_error('p_create_update_request(p_pidm => '     || p_pidm     || 
                                                 ', p_desc => '     || p_desc     ||
                                                 ', p_email => '    || p_email    ||
                                                 ', p_comments => ' || p_comments ||
                                                 ', p_pk => '       || p_pk       || ')');
              RAISE;
    END;
    --see whether this is a new request (insert) or an old one (update)
    IF p_pk IS NULL
    --new request, so insert
      THEN --if the user can create a request
           IF f_can_request(p_pidm => p_pidm) = 'Y'
             --then create it
             THEN --create the new request
                  BEGIN
                    INSERT INTO ztranscript_request.requests (rq_pidm, rq_seq_no, rq_st_code, rq_description, rq_email, rq_comments, rq_data_origin, rq_created_by, rq_created_on)
                           VALUES (p_pidm, nvl(v_seq_no, 1), 'PP', p_desc, p_email, p_comments, 'APEX_' || v('APP_ID') || '_' || v('APP_PAGE_ID'), v('APP_USER'), SYSDATE)
                           RETURNING rq_id INTO p_pk;
                    EXCEPTION 
                      WHEN DUP_VAL_ON_INDEX
                        THEN v_error_message := f_get_text_varchar('DUPLICATE_DESC_MESSAGE');
                             p_log_action('p_create_update_request(p_pidm => '     || p_pidm     || 
                                                                ', p_desc => '     || p_desc     ||
                                                                ', p_email => '    || p_email    ||
                                                                ', p_comments => ' || p_comments ||
                                                                ', p_pk => '       || p_pk       || 
                                                                ') resulted in an error: raise_application_error (-20002, ' || v_error_message ||
                                                                ')', 40);
                             raise_application_error (-20002, v_error_message);
                  END;
             ELSE --find the error message
                  v_error_message:= f_get_text_varchar('P2_NO_REQUESTS_MESSAGE');
                  --log and show the error message
                  p_log_action('p_create_update_request(p_pidm => '     || p_pidm     || 
                                                     ', p_desc => '     || p_desc     ||
                                                     ', p_email => '    || p_email    ||
                                                     ', p_comments => ' || p_comments ||
                                                     ', p_pk => '       || p_pk       || 
                                                     ') resulted in an error: raise_application_error (-20002, ' || v_error_message ||
                                                     ')', 40);
                  raise_application_error (-20002, v_error_message);
           END IF;
    --old request, so update
      ELSE --if the user can edit their request
           IF f_can_edit(p_pk => p_pk) = 'Y'
             --then update it
             THEN UPDATE ztranscript_request.requests rq
                     SET rq.rq_description = p_desc,
                         rq.rq_email = p_email,
                         rq.rq_comments = p_comments
                   WHERE rq.rq_id = p_pk;
           END IF;
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_create_update_request(p_pidm => '     || p_pidm     || 
                                               ', p_desc => '     || p_desc     ||
                                               ', p_email => '    || p_email    ||
                                               ', p_comments => ' || p_comments ||
                                               ', p_pk => '       || p_pk       || ')');  
             RAISE;  
  END p_create_update_request;
       
  /**********************************************************************************************************************
  * procedure that allows a user to mark multiple requests as completed
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_complete_requests IS 
       
  BEGIN
  --log
    p_log_action('p_complete_requests', 30);
  --process the claim
    FOR i IN 1..APEX_APPLICATION.G_F01.COUNT 
      LOOP
      --log
        p_log_action('p_complete_requests procedure, record ' || APEX_APPLICATION.G_F01(i) || '.', 40);
      --claim the record
        p_update_status(p_pk          => APEX_APPLICATION.G_F01(i),
                        p_new_status  => 'CM',
                        p_ro_comments => NULL) ;
    END LOOP;
    EXCEPTION 
      WHEN OTHERS
        THEN p_log_error('p_complete_requests');
             RAISE;
  END p_complete_requests;
         
  /**********************************************************************************************************************
  * procedure that deletes old logs that are no longer needed (anything above the deleting threshold and past the days, see constants)
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_old_logs IS 
  
  BEGIN
  --log
    p_log_action('p_delete_old_logs', 30);
    DELETE ztranscript_request.log log
     WHERE ((log.lg_activity < (SYSDATE - c_delete_threshold_log_days)
            AND log.lg_level > c_delete_log_threshold)  --October 2018 Updates: corrected this from "less than or equal to" to "greater than"
            OR (log.lg_activity < (SYSDATE - c_delete_log_days)));  --October 2018 Updates: added this in to delete all logs over a year old
    EXCEPTION
    -- don't raise this, just log it. if it doesn't succeed, who cares. 
      WHEN OTHERS
        THEN p_log_error('p_delete_old_logs');
  END p_delete_old_logs;
       
  /**********************************************************************************************************************
  * procedure that deletes a specific request
  * October 2018 Updates: edited this to delete documents and to catch all old requests (was only catching some)
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_request(p_rq_id IN t_pk) IS 
    CURSOR c_doc_deletions IS SELECT docm.zgrdocm_id docm_id
                            FROM ztranscript_request.requests rq
                            LEFT JOIN ztranscript_request.destinations ds
                              ON ds.ds_rq_id = rq.rq_id
                            LEFT JOIN zgeneral.zgrdocm docm
                              ON docm.zgrdocm_reference_id = to_char(ds.ds_id)
                           WHERE rq.rq_id = p_rq_id;
    r_doc_deletions c_doc_deletions%ROWTYPE;
  BEGIN
  --log
    p_log_action('p_delete_request(p_rq_id => ' || p_rq_id || ')', 30);
  --delete destinations, then documents attached, then the request
    DELETE ztranscript_request.destinations ds
     WHERE ds.ds_rq_id = p_rq_id;
    FOR r_doc_deletions IN c_doc_deletions
      LOOP
        DELETE zgeneral.zgrdocm docm
         WHERE docm.zgrdocm_id = r_doc_deletions.docm_id;
      END LOOP;
    DELETE ztranscript_request.requests rq
     WHERE rq.rq_id = p_rq_id;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_delete_request(p_rq_id => ' || p_rq_id || ')');
        RAISE;
  END p_delete_request;
      
  /**********************************************************************************************************************
  * procedure that deletes any requests that are unpaid and are older than the amount of days marked in the form.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_delete_unpaid IS 
    v_delete_days INT;
  BEGIN
  --log
    p_log_action('p_delete_unpaid', 30);
  --find the number of days to wait before deleting. If there's no data found or an invalid number, set it to 30 days.
    BEGIN
      v_delete_days := f_get_text_number('DELETE_UNPAID_REQSTS_DAYS');
      EXCEPTION 
        WHEN NO_DATA_FOUND
          THEN v_delete_days := 30;
        WHEN OTHERS
          THEN p_log_error('p_delete_unpaid');
    END;
  --call the unpaid days procedure to make the deletions
    p_delete_unpaid_days(p_days => v_delete_days);
  --call the procedure to delete old logs
    p_delete_old_logs;
    EXCEPTION
      WHEN OTHERS
    -- don't raise this, just log it. if it doesn't succeed, who cares. 
        THEN p_log_error('p_delete_unpaid');
  END p_delete_unpaid;

  /**********************************************************************************************************************
  * procedure that finds the description of a request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_find_req_description (p_rq_id IN t_pk) IS 
    v_rq_description ztranscript_request.requests.rq_description%TYPE;
    v_rq_desc_item   VARCHAR2(50) := 'P' || v('APP_PAGE_ID') || '_RQ_DESCRIPTION';
  BEGIN
  --log
    p_log_action('p_find_req_description(p_rq_id => ' || p_rq_id || ')', 30);
  --if the primary key has a value
    IF p_rq_id IS NOT NULL
      THEN --find the description of the request
           SELECT rq.rq_description
             INTO v_rq_description
             FROM ztranscript_request.requests rq
            WHERE rq.rq_id = p_rq_id;
           --set the item value
           APEX_UTIL.SET_SESSION_STATE(v_rq_desc_item, v_rq_description);
    END IF;
    EXCEPTION 
      WHEN OTHERS
        THEN p_log_error('p_find_req_description(p_rq_id => ' || p_rq_id || ')');
             RAISE;
  END p_find_req_description;
   
  /**********************************************************************************************************************
  * function that gets the address for the user. 
  * used on page 15 if the user wants to use their listed address to pay
  * Also used to set e-mail address
  * 
  **********************************************************************************************************************/
  PROCEDURE p_get_address (p_rq_id IN t_pk) IS 
    v_addr_name      VARCHAR2(200);
    v_addr_line1     zexec.zsavaddr.street_line1%TYPE;
    v_addr_line2     zexec.zsavaddr.street_line2%TYPE;
    v_addr_city      zexec.zsavaddr.city%TYPE;
    v_addr_stat_code zexec.zsavaddr.stat_code%TYPE;
    v_addr_zip       zexec.zsavaddr.zip%TYPE;
    v_addr_natn_code zexec.zsavaddr.natn_code%TYPE;
    v_email          ztranscript_request.requests.rq_email%TYPE;
    v_comments       ztranscript_request.requests.rq_comments%TYPE;
    v_pidm           ztranscript_request.requests.rq_pidm%TYPE;
  BEGIN
  --log
    p_log_action('p_get_address(p_rq_id => ' || p_rq_id || ')', 30);
  --find the user's address
    BEGIN
      SELECT s.spriden_first_name || CASE WHEN s.spriden_mi IS NOT NULL THEN ' ' || s.spriden_mi END || ' ' || s.spriden_last_name,
             addr.street_line1,
             addr.street_line2,
             addr.city,
             addr.stat_code,
             addr.zip,
             addr.natn_code,
             rq.rq_email,
             rq.rq_comments,
             rq.rq_pidm
        INTO v_addr_name,
             v_addr_line1,
             v_addr_line2,
             v_addr_city,
             v_addr_stat_code,
             v_addr_zip,
             v_addr_natn_code,
             v_email,
             v_comments,
             v_pidm
        FROM ztranscript_request.requests rq
        JOIN saturn.spriden s
          ON s.spriden_pidm = rq.rq_pidm
         AND s.spriden_change_ind IS NULL
        JOIN zexec.zsavaddr addr
          ON  addr.pidm = rq.rq_pidm
         AND addr.addr_rank = 1
       WHERE rq.rq_id = p_rq_id;
      EXCEPTION
        WHEN NO_DATA_FOUND
          THEN BEGIN
                 SELECT s.spriden_first_name || CASE WHEN s.spriden_mi IS NOT NULL THEN ' ' || s.spriden_mi END || ' ' || s.spriden_last_name,
                        addr.spraddr_street_line1,
                        addr.spraddr_street_line2,
                        addr.spraddr_city,
                        addr.spraddr_stat_code,
                        addr.spraddr_zip,
                        addr.spraddr_natn_code,
                        rq.rq_email
                   INTO v_addr_name,
                        v_addr_line1,
                        v_addr_line2,
                        v_addr_city,
                        v_addr_stat_code,
                        v_addr_zip,
                        v_addr_natn_code,
                        v_email
                   FROM ztranscript_request.requests rq
                   JOIN saturn.spriden s
                     ON s.spriden_pidm = rq.rq_pidm
                    AND s.spriden_change_ind IS NULL
                   JOIN saturn.spraddr addr
                     ON addr.spraddr_pidm = rq.rq_pidm
                    AND (addr.spraddr_status_ind IS NULL OR addr.spraddr_status_ind != 'I')
                  WHERE rq.rq_id = p_rq_id
                  ORDER BY addr.spraddr_activity_date DESC
                  FETCH FIRST ROW ONLY;
                 EXCEPTION 
                   WHEN NO_DATA_FOUND
                     THEN NULL;
               END;
    END;
  --set items on the page
    APEX_UTIL.SET_SESSION_STATE('P15_NAME',v_addr_name);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_LINE1',v_addr_line1);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_LINE2',v_addr_line2);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_CITY',v_addr_city);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_STATE',v_addr_stat_code);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_ZIP',v_addr_zip);
    APEX_UTIL.SET_SESSION_STATE('P15_ADDR_NATN',v_addr_natn_code);
    APEX_UTIL.SET_SESSION_STATE('P15_RQ_EMAIL',v_email);
    APEX_UTIL.SET_SESSION_STATE('P15_RQ_COMMENTS',v_comments);  
    APEX_UTIL.SET_SESSION_STATE('P15_RQ_PIDM',v_pidm);  
    EXCEPTION 
        WHEN OTHERS
          THEN p_log_error('p_get_address(p_rq_id => ' || p_rq_id || ')');
          RAISE;
  END p_get_address;
     
  /**********************************************************************************************************************
  * procedure that logs actions as they happen
  * 
  **********************************************************************************************************************/
  PROCEDURE p_log_action (p_action IN VARCHAR2, 
                          p_level  IN INT) IS 
  
  BEGIN
  --if the p_level is set to something equal to or lower than the threshold, log it
    IF p_level <= c_log_level_threshold
      THEN dbms_output.put_line(p_action);
           INSERT INTO ztranscript_request.log (lg_level, lg_action, lg_session, lg_user, lg_activity)
                  VALUES (p_level, p_action, v('APP_SESSION'), v('APP_USER'), SYSDATE);
      COMMIT;
    END IF;
    EXCEPTION 
        WHEN OTHERS
          THEN p_log_error('p_log_action (p_action => ' || p_action || 
                                         'p_level => '  || p_level  || ')');
               RAISE;
  END p_log_action;

  /**********************************************************************************************************************
  * procedure that allows a user to mark multiple requests as waiting for pickup
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_mark_requests_for_pickup IS 
     
  BEGIN
  --log
    p_log_action('p_mark_requests_for_pickup', 30);
  --process the claim
    FOR i IN 1..APEX_APPLICATION.G_F01.COUNT 
      LOOP
      --log
        p_log_action('p_mark_requests_for_pickup procedure, record ' || APEX_APPLICATION.G_F01(i) || '.', 40);
      --claim the record
        p_update_status(p_pk          => APEX_APPLICATION.G_F01(i),
                        p_new_status  => 'WP',
                        p_ro_comments => NULL) ;
    END LOOP;
    EXCEPTION 
      WHEN OTHERS
        THEN p_log_error('p_mark_requests_for_pickup');
             RAISE;
  END p_mark_requests_for_pickup;
        
  /**********************************************************************************************************************
  * procedure that marks a record as having paid for the transcript.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_mark_upay_completion (upay_json IN  VARCHAR2,
                                    results   OUT VARCHAR2) IS 
    v_response zexec.apex_upay_utils.upay_response_type;  
    v_rq_id    t_pk;
  BEGIN
  --log
    p_log_action('p_mark_upay_completion (upay_json => ' || upay_json || ')' , 30);
  --find the response containing the external transaction id
    v_response := zexec.apex_upay_utils.f_upay_response(p_response_in => upay_json);
  --find the pk of the record to be updated
    SELECT rq.rq_id
      INTO v_rq_id
      FROM ztranscript_request.requests rq
     WHERE rq.rq_upay_trans_id = v_response.ext_trans_id;
  --update the status of the record to "In Processing" (and do anything else that's needed within the procedure)
    p_update_status(p_pk          => v_rq_id,
                    p_new_status  => 'WT',
                    p_ro_comments => NULL);
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_mark_upay_completion (upay_json => ' || upay_json ||')');
             results := 'error in p_mark_upay_completion.  SQLERRM: ' || sqlerrm;
             RAISE;
  END p_mark_upay_completion;
   
  /**********************************************************************************************************************
  * procedure that sets the cost of a request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_payment_prep (p_rq_id IN t_pk) IS 
    v_trans_id     VARCHAR2(1000);
    v_cost         t_cost := f_payment_amount(p_pk => p_rq_id);
  BEGIN
  --log
    p_log_action('p_payment_prep(p_rq_id => ' || p_rq_id || ')', 30);
  --set the cost of the request and the external transaction ID if there isn't one already.
    SELECT rq.rq_upay_trans_id
      INTO v_trans_id
      FROM ztranscript_request.requests rq
     WHERE rq.rq_id = p_rq_id;
    IF v_trans_id IS NULL
      THEN v_trans_id := zexec.apex_upay_utils.f_new_external_tran_id;
    END IF;
    --update the request with the transaction ID and cost
    UPDATE ztranscript_request.requests rq
       SET rq.rq_cost = v_cost.cost,
           rq.rq_upay_trans_id = v_trans_id
     WHERE rq.rq_id = p_rq_id;
           p_log_action('p_payment_prep(p_rq_id => ' || p_rq_id || ') found a transaction ID of ' || v_trans_id || ' and a cost of ' || v_cost.cost || '. ',  40);
  --set the apex items for the upay transaction id
    APEX_UTIL.SET_SESSION_STATE('UPAY_EXT_TRANS_ID', v_trans_id);
    APEX_UTIL.SET_SESSION_STATE('P15_RQ_COST',v_cost.cost);
    APEX_UTIL.SET_SESSION_STATE('P15_STANDARD_COST',v_cost.standard_cost);
    APEX_UTIL.SET_SESSION_STATE('P15_SHIPPING_COST',v_cost.shipping_cost);
    APEX_UTIL.SET_SESSION_STATE('P15_COST_EXPLANATION',v_cost.explanation);    
    APEX_UTIL.SET_SESSION_STATE('P15_COST_MATH',v_cost.formula);
  --set the description for display purposes
    ztranscript_request.apex_app_logic.p_find_req_description(p_rq_id => p_rq_id);
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_payment_prep(p_rq_id => ' || p_rq_id || ')');
             RAISE;
  END p_payment_prep;
 
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: added
  * this allows printing transcripts using a baseline Banner job
  * 
  **********************************************************************************************************************/
  PROCEDURE p_print_transcripts (p_printer     IN  t_printers,
                                 p_req_id      IN  t_pk DEFAULT NULL) IS 
    pragma autonomous_transaction;
    l_prnt_id          t_pk;
    l_erring_req_id    t_pk;
    l_notes            ztranscript_request.prints.prnt_notes%TYPE;
    l_numb_prints      INT                                          := 0;
    l_app_user         VARCHAR2(30) := v('APP_USER');
    l_job              general.GJBPDFT.gjbpdft_job%TYPE             := 'SHRTRTC';
    l_jprm_code        GENERAL.GJBPDFT.GJBPDFT_JPRM_CODE%type       := 'TRANS_REQ';
    l_printer_param    varchar2(30)                                 := 'email_2';
    l_pwd              BANSECR.GUBIPRF.GUBIPRF_JS_PRXY_SID%type;
    e_no_params        EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_no_params, -20002);
    l_pagelength       number;
    l_waitforcomp      varchar2(1)                                  := 'Y';
    l_status           varchar2(4000);
    l_seq              number;
    l_tmp              pls_integer;
    CURSOR c_reqs IS   SELECT DISTINCT rq.rq_id rq_id, --the distinct ensures that, even if someone selects the same request twice, it only prints once.
                              rq.rq_pidm pidm,
                              'AL' levl, --might need to revisit this.
                              CASE WHEN ds.ds_sbgi_code IS NOT NULL THEN sbgi.stvsbgi_desc
                                   WHEN ds.ds_dm_code = 'PKUP' THEN 'For Pickup in SSC by Student'
                                   WHEN ds.ds_addr_name IS NOT NULL THEN ds.ds_addr_name
                                   WHEN ds.ds_email_address IS NOT NULL THEN ds.ds_email_address
                                        END aname,
                              ds.ds_addr_line1 aline1, 
                              ds.ds_addr_line2 aline2,
                              ds.ds_addr_city acity, 
                              ds.ds_addr_stat_code astat, 
                              ds.ds_addr_zip azip, 
                              ds.ds_addr_natn_code anatn,
                              rq.rq_created_on req_date,
                              dm.dm_tprt_code tprt_code, --CASE WHEN  tprt_code???
                              ds.ds_sbgi_code sbgi_code,
                              ds.ds_copies_requested copies,
                              ZEXEC.ZBTM_TERM.f_get_current_term_code(p_pidm => rq.rq_pidm) term
                         FROM table(f_printing_prep_req_ids(p_req_id)) reqs
                         JOIN ztranscript_request.requests rq 
                           ON rq.rq_id = reqs.pk
                         JOIN ztranscript_request.destinations ds
                           ON ds.ds_rq_id = rq.rq_id
                         JOIN ztranscript_request.delivery_method dm
                           ON dm.dm_code = ds.ds_dm_code
                         JOIN saturn.spriden s
                           ON s.spriden_pidm = rq.rq_pidm
                          AND s.spriden_change_ind IS NULL
                         LEFT JOIN saturn.stvsbgi sbgi
                           ON sbgi.stvsbgi_code = ds.ds_sbgi_code;
    l_prints           c_reqs%ROWTYPE;
  BEGIN
  -- step 1. basic stuff before starting the real work
  --log
    p_log_action('p_prep_req_for_printing', 30);
  -- insert into the prints table to record a printing session/someone clicking the button
    p_prints_insert(l_prnt_id);
  
  -- step 2. loop through the request information (in the cursor)
  -- and insert stuff where applicable
    BEGIN
      FOR l_prints IN c_reqs LOOP 
        BEGIN
        -- insert to shttrtc to prep for printing
        -- use the right variables here
          insert into SATURN.SHTTRTC (SHTTRTC_USER, SHTTRTC_PIDM, SHTTRTC_LEVL_CODE, SHTTRTC_ACTIVITY_DATE,
                      SHTTRTC_TERM, SHTTRTC_TPRT_CODE, SHTTRTC_TERM_CODE_IN_PRG, SHTTRTC_USER_ID, SHTTRTC_DATA_ORIGIN)
                VALUES (l_app_user, l_prints.pidm, l_prints.levl, SYSDATE, l_prints.term,
                        l_prints.tprt_code, l_prints.term, l_app_user, c_data_origin);
        -- update the request table to show that it's sent to be printed
        -- use the right variables here
          UPDATE ztranscript_request.requests rq
             SET rq.rq_prnt_id = l_prnt_id
           WHERE rq.rq_id = l_prints.rq_id;
        -- add one to the number of requests printed*/
          l_numb_prints := l_numb_prints + 1;
          l_notes := l_notes || ' Request ID ' || l_prints.rq_id || ' printed.' || chr(10);
        -- what about inserting to SPRCMNT? ... should that be here? maybe make it a turn on/off kind of thing?
        -- ...
        -- ...
        EXCEPTION WHEN OTHERS THEN 
          l_notes := l_notes || ' Request ID ' || l_prints.rq_id || ' hit the following error: ' || SQLCODE || SQLERRM || chr(10);
          l_erring_req_id := l_prints.rq_id;
          RAISE;
        END;
      END LOOP;
    -- update the prints table with any notes. 
      p_prints_update(p_prnt_pk => l_prnt_id,
                      p_notes   => l_numb_prints || ' requests printed ' ||
                                   'with the following notes: ' || l_notes);
      p_log_action('p_prep_req_for_printing is complete', 40);
    -- if there was an error, add the info to the notes table and then raise the error. 
    EXCEPTION WHEN OTHERS THEN 
      p_prints_update(p_prnt_pk => l_prnt_id,
                      p_notes   => l_numb_prints || ' requests printed ' ||
                                   'with the following notes: ' || l_notes);
      p_log_error('p_prep_req_for_printing');
      raise_application_error(-20001, 'There was an error gathering information from request id ' || l_erring_req_id || '. ' || SQLCODE || ' ' || SQLERRM);
    END;

  -- step 3. setup and run the job that prints
    BEGIN
    -- set the pwd variable
      select GUBIPRF_JS_PRXY_SID into l_pwd from BANSECR.GUBIPRF;
      -- prepare the print job
      -- delete the last set of parameters this user had for the job
      delete from general.GJBPDFT DF
       where DF.GJBPDFT_JOB = l_job
         and DF.GJBPDFT_USER_ID = l_app_user
         and DF.GJBPDFT_JPRM_CODE = l_jprm_code; 
      -- insert new parameters based on the default set
      insert into general.GJBPDFT (GJBPDFT_JOB, GJBPDFT_NUMBER, GJBPDFT_ACTIVITY_DATE, GJBPDFT_USER_ID,
             GJBPDFT_VALUE, GJBPDFT_JPRM_CODE, GJBPDFT_DATA_ORIGIN)
        select DF.GJBPDFT_JOB,
               DF.GJBPDFT_NUMBER,
               sysdate,
               l_app_user,
               decode(DF.GJBPDFT_VALUE,
                      'SYSDATE',
                      to_char(sysdate, 'DD-MON-YYYY'),
                      DF.GJBPDFT_VALUE),
               l_jprm_code, 
               c_data_origin
          from general.GJBPDFT DF
         where DF.GJBPDFT_JOB = l_job
           and DF.GJBPDFT_USER_ID is null
           and DF.GJBPDFT_JPRM_CODE is null;
      l_tmp := sql%rowcount;
      if l_tmp = 0 then
        RAISE e_no_params;
      end if;
      commit;
      
      -- run the print job
      ZEXEC.GZ_JOBSUB.sbmjob(usr          => l_app_user,
                             pwd          => l_pwd,
                             job          => l_job,
                             paramusr     => l_app_user,
                             paramnam     => l_jprm_code,
                             printername  => p_printer,
                             printerparam => l_printer_param,
                             pagelength   => l_pagelength,
                             waitforcomp  => l_waitforcomp,
                             status       => l_status,
                             seq          => l_seq);
    EXCEPTION 
      WHEN e_no_params THEN
        raise_application_error(-20002, 'Issue finding and creating a parameter set to run the print job.');
      WHEN OTHERS THEN 
        raise_application_error(-20003, 'Issue running the print job.');
    END;
    
  --final exception handling: add notes. 
  EXCEPTION WHEN OTHERS THEN 
    UPDATE ztranscript_request.prints p
       SET p.prnt_notes = l_notes
     WHERE p.prnt_id = l_prnt_id;
    COMMIT;
    RAISE;
  END p_print_transcripts;
   
  /**********************************************************************************************************************
  * procedure that saves comments
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_save_comments (p_rq_id       IN t_pk,
                             p_rq_comments IN ztranscript_request.requests.rq_comments%TYPE) IS 
  BEGIN
  --log
    p_log_action('p_save_comments(p_rq_id => '       || p_rq_id       ||
                                 'p_rq_comments => ' || p_rq_comments || ')', 30);
    UPDATE ztranscript_request.requests r
       SET r.rq_comments = p_rq_comments
     WHERE r.rq_id = p_rq_id;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_save_comments(p_rq_id => '       || p_rq_id       ||
                                         'p_rq_comments => ' || p_rq_comments || ')');
             RAISE;
  END p_save_comments;
 
  /**********************************************************************************************************************
  * procedure that sets the city/state/nation for US zip codes when the zip code is entered
  * October 2018 Updates: added this procedure
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_city_state (p_zip_code IN ztranscript_request.destinations.ds_addr_zip%TYPE,
                              p_city     IN ztranscript_request.destinations.ds_addr_city%TYPE,
                              p_state    IN ztranscript_request.destinations.ds_addr_stat_code%TYPE,
                              p_natn     IN ztranscript_request.destinations.ds_addr_natn_code%TYPE) IS 
    v_city  ztranscript_request.destinations.ds_addr_city%TYPE;
    v_state ztranscript_request.destinations.ds_addr_stat_code%TYPE;
    v_natn  ztranscript_request.destinations.ds_addr_natn_code%TYPE;
  BEGIN
  --log
    p_log_action('p_set_city_state(p_zip_code => ' || p_zip_code || 
                                  'p_city => '     || p_city     ||
                                  'p_state => '    || p_state    ||
                                  'p_natn => '     || p_natn     || ')', 30);
  --call the function that finds the address
    BEGIN
      SELECT zc.gtvzipc_city,
             zc.gtvzipc_stat_code,
             nvl(zc.gtvzipc_natn_code, sn.zsttntn_natn_code)
        INTO v_city,
             v_state,
             v_natn
        FROM general.gtvzipc zc
        JOIN zsaturn.zsttntn sn
          ON sn.zsttntn_stat_code = zc.gtvzipc_stat_code
       WHERE zc.gtvzipc_code = p_zip_code
       FETCH FIRST ROW ONLY;
      EXCEPTION
        WHEN NO_DATA_FOUND
          THEN NULL;
    END;
  --set the items on the page appropriately
    IF v_city IS NOT NULL
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_CITY',      nvl(v_city,p_city));
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_STAT_CODE', nvl(v_state,p_state));
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NATN_CODE', nvl(v_natn,p_natn));
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_set_city_state(p_zip_code => ' || p_zip_code || 
                                          'p_city => '     || p_city     ||
                                          'p_state => '    || p_state    ||
                                          'p_natn => '     || p_natn     || ')');
             RAISE;
  END p_set_city_state;
 
  /**********************************************************************************************************************
  * procedure that sets the address when a college is selected from the list provided
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_coll_addr (p_stat_code IN t_stat,
                             p_sbgi_code IN t_sbgi) IS 
    p_college_record t_college_record;
  BEGIN
  --log
    p_log_action('p_set_coll_addr(p_stat_code => ' || p_stat_code ||
                                ',p_sbgi_code => ' || p_sbgi_code || ')', 30);
  --call the function that finds the address
    BEGIN
      SELECT * 
        INTO p_college_record
        FROM TABLE(ZLUWEBAPP.lu_web_api.retrieve_schools(type_indicator => 'C', state_filter => p_stat_code)) 
       WHERE ceeb_code = p_sbgi_code
         AND ceeb_code != '111111' --ignores the Not Listed entry for schools
         AND alternate_name = 'Primary Record'
       FETCH FIRST ROW ONLY;
      EXCEPTION
        WHEN NO_DATA_FOUND
          THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NAME',       NULL);
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE1',      NULL);
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE2',      NULL);
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_CITY',       NULL);
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_STAT_CODE',  v('P5_STATE_FOR_COLLEGES'));
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_ZIP',        NULL);
               APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NATN_CODE',  NULL);
    END;
  --set the items on the page appropriately
    IF p_college_record.ceeb_code IS NOT NULL
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NAME',       p_college_record.description);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE1',      p_college_record.street_line1);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE2',      p_college_record.street_line2);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_CITY',       p_college_record.city);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_STAT_CODE',  p_college_record.stat_code);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_ZIP',        p_college_record.zip);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NATN_CODE',  p_college_record.natn_code);
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_set_coll_addr(p_stat_code => ' || p_stat_code ||
                                       ' ,p_sbgi_code => ' || p_sbgi_code || ')');
             RAISE;
  END p_set_coll_addr;

  /**********************************************************************************************************************
  * procedure that sets the cost, explanation of the cost, and the formula to calculate the cost
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_cost (p_rq_id     IN t_pk,
                        p_cost_item IN VARCHAR2,
                        p_expl_item IN VARCHAR2,
                        p_math_item IN VARCHAR2) IS
    v_cost_rec t_cost := ztranscript_request.apex_app_logic.f_payment_amount(p_pk => p_rq_id); --finds values
  BEGIN
  --log
    p_log_action('p_set_cost(p_rq_id => '     || p_rq_id     ||
                           ',p_cost_item => ' || p_cost_item ||
                           ',p_expl_item => ' || p_expl_item ||
                           ',p_math_item => ' || p_math_item || ')', 30);
  --set items
    APEX_UTIL.SET_SESSION_STATE(p_cost_item,to_char(v_cost_rec.cost, '$999,999.99'));
    APEX_UTIL.SET_SESSION_STATE(p_expl_item,v_cost_rec.explanation);
    APEX_UTIL.SET_SESSION_STATE(p_math_item,v_cost_rec.formula);
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_set_cost(p_rq_id => '     || p_rq_id     ||
                                   ',p_cost_item => ' || p_cost_item ||
                                   ',p_expl_item => ' || p_expl_item ||
                                   ',p_math_item => ' || p_math_item || ')');
             RAISE;
  END p_set_cost;
  /**********************************************************************************************************************
  * procedure that sets the text items throughout the form
  * 
  **********************************************************************************************************************/
  PROCEDURE p_set_text (p_page IN INT) IS 
    v_like            VARCHAR2(100) := '^P' || p_page || '(_{1})(.+)';
    CURSOR c_text IS SELECT *
                       FROM ztranscript_request.text t
                      WHERE REGEXP_LIKE(t.tx_item, v_like);
    r_text            c_text%ROWTYPE;
    v_can_edit        VARCHAR2(1);
    v_overridden      VARCHAR2(1);
    v_dm_instructions t_text_text;
  BEGIN
  --log
    p_log_action('p_set_text(p_page => ' || p_page || ')', 30);
  --set items
    FOR r_text IN c_text
      LOOP 
        APEX_UTIL.SET_SESSION_STATE(r_text.tx_item, r_text.tx_text);
      END LOOP;
  --on page 5, calculate the delivery method instructions dynamically
    IF p_page = 5 
      THEN v_can_edit   := ztranscript_request.apex_app_logic.f_can_edit(p_pk => v('P5_DS_RQ_ID'));
           v_overridden := ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC.f_cost_overridden(p_rq_id => v('P5_DS_RQ_ID'));
           IF (v('P5_DS_DM_CODE') IS NOT NULL AND v_can_edit = 'Y' AND v_overridden != 'Y')
              THEN --find the instructions for the specific DM code and set the instructions item in APEX
                   v_dm_instructions := f_get_text_varchar('DM_' || v('P5_DS_DM_CODE') || '_INSTRUCTIONS');
                   APEX_UTIL.SET_SESSION_STATE('P5_DM_INSTRUCTIONS', v_dm_instructions);
              ELSIF (v('P5_DS_ID') IS NOT NULL AND v_can_edit = 'Y' AND v_overridden != 'Y')
                THEN --find the instructions of the DM code, but go through the primary key in destinations
                     SELECT txt.tx_text
                       INTO v_dm_instructions
                       FROM ztranscript_request.text txt
                       JOIN ztranscript_request.destinations ds
                         ON ds.ds_id = v('P5_DS_ID')
                        AND txt.tx_item = 'DM_' || ds.ds_dm_code || '_INSTRUCTIONS';
                     APEX_UTIL.SET_SESSION_STATE('P5_DM_INSTRUCTIONS', v_dm_instructions);
              ELSE --find the instructions for all DM codes
                   v_dm_instructions := f_get_text_varchar('DM_NULL_INSTRUCTIONS');
                   APEX_UTIL.SET_SESSION_STATE('P5_DM_INSTRUCTIONS', v_dm_instructions);
            END IF;
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_set_text(p_page => ' || p_page || ')');
             RAISE;
  END p_set_text;

  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * procedure that updates the status of printed requests from In Processing (PC) to Completed (CM)
  * after the allotted amount of time.
  * 
  **********************************************************************************************************************/
  PROCEDURE p_update_printed_PC_to_CM IS 
    v_elapsed_hours       t_text_numb    := f_get_text_number('PC_WAIT_HOURS');
    CURSOR c_requests IS SELECT rq.rq_id rq_id,
                                nvl2(rq.rq_ro_comments, rq.rq_ro_comments || chr(10), NULL) || sysdate || ': ' || 'Automatically moved from "In Processing" to "Completed" after ' || to_char(v_elapsed_hours) || ' hours.' ro_comments
                           FROM ztranscript_request.requests rq
                           JOIN ztranscript_request.prints p
                             ON p.prnt_id = rq.rq_prnt_id
                            AND p.prnt_time < SYSDATE - v_elapsed_hours/24
                          WHERE rq.rq_modified_on < SYSDATE - v_elapsed_hours/24
                            AND rq.rq_st_code = 'PC';
   v_requests c_requests%ROWTYPE;
  BEGIN
  --log
    p_log_action('p_update_printed_PC_to_CM', 30);
  -- loop through the requests that meet the criteria in the cursor
  -- and update them to completed.
    FOR v_requests IN c_requests LOOP
      BEGIN
        p_update_status(p_pk          => v_requests.rq_id,
                        p_new_status  => 'CM',
                        p_ro_comments => v_requests.ro_comments);
    -- if there was an error, log it but don't raise it. 
    -- They can manually work through anything that runs into an issue. 
    -- if they don't notice it as being an issue, 
    -- it'll quicky rise to the top of the report on the processing page (910)
    -- and they'll notice it and ask anyone if that's an issue. 
    -- Rather, they'll probably just mark it as complete manually.
      EXCEPTION WHEN OTHERS THEN 
        p_log_error('p_update_printed_PC_to_CM resulted in an error. ' ||
                    'The current row of the requests cursor shows a pk of ' ||
                    to_char(v_requests.rq_id) || ' and commets are ' || v_requests.ro_comments);
      END;
    END LOOP;
  EXCEPTION WHEN OTHERS THEN 
    p_log_error('p_update_printed_PC_to_CM');
    RAISE;
  END p_update_printed_PC_to_CM;
  
  /**********************************************************************************************************************
  * procedure that updates the status of a single request
  * 
  **********************************************************************************************************************/
  PROCEDURE p_update_status (p_pk          IN t_pk,
                             p_new_status  IN t_status,
                             p_ro_comments IN VARCHAR2) IS 
    v_current_status      t_status;
    /* October 2018 Updates: */
    v_current_status_desc t_status_desc;
    v_new_status_desc     t_status_desc;
    v_student_name        t_stu_name;
    v_request_desc        t_req_desc;
    v_error_text          VARCHAR2(2000);
    /* end October 2018 Updates: */
    v_email_ind           VARCHAR2(1);  --'Y' means an e-mail should be sent, 'N' or null means no e-mail.
    v_mark_processor_ind  VARCHAR2(1);  --'Y' means the person updating it should be marked (if null), 'N' or null means not to
  BEGIN
  --log
    p_log_action('p_update_status (p_pk => '            || p_pk         || 
                                ', p_new_status => '    || p_new_status || 
                                ', p_ro_comments => '   || p_ro_comments   || ')', 30);
  --find the current status of the record
    SELECT rq.rq_st_code,
           st.st_desc,                                  /* October 2018 Updates */
           s.spriden_first_name || ' ' || s.spriden_last_name, /* October 2018 Updates */
           rq.rq_description                            /* October 2018 Updates */
      INTO v_current_status,                            
           v_current_status_desc,                       /* October 2018 Updates */
           v_student_name,                              /* October 2018 Updates */
           v_request_desc                               /* October 2018 Updates */
      FROM ztranscript_request.requests rq
      JOIN ztranscript_request.statuses st              /* October 2018 Updates */
        ON st.st_code = rq.rq_st_code                   /* October 2018 Updates */
      JOIN saturn.spriden s                             /* October 2018 Updates */
        ON s.spriden_pidm = rq.rq_pidm                  /* October 2018 Updates */
       AND s.spriden_change_ind IS NULL                 /* October 2018 Updates */
     WHERE rq.rq_id = p_pk;
  --find information about the status change
    BEGIN
     --check to ensure it's a valid change and find whether an e-mail should be sent (also find out if this change should mark the processor)
       SELECT sc.sc_email_ind,
              sc.sc_mark_processor_ind
         INTO v_email_ind,
              v_mark_processor_ind
         FROM ZTRANSCRIPT_REQUEST.status_changes sc
        WHERE sc.sc_prev_sc_code = v_current_status
          AND sc.sc_new_sc_code = p_new_status;
       EXCEPTION
         WHEN NO_DATA_FOUND
         --if no data was found then it's an invalid status; log that and give an appropriate error message.
           THEN /* October 2018 Updates: */
                SELECT st.st_desc
                  INTO v_new_status_desc
                  FROM ztranscript_request.statuses st
                 WHERE st.st_code = p_new_status;
                /* End October 2018 Updates */
                p_log_action('p_update_status (p_pk => '            || p_pk         || 
                                            ', p_new_status => '    || p_new_status || 
                                            ', p_ro_comments => '   || p_ro_comments   || ') did not happen because it is an invalid status change.', 40);
                --October 2018 Updates: changed below error message to be clear why an invalid change might be occurring. 
                v_error_text := 'You requested to change the status of the transcript request titled "' || v_request_desc || '" for ' || v_student_name || ' from ' || v_current_status_desc || ' to ' || v_new_status_desc || '. This is not a valid change. Please change the status appropriately before trying to mark it as ' || v_new_status_desc || '.' || SQLERRM;
                dbms_output.put_line(v_error_text);
                raise_application_error(-20017, v_error_text);
         WHEN OTHERS
           THEN p_log_error('p_update_status (p_pk => '            || p_pk         || 
                                           ', p_new_status => '    || p_new_status || 
                                           ', p_ro_comments => '   || p_ro_comments   || ')');
                RAISE;
     END;
  --check to see if the status isn't changing; if so, just log the comments (not the status)
    IF v_current_status = p_new_status
      THEN UPDATE ztranscript_request.requests r
              SET r.rq_ro_comments = case WHEN p_ro_comments = r.rq_ro_comments THEN p_ro_comments
                                          WHEN p_ro_comments IS NULL THEN r.rq_ro_comments
                                          WHEN r.rq_ro_comments IS NULL THEN p_ro_comments
                                               ELSE r.rq_ro_comments || chr(10) || p_ro_comments END ,  --case statement added JanFeb2019 to avoid overwriting comments. 
                  r.rq_processor = CASE WHEN v_mark_processor_ind = 'Y' THEN nvl(r.rq_processor,v('APP_USER')) ELSE r.rq_processor END,
                  r.rq_modified_by = v('APP_USER'),
                  r.rq_modified_on = SYSDATE
            WHERE r.rq_id = p_pk;
      --if the status is changing, ...
      ELSE 
         --update the record (provided an error wasn't raised above
           UPDATE ztranscript_request.requests r
              SET r.rq_st_code = p_new_status,
                  r.rq_ro_comments = case WHEN p_ro_comments = r.rq_ro_comments THEN p_ro_comments
                                          WHEN p_ro_comments IS NULL THEN r.rq_ro_comments
                                          WHEN r.rq_ro_comments IS NULL THEN p_ro_comments
                                               ELSE r.rq_ro_comments || chr(10) || p_ro_comments END ,  --case statement added JanFeb2019 to avoid overwriting comments. 
                  r.rq_processor = CASE WHEN v_mark_processor_ind = 'Y' THEN nvl(r.rq_processor,v('APP_USER')) ELSE r.rq_processor END,
                  r.rq_modified_by = v('APP_USER'),
                  r.rq_modified_on = SYSDATE
            WHERE r.rq_id = p_pk;
         --if an e-mail should be sent, send it
           IF v_email_ind = 'Y'
             THEN p_send_email (p_pk     => p_pk, 
                                p_status => p_new_status);
           END IF;
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_update_status (p_pk => '            || p_pk         || 
                                        ', p_new_status => '    || p_new_status || 
                                        ', p_ro_comments => '   || p_ro_comments   || ')');
             RAISE;
  END p_update_status;

  /**********************************************************************************************************************
  * procedure that uploads the attachment for the destination
  * 
  **********************************************************************************************************************/
  PROCEDURE p_upload_attachment (p_ds_id              IN  t_pk,
                                 p_doc_upload         IN  VARCHAR2,
                                 p_ds_doc_id          OUT INT,
                                 p_doc_status         OUT VARCHAR2,
                                 p_doc_status_message OUT VARCHAR2) IS 
    v_error_message  VARCHAR2(4000);
    l_filerow ZAPEX.WWV_FLOW_FILE_OBJECTS$%rowtype;
  BEGIN
  --log
    p_log_action('p_upload_attachment (p_ds_id => '              || p_ds_id              || 
                                     ',p_doc_upload => '         || p_doc_upload         ||
                                     ',p_ds_doc_id => '          || p_ds_doc_id          ||
                                     ',p_doc_status => '         || p_doc_status         || 
                                     ',p_doc_status_message => ' || p_doc_status_message || ')', 30);
  --find the data that APEX stores on the back end when a user uploads a file    
    select *
    into l_filerow
    from ZAPEX.WWV_FLOW_FILE_OBJECTS$
    where flow_id = v('APP_ID') and name = p_doc_upload;
  --run the procedure to enter the document into the correct place and return the right values  
    ZEXEC.GZ_DOCUMENTS.p_submit_document(l_filerow.filename,l_filerow.dad_charset,l_filerow.blob_content, v('APP_ID'),v('APP_USER'),v('USER_PIDM'),'B-S-ID','',p_ds_id, sysdate+numtoyminterval(5,'year'),p_ds_doc_id,p_doc_status,p_doc_status_message);
  --update the destination to include the document ID
    UPDATE ztranscript_request.destinations ds
       SET ds.ds_doc_id = p_ds_doc_id
     WHERE ds.ds_id = p_ds_id;
    EXCEPTION
      WHEN NO_DATA_FOUND 
        THEN --pull up the error message and log/return it.
             v_error_message := f_get_text_varchar('DOC_UPLOAD_NO_DOC');
             p_log_action('p_upload_attachment (p_ds_id => '              || p_ds_id              || 
                                              ',p_doc_upload => '         || p_doc_upload         ||
                                              ',p_ds_doc_id => '          || p_ds_doc_id          ||
                                              ',p_doc_status => '         || p_doc_status         || 
                                              ',p_doc_status_message => ' || p_doc_status_message || 
                                              ') resulted in an error: raise_application_error (-20012, ' || v_error_message ||
                                              ')', 40);
             raise_application_error (-20012, v_error_message);
      WHEN OTHERS
        THEN p_log_error('p_upload_attachment (p_ds_id => '              || p_ds_id              || 
                                             ',p_doc_upload => '         || p_doc_upload         ||
                                             ',p_ds_doc_id => '          || p_ds_doc_id          ||
                                             ',p_doc_status => '         || p_doc_status         || 
                                             ',p_doc_status_message => ' || p_doc_status_message || ')');
             RAISE;
  END p_upload_attachment;
  
  /**********************************************************************************************************************
  * Added during October 2018 Updates
  * Function that calculates whether the exp_ship_ind should be able to be selected. 
  * If so, set the value that was selected. 
  * Otherwise, null.
  * 
  **********************************************************************************************************************/
  FUNCTION f_calculate_exp_ship_ind (p_dm_code      IN t_dm, 
                                     p_exp_ship_ind IN VARCHAR2) RETURN VARCHAR2 IS 
  BEGIN
  --log
    p_log_action('f_calculate_exp_ship_ind (p_dm_code => '      || p_dm_code      || 
                                          ',p_exp_ship_ind => ' || p_exp_ship_ind || ')', 30);
  --see if this delivery method/destination type allows expedited shipping. if it does, return what the user entered. If not, return null.
    IF ztranscript_request.apex_app_logic.f_dest_page_item_condit(p_dm_code => p_dm_code, p_condit_type => 'EXSP')  
      THEN RETURN p_exp_ship_ind;
      ELSE RETURN NULL;
    END IF;
    EXCEPTION 
      WHEN OTHERS
        THEN p_log_error('f_calculate_exp_ship_ind (p_dm_code => '      || p_dm_code      || 
                                                  ',p_exp_ship_ind => ' || p_exp_ship_ind || ')');
             RAISE;
  END f_calculate_exp_ship_ind;

  /**********************************************************************************************************************
  * function that determines if this request can be edited by the requester
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_edit (p_pk IN t_pk DEFAULT NULL) RETURN VARCHAR2 IS 
    v_pidm              t_pidm := v('USER_PIDM');
    v_can_edit_ind      VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_can_edit (p_pk => ' || p_pk || ')', 30);
  --check if they can order their transcripts, because if they can't order, then they shouldn't be able to edit
    IF f_current_transcript_hold(p_pidm => v_pidm) = 'N'
      THEN IF p_pk IS NOT NULL
             THEN --lookup the indicator connected to their status
                  SELECT CASE WHEN (v('USER_PIDM') = rq.rq_pidm OR v('APP_PAGE_ID') = 912)      --ensures that someone can't access anyone else's record
                                   THEN st.st_can_edit_ind
                                        ELSE 'Z' END        --if they are trying to access someone's record that they shouldn't, throw a big error (see if statement about 15 lines below)
                    INTO v_can_edit_ind
                    FROM ztranscript_request.requests rq
                    JOIN ztranscript_request.statuses st
                      ON st.st_code = rq.rq_st_code
                   WHERE rq.rq_id = p_pk;
             ELSE v_can_edit_ind := 'Y';
           END IF;
      ELSE --if they have a hold, they can't edit
           v_can_edit_ind := 'N';
    END IF;
  --if they are trying to access someone's record that they shouldn't, throw a big error
    IF v_can_edit_ind = 'Z'
      THEN p_log_action('f_can_edit (p_pk => ' || p_pk || ') raised the IMPROPER_USER error: Stop trying to access another student''s transcript request. the Registrar''s Office and IT have been notified.', 40);
           raise_application_error(-20005, 'Stop trying to access another student''s transcript request. the Registrar''s Office and IT have been notified.');
    END IF;
  --log what's being returned and return it.
    p_log_action('f_can_edit (p_pk => ' || p_pk || ') returned ' || v_can_edit_ind || '.', 40);
    RETURN v_can_edit_ind;
    EXCEPTION
     WHEN OTHERS
        THEN p_log_error('f_can_edit (p_pk => ' || p_pk || ')'); 
             RAISE;
  END f_can_edit;
  
  /**********************************************************************************************************************
  * function that determines if the cost can be edited (by the RO only)
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_edit_cost (p_rq_id IN t_pk) RETURN VARCHAR2 IS 
    v_in_processing ztranscript_request.statuses.st_in_processing_ind%TYPE;
  BEGIN 
  --log
    p_log_action('f_can_edit_cost (p_rq_id => ' || p_rq_id || ')', 30);
  --find if the request is in processing 
    SELECT nvl(st.st_can_edit_ind, 'N')    --indicates that the student can still edit the request, in which case they haven't paid for it.
      INTO v_in_processing
      FROM ztranscript_request.requests rq
      JOIN ztranscript_request.statuses st
        ON st.st_code = rq.rq_st_code
     WHERE rq.rq_id = p_rq_id;
  --log the return and return it
    p_log_action('f_can_edit_cost (p_rq_id => ' || p_rq_id || ') returned ' || v_in_processing, 30);
    RETURN v_in_processing;
  END f_can_edit_cost;
       
  /**********************************************************************************************************************
  * function that determines if a student can order their transcripts
  * 
  **********************************************************************************************************************/
  FUNCTION f_can_request (p_pidm IN t_pidm) RETURN VARCHAR2 IS
    v_can_request_ind VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_can_request (p_pidm => ' || p_pidm || ')', 30);
    IF f_stu_has_course(p_pidm => p_pidm) = 'Y' AND f_current_transcript_hold(p_pidm => p_pidm) = 'N'
      THEN v_can_request_ind := 'Y';
      ELSE v_can_request_ind := 'N';
    END IF;
  --log the return and return it
    p_log_action('f_can_request (p_pidm => ' || p_pidm || ') returned ' || v_can_request_ind, 40);
    RETURN v_can_request_ind;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_can_request (p_pidm => ' || p_pidm || ')');
             RAISE;
  END f_can_request;

  /**********************************************************************************************************************
  * determines what the current cost of a request is (used to see if it's being changed)
  * procedure that determines if the cost for this request has been changed (only the RO has permissions to override/change, in which case it locks for the form for that record).
  * if so, it marks the "cost Override" indicator in the table.
  * 
  **********************************************************************************************************************/
  FUNCTION f_check_cost_update (p_rq_id    IN t_pk, 
                                 p_new_cost IN ztranscript_request.requests.rq_cost%TYPE) RETURN VARCHAR2 IS 
    v_cost   ztranscript_request.requests.rq_cost%TYPE;
    v_return ztranscript_request.requests.rq_cost_override_ind%TYPE;
  BEGIN
  --log
    p_log_action('p_check_cost_update (p_rq_id => '    || p_rq_id    || 
                                     ',p_new_cost => ' || p_new_cost || ')', 30);
  --find the cost
    SELECT rq.rq_cost
      INTO v_cost
      FROM ztranscript_request.requests rq
     WHERE rq.rq_id = p_rq_id;
  --if there is a change in cost, mark the override indicator
    IF ((v_cost IS NULL AND p_new_cost IS NOT NULL)
         OR 
        (v_cost != p_new_cost))
      THEN v_return := 'Y';
      ELSE v_return := 'N';
    END IF;
  --log the return and return is 
    p_log_action('p_check_cost_update (p_rq_id => '    || p_rq_id    || 
                                     ',p_new_cost => ' || p_new_cost || ') returned ' || v_return, 40);
    RETURN v_return;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('p_check_cost_update (p_rq_id => '    || p_rq_id    || 
                                     ',p_new_cost => ' || p_new_cost || ')');
             RAISE;
  END f_check_cost_update;
  
  /**********************************************************************************************************************
  * function that determines whether or not the college address fields should be enabled (or disabled)
  *
  **********************************************************************************************************************/
  FUNCTION f_coll_addr_condit (p_ds_id        IN t_pk, 
                               p_part_of_addr IN VARCHAR2) RETURN BOOLEAN IS 
    r_college_record t_college_record;
    v_dm_code        ztranscript_request.delivery_method.dm_code%TYPE;
    v_sbgi_code      ztranscript_request.destinations.ds_sbgi_code%TYPE;
    v_return         BOOLEAN;
  BEGIN   
  --log
    p_log_action('f_coll_addr_condit (p_ds_id => '        || p_ds_id        || 
                                    ',p_part_of_addr => ' || p_part_of_addr || ')', 30);
  --if there's nothing for the PK, it's enabled
    IF p_ds_id IS NULL
      THEN v_return := TRUE;
      ELSE --see if this record is a college one and shows address
           SELECT ds.ds_dm_code,
                  ds.ds_sbgi_code 
             INTO v_dm_code,
                  v_sbgi_code
             FROM ztranscript_request.destinations ds
            WHERE ds.ds_id = p_ds_id;
           --if this is not a DM type of COLL, then it's enabled
           IF nvl(v('P5_DS_DM_CODE'),v_dm_code) != 'COLL'
             THEN v_return := TRUE;
             ELSE --then see what was auto-populated by ZLUWEBAPP.lu_web_api.retrieve_schools, and if it was, then it's disabled
                  BEGIN
                    SELECT * 
                      INTO r_college_record
                      FROM TABLE(ZLUWEBAPP.lu_web_api.retrieve_schools(type_indicator => 'C', state_filter => NULL)) 
                     WHERE ceeb_code = v_sbgi_code
                       AND ceeb_code != '111111' --ignores the Not Listed entry for schools
                       AND alternate_name = 'Primary Record'
                     FETCH FIRST ROW ONLY;
                    IF p_part_of_addr = 'NAME' AND r_college_record.description IS NOT NULL
                      THEN v_return := FALSE;
                      ELSIF p_part_of_addr = 'LINE1' AND r_college_record.street_line1 IS NOT NULL
                            THEN v_return := FALSE;
                            ELSIF p_part_of_addr = 'LINE2' AND r_college_record.street_line2 IS NOT NULL
                                  THEN v_return := FALSE;
                                  ELSIF p_part_of_addr = 'CITY' AND r_college_record.city IS NOT NULL
                                        THEN v_return := FALSE;
                                        ELSIF p_part_of_addr = 'STAT' AND r_college_record.stat_code IS NOT NULL
                                              THEN v_return := FALSE;
                                              ELSIF p_part_of_addr = 'ZIP' AND r_college_record.zip IS NOT NULL
                                                    THEN v_return := FALSE;
                                                    ELSIF p_part_of_addr = 'NATN' AND r_college_record.natn_code IS NOT NULL
                                                          THEN v_return := FALSE;
                                                          ELSE v_return := TRUE;
                    END IF;
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                        THEN v_return := TRUE;
                  END;
           END IF;
    END IF;
   --log return and return it
    IF v_return
      THEN p_log_action('f_coll_addr_condit (p_ds_id => '        || p_ds_id        ||
                                           ',p_part_of_addr => ' || p_part_of_addr || ') returned TRUE', 40);
      ELSE p_log_action('f_coll_addr_condit (p_ds_id => '        || p_ds_id        ||
                                           ',p_part_of_addr => ' || p_part_of_addr || ') returned FALSE', 40);
    END IF;
    RETURN v_return;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_coll_addr_condit (p_ds_id => '        || p_ds_id        || 
                                            ',p_part_of_addr => ' || p_part_of_addr || ')');
             RAISE;
  END f_coll_addr_condit;
  
  /**********************************************************************************************************************
  * function that determines if the cost for this request has been overridden (only the RO has permissions to override, in which case it locks for the form for that record.
  * 
  **********************************************************************************************************************/
  FUNCTION f_cost_overridden (p_rq_id IN t_pk) RETURN VARCHAR2 IS 
    v_response ztranscript_request.requests.rq_cost_override_ind%TYPE;
  BEGIN
  --log
    p_log_action('f_cost_overridden (p_rq_id => ' || p_rq_id || ')', 30);
    IF p_rq_id IS NULL 
      THEN v_response := 'N';
      ELSE --find the cost_override indicator
           SELECT CASE WHEN st.st_can_edit_ind = 'Y'          --if the status is one that normally allows the student to edit, check if the RO has overridden the cost. 
                            THEN rq.rq_cost_override_ind
                                 ELSE 'N' END                 --if the status isn't one that normally allows the student to edit, then it is irrelevant if the cost has been overridden (because the student has already paid), and everything should function normally from thereon. 
             INTO v_response
             FROM ztranscript_request.requests rq
             JOIN ztranscript_request.statuses st
               ON st.st_code = rq.rq_st_code
            WHERE rq.rq_id = p_rq_id;
         --if the response was null, return 'N'
           IF v_response IS NULL
             THEN v_response := 'N';
           END IF;
    END IF;
  --log the return and return it
    p_log_action('f_cost_overridden (p_rq_id => ' || p_rq_id || ') returned ' || v_response, 40);
    RETURN v_response;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_cost_overridden (p_rq_id => ' || p_rq_id || ')');
             RAISE;
  END f_cost_overridden;
      
  /**********************************************************************************************************************
  * function that determines what the next sequence number should be for the request
  * 
  **********************************************************************************************************************/
  FUNCTION f_destination_seq_no (p_pk IN t_pk) RETURN INT IS 
    v_seq_no INT;
  BEGIN
  --log
    p_log_action('f_destination_seq_no (p_pk => ' || p_pk || ')', 30);
  --find the next sequence number
    SELECT nvl(MAX(ds.ds_seq_no) + 1,1)
      INTO v_seq_no
      FROM ztranscript_request.destinations ds
     WHERE ds.ds_rq_id = p_pk;
    p_log_action('f_destination_seq_no (p_pk => ' || p_pk || ') returned ' || v_seq_no, 40);     
    RETURN v_seq_no;
  --if no data was found, this is the first one.
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_destination_seq_no (p_pk => ' || p_pk || ')');
             RAISE;
  END f_destination_seq_no;
  
  /**********************************************************************************************************************
  * function that determines hide/show conditions for items on the add destinations form page (5)
  * 
  **********************************************************************************************************************/
  FUNCTION f_dest_page_item_condit (p_dm_code     IN t_dm, 
                                    p_condit_type IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN IS 
    v_dm_hs_record t_dm_hide_show;
    FUNCTION f_return_val (p_yn_variable IN varchar2) RETURN BOOLEAN IS
      BEGIN 
      --if whatever's being passed through is Y
        IF p_yn_variable = 'Y'
        --then log the return and return TRUE
          THEN p_log_action('f_dest_page_item_condit (p_dm_code => '     || p_dm_code     || 
                                                    ',p_condit_type => ' || p_condit_type || ') returned TRUE.', 40);
               RETURN TRUE;
          ELSE p_log_action('f_dest_page_item_condit (p_dm_code => '     || p_dm_code     || 
                                                    ',p_condit_type => ' || p_condit_type || ') returned FALSE.', 40);
               RETURN FALSE;
        END IF;
      END f_return_val;
  BEGIN
  --log
    p_log_action('f_dest_page_item_condit (p_dm_code => '     || p_dm_code     || 
                                         ',p_condit_type => ' || p_condit_type || ')', 30);
  --if there is no value for p_condit_type, then log and return false 
    IF p_condit_type IS NULL OR p_dm_code IS NULL
      THEN RETURN f_return_val('N');
    --otherwise determine what to return
      ELSE --find what should show based on this dm_code
           v_dm_hs_record := f_dm_hide_show(p_dm_code => p_dm_code);
         --depending on which condition type they want checked, go through and return TRUE. If no TRUE value is hit, return FALSE.
           IF p_condit_type = 'ADDR'
             THEN RETURN f_return_val(v_dm_hs_record.addr);
             ELSIF p_condit_type = 'EMAL'
               THEN RETURN f_return_val(v_dm_hs_record.emal);
               ELSIF p_condit_type = 'CMMT'
                 THEN RETURN f_return_val(v_dm_hs_record.cmmt);
                 ELSIF p_condit_type = 'COLL'
                   THEN RETURN f_return_val(v_dm_hs_record.coll);
                   ELSIF p_condit_type = 'ATCH'
                     THEN RETURN f_return_val(v_dm_hs_record.atch);
                     ELSIF p_condit_type = 'EXSP'
                       THEN RETURN f_return_val(v_dm_hs_record.exsp);
                       ELSIF p_condit_type = 'CPYS'
                         THEN RETURN f_return_val(v_dm_hs_record.cpys);
                         ELSIF p_condit_type = 'ESCP'
                           THEN RETURN f_return_val(v_dm_hs_record.escp);
                           ELSE p_log_action('f_dest_page_item_condit (p_dm_code => '     || p_dm_code     || 
                                                                     ',p_condit_type => ' || p_condit_type || ') returned FALSE due to unrecognized value for variable p_condit_type.', 40);
                             RETURN FALSE;
           END IF;    
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_dest_page_item_condit (p_dm_code => '     || p_dm_code     || 
                                                 ',p_condit_type => ' || p_condit_type || ')');
             RAISE;
  END f_dest_page_item_condit;
  
  /**********************************************************************************************************************
  * function that determines how many copies (if there is a set number) for a specific type of delivery method
  * 
  **********************************************************************************************************************/
  FUNCTION f_dm_code_copies (p_dm_code IN t_dm) RETURN INT IS 
    v_copies INT;
  BEGIN
  --log
    p_log_action('f_dm_code_copies (p_dm_code => ' || p_dm_code || ')', 30);
  --find the amount of copies (if any)
    SELECT dm.dm_copies_per_dest
      INTO v_copies
      FROM ztranscript_request.delivery_method dm
     WHERE dm.dm_code = p_dm_code;
  --return the result 
    p_log_action('f_dm_code_copies (p_dm_code => ' || p_dm_code || ') returned ' || v_copies, 40);
    RETURN v_copies;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_dm_code_copies (p_dm_code => ' || p_dm_code || ')');
             RAISE;
  END f_dm_code_copies;

  /**********************************************************************************************************************
  * function that determines if a user doesn't have to pay for their transcripts
  * October 2018 Updates: edited this procedure to allow the discount for military spouses to be turned on/off.
  * 
  **********************************************************************************************************************/
  FUNCTION f_fee_waiver (p_pidm IN t_pidm) RETURN VARCHAR2 IS 
    v_attr_curr_term    VARCHAR2(6);
    v_mil_spouse_waiver VARCHAR2(1);
    v_waive_ind         VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_fee_waiver (p_pidm => ' || p_pidm || ')', 30);
  --October 2018 Updates: see if military spouses should receive the discount
    IF upper(f_get_text_varchar('MILITARY_SPOUSE_WAIVER')) LIKE '%Y%'
      THEN v_mil_spouse_waiver := 'Y';
      ELSE v_mil_spouse_waiver := 'N';
    END IF;
  --find the most recent term that the student's attributes were updated that is less than or equal to the current standard term
    SELECT MAX(satt.sgrsatt_term_code_eff)
      INTO v_attr_curr_term
      FROM saturn.sgrsatt satt
     WHERE satt.sgrsatt_pidm = p_pidm
       AND satt.sgrsatt_term_code_eff <= (SELECT MIN(term.term_code)
                                            FROM zbtm.terms_by_group_v term
                                           WHERE term.end_date >= SYSDATE
                                             AND term.group_code = 'STD');    
  --log term that was found
    p_log_action('f_fee_waiver (p_pidm => ' || p_pidm || ') found a term of ' || v_attr_curr_term, 30);
  --find if the regular fee should be waived
    SELECT waive.ind
      INTO v_waive_ind
      --find if they have a military attribute (but not a military spouse attribute)
      FROM (SELECT 'Y' ind
              FROM saturn.sgrsatt satt1
             WHERE satt1.sgrsatt_pidm = p_pidm
               AND satt1.sgrsatt_atts_code = 'MILT'         --find someone who is military
               --AND satt1.sgrsatt_term_code_eff = v_attr_curr_term    --removed this requirement per RO. Military Affairs (MAO) removes the MILT attribute after a few years 
                                                                       --and asks students to re-verify their status. Due to students potentially requesting transcripts long after they've had to verify their military status, 
                                                                       --the RO wants to simply check if the student has ever had the MILT attribute. 
                                                                       --They are aware that some people who were dishonorably discharged will receive the discount when they shouldn't. 
               AND NOT EXISTS (SELECT 'X'     --exclude military spouses for this part of the union
                                 FROM saturn.sgrsatt satt2
                                WHERE satt2.sgrsatt_pidm = p_pidm
                                  AND satt2.sgrsatt_atts_code = 'MLSP'
                                  AND satt2.sgrsatt_term_code_eff = v_attr_curr_term)
            /* October 2018 Updates */
             UNION ALL
            SELECT 'Y' ind
              FROM saturn.sgrsatt satt1
             WHERE v_mil_spouse_waiver = 'Y'                --ensure the military spouse waiver is active
               AND satt1.sgrsatt_pidm = p_pidm
               AND satt1.sgrsatt_atts_code = 'MLSP'         --find military spouses
               AND satt1.sgrsatt_term_code_eff = v_attr_curr_term
            /* End October 2018 Updates */
             UNION ALL
             --find if they are a current employee (faculty or staff, not student workers)
            SELECT 'Y' ind
              FROM zgeneral.currentemployees ce
              JOIN saturn.spriden s
                ON s.spriden_id = ce.EmpID
               AND s.spriden_change_ind IS NULL
             WHERE ce.EMPSTATUS IN ('A','L')
               AND s.spriden_pidm = p_pidm
           ) waive
     FETCH FIRST ROW ONLY;
  --log what was returned and return it (if data was found)
    p_log_action('f_fee_waiver (p_pidm => ' || p_pidm || ') returned ' || v_waive_ind, 30);
    RETURN v_waive_ind;
    --if there wasn't any data found, they don't get the waiver, so return 'N'
    EXCEPTION
      WHEN NO_DATA_FOUND
        THEN --log what was returned and return it 
             p_log_action('f_fee_waiver (p_pidm => ' || p_pidm || ') returned N', 30);
             RETURN 'N';
      WHEN OTHERS
        THEN p_log_error('f_fee_waiver (p_pidm => ' || p_pidm || ')');
             RAISE;
  END f_fee_waiver;
    
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if a student currently has an MD hold on their account. 
  * 
  **********************************************************************************************************************/
  FUNCTION f_has_md_hold (p_pidm  IN  t_pidm) RETURN VARCHAR2 IS 
    response VARCHAR2(1);
  BEGIN 
  --log
    p_log_action('f_has_md_hold (p_pidm => ' || p_pidm || ')', 30);
  -- see if there's an MD hold for the student right now
    BEGIN
      SELECT 'Y'
        INTO response
        FROM saturn.sprhold h
       WHERE h.sprhold_pidm = p_pidm
         AND h.sprhold_hldd_code = 'MD' 
         AND h.sprhold_to_date > SYSDATE
       FETCH FIRST ROW ONLY;
    -- if no data was found, then there's not one. 
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      response := 'N';
    END;
  -- log and return.
    p_log_action('f_has_md_hold (p_pidm => ' || p_pidm || ') returned ' || response, 30);
    RETURN response;
  EXCEPTION WHEN OTHERS THEN
    p_log_error('f_has_md_hold (p_pidm => ' || p_pidm || ')');
    RAISE;
  END f_has_md_hold;
    
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if sending to this destination is available via escrip.
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_escrip (p_sbgi_code IN  t_sbgi DEFAULT NULL,
                        p_ds_id     IN  t_pk   DEFAULT NULL) RETURN VARCHAR2 IS 
    v_escrip_locs       INT := 0;
    response            VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_is_escrip (p_sbgi_code => ' || p_sbgi_code || ')', 30);
  -- find the number of escrip locations with this sbgi code
    IF p_sbgi_code IS NOT NULL THEN
      SELECT COUNT('X')
        INTO v_escrip_locs
        FROM ztranscript_request.escrip_locations el
       WHERE el.el_sbgi_code = p_sbgi_code
         AND el_active_ind = 'Y';
    ELSIF p_ds_id IS NOT NULL THEN 
      SELECT COUNT('X')
        INTO v_escrip_locs
        FROM ztranscript_request.escrip_locations el
        JOIN ztranscript_request.destinations ds
          ON ds.ds_id = p_ds_id
         AND ds.ds_sbgi_code = el.el_sbgi_code
       WHERE el_active_ind = 'Y';
    END IF;
  -- if there is 1 or more location(s), return true
    IF v_escrip_locs > 0 THEN
       response := 'Y';
  -- if not, return false
    ELSE 
       response := 'N';
    END IF;
  -- log and return
    p_log_action('f_is_escrip (p_sbgi_code => ' || p_sbgi_code || ') returned ' || response || '.', 30);
    RETURN response;
  EXCEPTION WHEN OTHERS
    THEN p_log_error('f_is_escrip (p_sbgi_code => ' || p_sbgi_code || ')');
         RAISE;
  END f_is_escrip;
  
  /**********************************************************************************************************************
  * function (mainly for authorization) that determines if someone has ever been a student
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_student (p_user IN VARCHAR2) RETURN VARCHAR2 IS 
    v_student_ind VARCHAR2(1);
    v_invalid_level_codes t_text_text := upper(f_get_text_varchar('INVALID_LEVEL_CODES'));
  BEGIN
  --log
    p_log_action('f_is_student (p_user => ' || p_user || ')', 30);
  --see if they have a sgbstdn record
    SELECT CASE WHEN COUNT('X') > 0 THEN 'Y' ELSE 'N' END
      INTO v_student_ind
      FROM saturn.sgbstdn d
      JOIN general.gobtpac g
        ON g.gobtpac_pidm = d.sgbstdn_pidm
       AND g.gobtpac_ldap_user = p_user
     WHERE v_invalid_level_codes NOT LIKE '%-' || d.sgbstdn_levl_code || '-%';
  --log the return and return it
    p_log_action('f_is_student (p_user => ' || p_user || ') returned ' || v_student_ind, 40);
    RETURN v_student_ind;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_is_student (p_user => ' || p_user || ')');
             RAISE;
  END f_is_student;
       
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that determines if the student entered their own e-mail address
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_users_email (p_entered_email  IN general.goremal.goremal_email_address%TYPE)
                             RETURN VARCHAR2 IS 
    response VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_is_users_email (p_entered_email => ' || p_entered_email || ')', 30);
  -- see if this e-mail has ever been in the system for this user
    BEGIN 
      SELECT 'Y'
        INTO response
        FROM general.goremal g
        JOIN general.gobtpac u
          ON u.gobtpac_pidm = g.goremal_pidm
         AND u.gobtpac_ldap_user = v('APP_USER')
       WHERE upper(g.goremal_email_address) = upper(p_entered_email)
       FETCH FIRST ROW ONLY;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      response := 'N';
    END;
    --log the return and return it
    p_log_action('f_is_users_email (p_entered_email => ' || p_entered_email || ') returned ' || response, 40);
    RETURN response;
  EXCEPTION WHEN OTHERS THEN 
    p_log_error('f_is_users_email (p_entered_email => ' || p_entered_email || ')');
    RAISE;
  END f_is_users_email;
    
  /**********************************************************************************************************************
  * function that determines if a document upload is a valid filetype
  * 
  **********************************************************************************************************************/
  FUNCTION f_is_valid_file (p_doc_upload IN ZAPEX.WWV_FLOW_FILE_OBJECTS$.NAME%TYPE) RETURN BOOLEAN IS 
    v_rows  INT;
    v_valid_types t_text_text := upper(f_get_text_varchar('VALID_FILE_TYPES'));
  BEGIN
  --log
    p_log_action('f_is_valid_file (p_doc_upload => ' || p_doc_upload || ')', 30);
  --query to see the file type
    SELECT count(*)
      INTO v_rows
      FROM (SELECT 'X'
            FROM ZAPEX.WWV_FLOW_FILE_OBJECTS$ apexy 
            JOIN zgeneral.zgrmime mime ON apexy.mime_type = mime.zgrmime_mime_type
            WHERE mime.zgrmime_file_type IN (SELECT mime2.zgrmime_file_type
                                               FROM zgeneral.zgrmime mime2
                                              WHERE v_valid_types LIKE '%-' || mime2.zgrmime_file_type || '-%')
              AND apexy.flow_id = v('APP_ID') and apexy.name = p_doc_upload
            union
            select 'X'
            from dual
            where p_doc_upload is NULL);
  --log the return and return it
    IF v_rows > 0
      THEN p_log_action('f_is_valid_file (p_doc_upload => ' || p_doc_upload || ') returned TRUE', 40);
           RETURN TRUE;
      ELSE p_log_action('f_is_valid_file (p_doc_upload => ' || p_doc_upload || ') returned FALSE', 40);
           RETURN FALSE;
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_is_valid_file (p_doc_upload => ' || p_doc_upload || ')');      
             RAISE;
  END f_is_valid_file;
      
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that returns a pidm based on an ID number
  * 
  **********************************************************************************************************************/
  FUNCTION f_lookup_pidm (p_lu_id  IN VARCHAR2) RETURN t_pidm IS 
    v_pidm t_pidm;
  BEGIN
  --log
    p_log_action('f_lookup_pidm (p_lu_id => ' || p_lu_id || ')', 30);
    SELECT s.spriden_pidm
      INTO v_pidm
      FROM saturn.spriden s
     WHERE s.spriden_id = p_lu_id
       AND s.spriden_change_ind IS NULL;
    p_log_action('f_lookup_pidm (p_lu_id => ' || p_lu_id || ') returned ' || TO_char(v_pidm), 40);   
    RETURN v_pidm;
  EXCEPTION WHEN NO_DATA_FOUND THEN 
                 p_log_action('f_lookup_pidm (p_lu_id => ' || p_lu_id || ') returned NULL', 40);
                 RETURN NULL;
            WHEN OTHERS THEN
                 p_log_error('f_lookup_pidm (p_lu_id => ' || p_lu_id || ')');      
                 RAISE;
  END f_lookup_pidm;
        
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: Added
  * function that returns a name based on pidm
  * 
  **********************************************************************************************************************/
  FUNCTION f_lookup_name (p_pidm  IN t_pidm, 
                          p_type  IN VARCHAR2 DEFAULT 'FULLNM') RETURN VARCHAR2 IS 
    v_name VARCHAR2(200);
    v_type VARCHAR2(100) := upper(p_type);
  BEGIN
  --log
    p_log_action('f_lookup_name (p_pidm => ' || p_pidm || ', ' ||
                                'p_type => ' || p_type || ')', 30);
  -- lookup the student's name
    BEGIN
      SELECT CASE WHEN v_type = 'FULLNM'   THEN s.spriden_first_name || ' ' || s.spriden_last_name
                  WHEN v_type = 'FULL'     THEN s.spriden_first_name || ' ' || nvl2(s.spriden_mi, s.spriden_mi || ' ', null) || s.spriden_last_name
                  WHEN v_type = 'FIRST'    THEN s.spriden_first_name
                  WHEN v_type = 'LAST'     THEN s.spriden_last_name
                  WHEN v_type = 'MI'       THEN s.spriden_mi
                       ELSE s.spriden_first_name || ' ' || s.spriden_last_name END 
        INTO v_name
        FROM saturn.spriden s
       WHERE s.spriden_pidm = p_pidm
         AND s.spriden_change_ind IS NULL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_name := NULL;
    END;
  -- log and return
    p_log_action('f_lookup_name (p_pidm => ' || p_pidm || ', ' ||
                                'p_type => ' || p_type || ') returned ' || v_name, 40);   
    RETURN v_name;
  EXCEPTION WHEN OTHERS THEN 
    p_log_error('f_lookup_name (p_pidm => ' || p_pidm || ', ' ||
                               'p_type => ' || p_type || ')');      
    RAISE;
  END f_lookup_name;
   
  /**********************************************************************************************************************
  * function that determines how much a student needs to pay for their transcripts
  * 
  **********************************************************************************************************************/
  FUNCTION f_payment_amount (p_pk IN t_pk) RETURN t_cost IS 
    v_explanation          VARCHAR2(4000);
    v_math_explanation     VARCHAR2(4000);
    v_pidm                 t_pidm;
    v_reg_copies           INT;            --regular copies
    v_ttl_copies           INT;            --total copies
    v_exp_ship_requests    INT;
    v_dm_cost              ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_dm_ttl_copies        INT;
    v_dm_explanation       VARCHAR2(4000);
    v_first_copy_cost      ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_subsequent_copy_cost ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_copies_cost          ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_exp_shipping_cost    ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_total_shipping_cost  ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_total_cost           ZTRANSCRIPT_REQUEST.requests.rq_cost%TYPE;
    v_cost                 t_cost;
    ex_copies_err          EXCEPTION;
    CURSOR c_dm_specs IS SELECT dm.dm_desc dm_desc,
                                SUM(CASE WHEN dm.dm_cost_override IS NOT NULL THEN ds.ds_copies_requested END) dm_copies,
                                dm.dm_cost_override dm_cost
                           FROM ztranscript_request.requests rq
                           JOIN ztranscript_request.destinations ds
                             ON ds.ds_rq_id = rq.rq_id
                           JOIN ztranscript_request.delivery_method dm
                             ON dm.dm_code = ds.ds_dm_code
                            AND dm.dm_cost_override IS NOT NULL
                          WHERE rq.rq_id = p_pk
                          GROUP BY dm.dm_desc, 
                                   dm_cost_override;
    v_dm_specs c_dm_specs%ROWTYPE;
    PROCEDURE p_invalid_text IS 
        v_message VARCHAR2(4000);
      BEGIN 
      --find the message that should display
        v_message := f_get_text_varchar('CALC_COST_ERROR_MESSAGE');
        p_log_error('f_payment_amount (p_pk => ' || p_pk || ') returned ' || v_message || ' and ');
      --display the message
        dbms_output.put_line(v_message);
      END;
  BEGIN
  --log
    p_log_action('f_payment_amount (p_pk => ' || p_pk || ')', 30);
  --if the cost has been overridden, use it
  IF ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC.f_cost_overridden(p_rq_id => p_pk) = 'Y'
    THEN SELECT rq.rq_cost
           INTO v_total_cost
           FROM ztranscript_request.requests rq
          WHERE rq.rq_id = p_pk;
         v_explanation := 'The Registrar''s Office has overridden the cost of your transcript request, and set it to $' 
                          || v_total_cost || '. If you have any questions about this, please contact them.';
    ELSE --find the pidm, the number of standard-price copies they requested, and the amount of expedited shipping requests they have. 
          BEGIN 
            SELECT rq.rq_pidm,
                   nvl(SUM(CASE WHEN dm.dm_cost_override IS NULL THEN ds.ds_copies_requested END), 0),  --if the delivery method has a cost override that will be calculated below; ignore them for now. 
                   nvl(SUM(ds.ds_copies_requested), 0),
                   nvl(COUNT(CASE WHEN ds.ds_exp_ship_ind = 'Y' THEN 'X' END), 0)
              INTO v_pidm,
                   v_reg_copies,
                   v_ttl_copies,
                   v_exp_ship_requests
              FROM ztranscript_request.requests rq
              JOIN ztranscript_request.destinations ds
                ON ds.ds_rq_id = rq.rq_id
              JOIN ztranscript_request.delivery_method dm
                ON dm.dm_code = ds.ds_dm_code
             WHERE rq.rq_id = p_pk
             GROUP BY rq.rq_pidm,
                      rq.rq_cost; 
            v_explanation := CASE WHEN v_ttl_copies > 1 
                                       THEN 'You requested ' || v_ttl_copies || ' copies of your transcript. ' || '<p>'
                                            END;
          --take into account if something goes wrong and there's null or zero copies
            IF (v_ttl_copies IS NULL OR v_ttl_copies = 0) --I've setup the form to where this shouldn't happen, but just in case ...
              THEN RAISE EX_COPIES_ERR;
            END IF;
            EXCEPTION 
              WHEN EX_COPIES_ERR
                THEN dbms_output.put_line('You must request at least one copy to complete the form. Please add a destination.');
                     raise_application_error( -20001, 'Invalid Copy Request' );
              WHEN OTHERS
                THEN p_log_error('f_payment_amount (p_pk => ' || p_pk || ')');
                     RAISE;
          END;
        --if the requester should pay the costs
          IF f_fee_waiver(p_pidm => v_pidm) = 'N'
            THEN --find the standard costs
                 IF v_reg_copies > 0
                   THEN BEGIN
                         --cost of the first transcript ordered
                           v_first_copy_cost := f_get_text_number('FIRST_TRANSCRIPT_COST');
                         --cost of any additional transcript ordered beyond the first 
                           v_subsequent_copy_cost := f_get_text_number('SUBSEQUENT_TRANSCRIPTS_COST');
                         --find their cost based on the amount of copies they requested
                           v_copies_cost := ((CASE WHEN v_reg_copies > 0 THEN v_first_copy_cost END) + ((v_reg_copies - 1) * v_subsequent_copy_cost));
                           v_explanation := v_explanation ||
                                            CASE WHEN v_reg_copies = 1 
                                                      THEN 'You requested one copy at the standard rate, which costs $' || v_first_copy_cost || '. '
                                                 WHEN v_reg_copies = 2
                                                      THEN 'The first copy at the standard rate costs $' || v_first_copy_cost 
                                                           || '. You requested 1 additional copy, which costs $' || v_subsequent_copy_cost || '. '
                                                 WHEN v_reg_copies > 2
                                                      THEN 'The first copy at the standard rate costs $' || v_first_copy_cost || '. The ' || (v_reg_copies - 1) || 
                                                           ' additional copies cost $' || v_subsequent_copy_cost || ' apiece. '
                                                           END;
                           v_math_explanation := v_first_copy_cost || 
                                                 CASE WHEN v_reg_copies > 1
                                                           THEN ' + (' || (v_reg_copies - 1) || ' * ' || v_subsequent_copy_cost || ') '
                                                                END;
                           --if an expected error, inform the user
                           EXCEPTION
                             WHEN NO_DATA_FOUND
                               THEN --display the custom message for calculating transcript cost and the standard sql error message
                                    p_invalid_text;
                                    dbms_output.put_line(SQLERRM);
                                    RAISE;
                             WHEN INVALID_NUMBER
                               THEN --display the custom message for calculating transcript cost and the standard sql error message
                                    p_invalid_text;
                                    dbms_output.put_line(SQLERRM);
                                    RAISE;
                             WHEN OTHERS
                               THEN p_log_error('f_payment_amount (p_pk => ' || p_pk || ')');
                                    RAISE;
                         END;
                 END IF;
               --Then find their delivery method-specific costs
                 v_dm_cost := 0;  --set cost to 0 to begin, or if there are no costs for it.
                 v_dm_ttl_copies := 0; --set total copies to 0 to begin, or if there end up being none.
                 FOR v_dm_specs IN c_dm_specs
                   LOOP 
                   --calculate the delivery method-specific costs
                     v_dm_cost := v_dm_cost + NVL((v_dm_specs.dm_copies * v_dm_specs.dm_cost), 0);
                     v_dm_ttl_copies := v_dm_ttl_copies + nvl(v_dm_specs.dm_copies, 0);
                   --create an explanation for this calculation
                     v_dm_explanation := CASE WHEN v_dm_explanation IS NOT NULL
                                                   THEN v_dm_explanation || '<p>'
                                                        END || 
                                         'You requested ' ||
                                         CASE WHEN v_dm_specs.dm_copies = 1
                                                   THEN '1 copy '
                                              WHEN v_dm_specs.dm_copies > 1
                                                   THEN v_dm_specs.dm_copies || ' copies '
                                                        END  || 
                                         ' via the "' || v_dm_specs.dm_desc || '" delivery method. At the rate of $' ||
                                         v_dm_specs.dm_cost || ' per copy, that is $' || (v_dm_specs.dm_copies * v_dm_specs.dm_cost) || 
                                         '. ';
                     v_math_explanation := CASE WHEN v_math_explanation IS NOT NULL 
                                                     THEN v_math_explanation || ' + ' 
                                                          END || 
                                           '(' || v_dm_specs.dm_copies || ' * ' || v_dm_specs.dm_cost || ')';
                   END LOOP;
                 IF v_dm_ttl_copies > 1
                   THEN v_dm_explanation := v_dm_explanation || 'This accounts for ' || v_dm_ttl_copies 
                                            || ' of the copies you requested for a total of $' || v_dm_cost || '. <p>';
                 END IF;
            v_explanation := CASE WHEN v_explanation IS NOT NULL AND v_dm_explanation IS NOT NULL
                                       THEN v_explanation || '<p>' || v_dm_explanation
                                            ELSE v_explanation || '<p>' || v_dm_explanation END;
            ELSE v_copies_cost := 0;
                 v_dm_cost     := 0; 
                 v_explanation := 'Your standard transcript costs have been waived.';
          END IF;
        --log the cost of the copies
          p_log_action('f_payment_amount (p_pk => ' || p_pk || ') cost of copies is ' || v_copies_cost || ' and total copies comes to ' || v_reg_copies, 40);
        --log the dm-specific 
          p_log_action('f_payment_amount (p_pk => ' || p_pk || ') delivery method specific costs are ' || v_dm_cost || ' and the total standard copies requested is ' || v_dm_ttl_copies, 40);
        --find the amount of expedited shipping they should pay
          IF v_exp_ship_requests > 0
            THEN BEGIN
                  --cost of each expedited shipping request
                    v_exp_shipping_cost := f_get_text_number('EXPEDITED_SHIPPING_COST');
                  --calculate the total cost of their shipping based on how many expedited shipping requests they had
                    v_total_shipping_cost := (v_exp_ship_requests * v_exp_shipping_cost);
                  --log the cost of expedited shipping
                    p_log_action('f_payment_amount (p_pk => ' || p_pk || ') cost of shipping is ' || v_total_shipping_cost, 40);
                    v_explanation := v_explanation || '<p>' || 
                                     CASE WHEN v_exp_ship_requests = 1 
                                               THEN 'Finally, you requested Expedited Shipping for one destination. This has a cost of $' 
                                                    || to_char(v_exp_shipping_cost) || '. '
                                          WHEN v_exp_ship_requests > 1 
                                               THEN 'Finally, you requested ' || to_char(v_exp_ship_requests) || 
                                                    ' destinations be sent with Expedited Shipping. This has a cost of $'
                                                    || to_char(v_exp_shipping_cost) || ' for each destination for a total of $' 
                                                    || to_char(v_total_shipping_cost) || '. '
                                                    END;
                    v_math_explanation := CASE WHEN v_math_explanation IS NOT NULL 
                                                    THEN v_math_explanation || ' + ' 
                                                         END || 
                                          CASE WHEN v_exp_ship_requests = 1 
                                                    THEN to_char(v_exp_shipping_cost)
                                               WHEN v_exp_ship_requests > 1 
                                                    THEN '(' || to_char(v_exp_ship_requests) || ' * ' || to_char(v_exp_shipping_cost) || ')'
                                                         END;
                  --if an expected error, inform the user
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                        THEN --display the custom message for calculating transcript cost and the standard sql error message
                             p_invalid_text;
                             dbms_output.put_line(SQLERRM);
                             RAISE;
                      WHEN INVALID_NUMBER
                        THEN --display the custom message for calculating transcript cost and the standard sql error message
                             p_invalid_text;
                             dbms_output.put_line(SQLERRM);
                             RAISE;
                      WHEN OTHERS
                        THEN p_log_error('f_payment_amount (p_pk => ' || p_pk || ')');
                             RAISE;
                   END;
            ELSE v_total_shipping_cost := 0;
          END IF;
          --log the cost of shipping
          p_log_action('f_payment_amount (p_pk => ' || p_pk || ') cost of shipping is ' || v_total_shipping_cost || ' and the number of expedited shipping requests is ' || v_exp_ship_requests, 40);
        --find the total amount they should pay
          v_total_cost       := (nvl(v_copies_cost,0) + nvl(v_dm_cost,0) + nvl(v_total_shipping_cost,0));
          v_explanation      := v_explanation || 
                                CASE WHEN (nvl(v_dm_ttl_copies,0) + nvl(v_exp_ship_requests,0) + nvl(v_reg_copies,0)) > 1
                                          THEN '<p>' || 'The total comes to $' || v_total_cost || '.'
                                               END;
          v_math_explanation := CASE WHEN v_math_explanation IS NOT NULL AND length(v_math_explanation) > 4  --make sure there's a value here and a formuale with it. otherwise there's no point in showing the calculation
                                          THEN ' <br> Calculation: ' || v_math_explanation || ' = ' || v_total_cost
                                               ELSE NULL END;     --if it doesn't meet the criteria, set this to null so no calculations show (it'd be funny to show "Calculation: 10=10" on a page).
    END IF;
  --create the record to return
    SELECT v_total_cost,
           v_total_cost - nvl(v_total_shipping_cost,0), -- non-shipping costs
           nvl(v_total_shipping_cost,0),                -- shipping costs
           v_explanation,
           v_math_explanation
      INTO v_cost
      FROM dual;
  --update the record so there's no confusion
    UPDATE ztranscript_request.requests rq
       SET rq.rq_cost = v_total_cost
     WHERE rq.rq_id = p_pk;
  --log what's being returned and return it
    p_log_action('f_payment_amount (p_pk => ' || p_pk || ') total cost is ' || v_total_cost, 40);
    RETURN v_cost;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_payment_amount (p_pk => ' || p_pk || ')');
             RAISE;
  END f_payment_amount;
  
  /**********************************************************************************************************************
  * JanFeb 2019 Updates: added
  * this creates a table from all the request IDs selected on the Processing page so they can be queried in a cursor.
  * 
  **********************************************************************************************************************/
  FUNCTION f_printing_prep_req_ids (p_rq_id  IN  t_pk DEFAULT NULL) RETURN t_pk_tab PIPELINED IS 
   v_pk     t_pk;
   v_pk_rec t_pk_rec;
   v_rows   INT          := 0;
  BEGIN
  --log
    p_log_action('f_prep_rq_ids_for_printing (p_rq_id => ' || p_rq_id || ')', 30);
  -- if there's a value for p_rq_id, use that. 
    IF p_rq_id IS NOT NULL THEN 
       v_pk_rec.pk := p_rq_id;
       PIPE ROW (v_pk_rec);
      v_rows := 1;
  -- Otherwise, assume it is checkboxes in the apex form. 
    ELSE 
       FOR i IN 1..APEX_APPLICATION.G_F01.COUNT LOOP
         v_pk := APEX_APPLICATION.G_F01(i);
         v_pk_rec.pk := v_pk;
         PIPE ROW(v_pk_rec);
         v_pk_rec := NULL;
         p_log_action('f_prep_rq_ids_for_printing (p_rq_id => ' || p_rq_id || ') piped ' || v_pk || '. ', 30);
         v_rows := v_rows + 1;
       END LOOP;
     END IF;
    RETURN; 
    p_log_action('f_prep_rq_ids_for_printing (p_rq_id => ' || p_rq_id || ') piped ' || v_rows || ' row.', 40);
  EXCEPTION WHEN OTHERS THEN 
    p_log_error('f_prep_rq_ids_for_printing (p_rq_id => ' || p_rq_id || ')');
    RAISE;
  END f_printing_prep_req_ids;
   
  /**********************************************************************************************************************
  * function that determines if the continue button on page 4 should show
  * 
  **********************************************************************************************************************/
  FUNCTION f_ready_to_pay (p_rq_id IN t_pk) RETURN BOOLEAN IS 
    v_destinations_ttl INT;
    v_can_edit         VARCHAR2(1);
  BEGIN
  --log
    p_log_action('f_ready_to_pay (p_rq_id => ' || p_rq_id || ')', 30);
  --see how many destinations are listed
    SELECT COUNT('X')
      INTO v_destinations_ttl
      FROM ztranscript_request.destinations ds
     WHERE ds.ds_rq_id = p_rq_id;
  --see if the request is editable
    v_can_edit := f_can_edit(p_pk => p_rq_id);
  --based on these two, see if the continue button should show
    IF (v_destinations_ttl > 0 AND v_can_edit = 'Y')
      THEN p_log_action('f_ready_to_pay (p_rq_id => ' || p_rq_id || ') returned TRUE.', 40);     
           RETURN TRUE;
      ELSE p_log_action('f_ready_to_pay (p_rq_id => ' || p_rq_id || ') returned FALSE.', 40);     
           RETURN FALSE;
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_ready_to_pay (p_rq_id => ' || p_rq_id || ')');
             RAISE;
  END f_ready_to_pay;
        
  /**********************************************************************************************************************
  * function that finds how many (if any) destinations a request has
  * 
  **********************************************************************************************************************/
  FUNCTION f_req_ttl_destinations (p_pk IN t_pk) RETURN INT IS 
    v_ttl INT;
  BEGIN
  --log
    p_log_action('f_req_ttl_destinations (p_pk => ' || p_pk || ')', 30);
  --find out how many destinations this request has
    SELECT COUNT('X')
      INTO v_ttl
      FROM ztranscript_request.destinations ds
     WHERE ds.ds_rq_id = p_pk;
  --log the return and return it
    p_log_action('f_req_ttl_destinations (p_pk => ' || p_pk || ') returned ' || v_ttl, 40);
    RETURN v_ttl;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_req_ttl_destinations (p_pk => ' || p_pk || ')');
             RAISE;
  END f_req_ttl_destinations;

  /**********************************************************************************************************************
  * function that determines if the address entered for the destination is not a PO Box or an APO address (if it's a standard address)
  * 
  **********************************************************************************************************************/
  FUNCTION f_standard_address (p_dm_code IN t_dm,
                               p_addr_line1 IN VARCHAR2,
                               p_addr_line2 IN VARCHAR2) RETURN VARCHAR2 IS 
                            
  BEGIN
  --log
    p_log_action('f_standard_address (p_dm_code => '    || p_dm_code    || 
                                    ',p_addr_line1 => ' || p_addr_line1 || 
                                    ',p_addr_line2 => ' || p_addr_line2 || ')', 30);
  --if the delivery method is one that requires the student to enter an address, check it to ensure.
    IF (upper(p_addr_line1) LIKE '%APO%' 
        OR upper(p_addr_line2) LIKE '%APO%' 
        OR regexp_like(p_addr_line1, 'P(.{0,3})O(.{0,3})BOX','i') 
        OR regexp_like(p_addr_line2, 'P(.{0,3})O(.{0,3})BOX','i'))
      THEN --log it and return false
           p_log_action('f_standard_address (p_dm_code => '    || p_dm_code    || 
                                           ',p_addr_line1 => ' || p_addr_line1 || 
                                           ',p_addr_line2 => ' || p_addr_line2 || ') returned FALSE.', 40);
           RETURN 'N';
      ELSE --log it and return false
           p_log_action('f_standard_address (p_dm_code => '    || p_dm_code    || 
                                           ',p_addr_line1 => ' || p_addr_line1 || 
                                           ',p_addr_line2 => ' || p_addr_line2 || ') returned TRUE.', 40);
           RETURN 'Y';
    END IF;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_standard_address (p_dm_code => '    || p_dm_code    || 
                                            ',p_addr_line1 => ' || p_addr_line1 || 
                                            ',p_addr_line2 => ' || p_addr_line2 || ')');
             RAISE;
  END f_standard_address;
        
  /**********************************************************************************************************************
  * function that finds how many (if any) requests a student has
  * 
  **********************************************************************************************************************/
  FUNCTION f_stu_ttl_requests (p_pidm IN t_pidm) RETURN INT IS 
    v_ttl INT;
  BEGIN
  --log
    p_log_action('f_stu_ttl_requests (p_pidm => ' || p_pidm || ')', 30);
  --find out how many requests this student has
    SELECT COUNT('X')
      INTO v_ttl
      FROM ztranscript_request.requests rq
     WHERE rq.rq_pidm = p_pidm;
  --log the return and return it
    p_log_action('f_stu_ttl_requests (p_pidm => ' || p_pidm || ') returned ' || v_ttl, 40);
    RETURN v_ttl;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_stu_ttl_requests (p_pidm => ' || p_pidm || ')');
             RAISE;
  END f_stu_ttl_requests;
  
  /**********************************************************************************************************************
  * function that ensures everything is submitted correctly in the destination form (apex 1214, page 5)
  * 
  **********************************************************************************************************************/
  FUNCTION f_submit_destination (p_message_out OUT VARCHAR2) RETURN BOOLEAN IS 
    v_dm_hs_record   t_dm_hide_show;
    v_doc_upload     INT;
    v_return_message VARCHAR2(4000);
    v_result         BOOLEAN;
  BEGIN
  --log
    p_log_action('f_submit_destination', 30);
  --find out what should be completed (big picture) based on DM code
    v_dm_hs_record := f_dm_hide_show(p_dm_code => v('P5_DS_DM_CODE'));
  --find out what items should be cleared based on what should show (in case they filled out something, then went back up and changed the delivery method but the previous entry is still saved in the session)
    IF v_dm_hs_record.cmmt != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_COMMENTS',NULL);
    END IF;
    IF v_dm_hs_record.atch != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_DOC_ID',NULL);
    END IF;
    IF v_dm_hs_record.exsp != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_EXP_SHIP_IND',NULL);
    END IF;
    IF v_dm_hs_record.emal != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_EMAIL_ADDRESS',NULL);
      ELSE IF (v('P5_DS_EMAIL_ADDRESS') IS NULL OR v('P5_DS_EMAIL_ADDRESS') NOT LIKE '%@%.%')
             THEN p_log_action('f_submit_destination found an invalid e-mail address', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('EMAIL_ADDR_ERROR');
                  v_result := FALSE;
           END IF;
           --if they selected a liberty e-mail address but didn't type it in right
           IF (v('P5_DS_DM_CODE') = 'LBTY' AND upper(v('P5_DS_EMAIL_ADDRESS')) NOT LIKE '%@LIBERTY.EDU')
             THEN p_log_action('f_submit_destination found an invalid liberty e-mail address (since this is a liberty internal request)', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('LIBERTY_EMAIL_ADDR_ERROR');
                  v_result := FALSE;
           END IF;
           --if they didn't select a liberty e-mail delivery method but typed in a liberty e-mail
           IF (v('P5_DS_DM_CODE') != 'LBTY' AND upper(v('P5_DS_EMAIL_ADDRESS')) LIKE '%@LIBERTY.EDU')
             THEN p_log_action('f_submit_destination found a liberty e-mail address but Liberty was not selected as the destination', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('LIBERTY_EMAIL_WRONG_DM_ERROR');
                  v_result := FALSE;
           END IF;
    END IF;
    IF v_dm_hs_record.coll != 'Y'
      THEN IF (v_dm_hs_record.addr = 'Y' AND (upper(v('P5_DS_ADDR_NAME')) LIKE '%COLLEGE%' OR upper(v('P5_DS_ADDR_NAME')) LIKE '%UNIVERSITY%'))
              THEN p_log_action('f_submit_destination found a college address but the college was not selected', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('COLL_ADDR_AND_DM_NOT_COLL');
                  v_result := FALSE;
              ELSE APEX_UTIL.SET_SESSION_STATE('P5_DS_SBGI_CODE',NULL);
           END IF;
      ELSE IF v('P5_DS_SBGI_CODE') IS NULL AND v_dm_hs_record.emal != 'Y'
             THEN p_log_action('f_submit_destination found null SBGI code', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NULL_SBGI_CODE_ERROR');
                  v_result := FALSE;
           END IF;
           IF (v_dm_hs_record.emal = 'Y' AND v('P5_DS_SBGI_CODE') IS NULL AND upper(v('P5_DS_EMAIL_ADDRESS')) LIKE '%.EDU' AND upper(v('P5_DS_EMAIL_ADDRESS')) NOT LIKE '%@LIBERTY.EDU')
             THEN p_log_action('f_submit_destination found ".edu" e-mail without a college selected', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NO_COLL_EDU_EMAIL');
                  v_result := FALSE;
           END IF;
    END IF;
    IF v_dm_hs_record.addr != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NAME',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE1',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_LINE2',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_CITY',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_STAT_CODE',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_ZIP',NULL);
           APEX_UTIL.SET_SESSION_STATE('P5_DS_ADDR_NATN_CODE',NULL);
      --ensure they didn't select both expedited shipping and a non-us address, or exp ship and a PO Box/APO address
      ELSE IF (v('P5_DS_EXP_SHIP_IND') = 'Y' AND (v('P5_DS_ADDR_NATN_CODE') != 'US' OR f_standard_address(p_dm_code    => v('P5_DS_DM_CODE'),     --if this isn't a college, check for a PO Box/APO address
                                                                                                          p_addr_line1 => v('P5_DS_ADDR_LINE1'),
                                                                                                          p_addr_line2 => v('P5_DS_ADDR_LINE2')) = 'N'))
             THEN p_log_action('f_submit_destination found a non-US address or a PO Box/APO address with an expedited shipping request.', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('EXP_SHIP_ERROR');
                  v_result := FALSE;
           END IF;
           IF (v('P5_DS_ADDR_NAME') IS NULL 
               OR v('P5_DS_ADDR_LINE1') || v('P5_DS_ADDR_LINE2') IS NULL 
               OR v('P5_DS_ADDR_CITY') IS NULL 
               OR v('P5_DS_ADDR_STAT_CODE') IS NULL 
               OR v('P5_DS_ADDR_ZIP') IS NULL 
               OR v('P5_DS_ADDR_NATN_CODE') IS NULL)
             THEN p_log_action('f_submit_destination found null Address.', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NULL_ADDRESS_ERROR');
                  v_result := FALSE;
           END IF;
    END IF;
    IF v_dm_hs_record.cpys != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_COPIES_REQUESTED',NULL);
      ELSE IF v('P5_DS_COPIES_REQUESTED') IS NULL
             THEN p_log_action('f_submit_destination found null value for the amount of copies requested.', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NULL_COPIES_REQUESTED_ERROR');
                  v_result := FALSE;
           END IF;
    END IF;
    IF v_dm_hs_record.escp != 'Y'
      THEN APEX_UTIL.SET_SESSION_STATE('P5_DS_EL_ID',NULL);
      -- must have a value in the department.
      ELSE IF v('P5_DS_EL_ID') IS NULL
             THEN p_log_action('f_submit_destination found null value for Escrip Department when that should be entered.', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NO_ESCRIP_DEPT_ERROR');
                  v_result := FALSE;
           END IF;
           -- must select a school currently using Escrip
           IF ztranscript_request.apex_app_logic.f_is_escrip(p_sbgi_code => v('P5_DS_SBGI_CODE')) = 'Y' THEN
              NULL;
           ELSE p_log_action('f_submit_destination found that the student selected to send an Escrip to a school that does not use Escrip.', 40);
                v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('NON_ESCRIP_SCHOOL_ERROR');
                v_result := FALSE;
           END IF;
    END IF;
    IF v('P5_DS_DM_CODE') IN ('USML','USTR')
      THEN SELECT count(*)
             INTO v_doc_upload
             from (SELECT 'X'
                   FROM ZAPEX.WWV_FLOW_FILE_OBJECTS$ apexy 
                   JOIN zgeneral.zgrmime mime ON apexy.mime_type = mime.zgrmime_mime_type
                  WHERE zgrmime_file_type IN ('PDF')
                    AND flow_id = v('APP_ID') and name = v('P5_DOC_UPLOAD')
                  UNION ALL
                 SELECT 'X'
                   FROM dual
                  WHERE v('P5_DOC_UPLOAD') IS NULL);
           IF v_doc_upload = 0
             THEN p_log_action('f_submit_destination found a file that is not a .pdf.', 40);
                  v_return_message := CASE WHEN v_return_message IS NOT NULL THEN v_return_message || '<br>' END || f_get_text_varchar('PDF_UPLOAD_ERROR');
                  v_result := FALSE;
           END IF;
    END IF;
    IF v_result IS NULL
      THEN v_result := TRUE;
           p_log_action('f_submit_destination returned TRUE', 40);
      ELSE p_log_action('f_submit_destination returned FALSE', 40);
    END IF;
    p_message_out := v_return_message;
    RETURN v_result;
    EXCEPTION
      WHEN OTHERS
        THEN p_log_error('f_submit_destination');
             RAISE;
  END f_submit_destination;
    
  /**********************************************************************************************************************
  * function that retrieves the tprt_code for this destination
  * added in JanFeb 2019 updates for printing
  * 
  **********************************************************************************************************************/
  FUNCTION f_tprt_code (p_ds_id   IN  t_pk) return t_tprt_code IS
    v_tprt_code t_tprt_code;
  BEGIN
    SELECT dm.dm_tprt_code 
      INTO v_tprt_code
      FROM ztranscript_request.destinations ds
      JOIN ztranscript_request.delivery_method dm
        ON dm.dm_code = ds.ds_dm_code
     WHERE ds.ds_id = p_ds_id;
    RETURN v_tprt_code;
  EXCEPTION WHEN NO_DATA_FOUND THEN
                 RETURN NULL;
            WHEN OTHERS THEN
                 RAISE;
  END f_tprt_code;  
  /**********************************************************************************************************************
  * function that validates an escrip address entry
  * added in JanFeb 2019 updates for printing
  * 
  **********************************************************************************************************************/
  /*FUNCTION f_validate_escrip_addr (p_sbgi_code        t_sbgi, 
                                   p_escrip_city      t_city,
                                   p_escrip_stat_code t_stat) 
                                   RETURN VARCHAR2 IS 
    l_sbgi_city_out     VARCHAR2(4000);
    l_sbgi_stat_out     VARCHAR2(4000);
    l_sbgi_code_out     VARCHAR2(4000);
    l_message_out       VARCHAR2(4000) := NULL;
  BEGIN
    -- log
    
    ...
    
    -- look for an exact match (city and state)
    SELECT s.sobsbgi_city, 
           s.sobsbgi_stat_code
      INTO l_sbgi_city_out, 
           l_sbgi_stat_out
      FROM saturn.sobsbgi s
     WHERE s.sobsbgi_sbgi_code = p_sbgi_code
       AND s.sobsbgi_city      = p_escrip_city
       AND s.sobsbgi_stat_code = p_escrip_stat_code
     FETCH FIRST ROW ONLY;
   --if the above ran without error, then there is nothing to do (l_message_out will return as null and no message will display)
  EXCEPTION WHEN NO_DATA_FOUND THEN 
  -- if there was no match for city and state, ...
    BEGIN 
      -- ... then look for a match in the same state but not city, and ...
      SELECT listagg(s.sobsbgi_city, ', ') within GROUP (ORDER BY s.sobsbgi_city), 
             s.sobsbgi_stat_code
        INTO l_sbgi_city_out, 
             l_sbgi_stat_out
        FROM saturn.sobsbgi s
       WHERE s.sobsbgi_sbgi_code = p_sbgi_code
         AND s.sobsbgi_stat_code = p_escrip_stat_code
       GROUP BY s.sobsbgi_stat_code;
      -- if a match was found for the state (but not for the city), 
      -- lookup the output message and concatenate it to the cities found
      l_message_out := f_get_text_varchar('EAB_STAT_NOT_CITY') || l_sbgi_city_out;      
      -- if there was no_data_found, ignore it
    EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN 
      -- ... look for a similar named college in the same city and state
      NULL;
    EXCEPTION WHEN ??? THEN 
      ???;
    END;
  END f_validate_escrip_addr;*/
  
END APEX_APP_LOGIC;
/
