 -- TKT0389997 create prints table for transcript request form

CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','zbtm.terms_by_group_v');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.ssbsect');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.sfrstcr');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvterm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtckn');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtckg');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtckl');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtrce');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.scbcrse');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrgrde');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtrit');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvsbgi');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvrsts');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.sobptrm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvlevl');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvmajr');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvdegc');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtgpa');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrlgpa');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrdgmr');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvcamp');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','general.gtvprnt');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shttran');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvtprt');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvdegs');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrttrm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvastd');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrttcm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.shrtmcm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.gz_student_utils');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvterm');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','general.gtvprnt');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvtprt');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','EXECUTE','ZEXEC.ZBTM_TERM');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT,INSERT','saturn.SHTTRTC');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT,INSERT,DELETE','general.GJBPDFT'); 
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','BANSECR.GUBIPRF');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','EXECUTE','ZEXEC.GZ_JOBSUB');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT,REFERENCES','saturn.stvtprt');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','general.goremal');
CALL sys.dba_assist.grant_obj_perms('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvhldd');


--insert for default printer
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_number, tx_last_activity_user, tx_last_activity)
VALUES ('Primary Printer','The first printer in the drop-down list', 'Banner_PR000096',1, USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_number, tx_last_activity_user, tx_last_activity)
VALUES ('Secondary Printer','The second printer in the drop-down list', 'Banner_PR000034',2, USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_number, tx_last_activity_user, tx_last_activity)
VALUES ('Tertiary Printer','The third printer in the drop-down list', 'Escrip_printer',3, USER, SYSDATE);
COMMIT;
 
CREATE TABLE ztranscript_request.prints
(prnt_id INT GENERATED ALWAYS AS IDENTITY, 
 prnt_time TIMESTAMP DEFAULT SYSDATE, 
 prnt_user VARCHAR2(30) DEFAULT USER, 
 prnt_notes CLOB,
 CONSTRAINT prnt_pk PRIMARY KEY (prnt_id));
 
COMMENT ON TABLE ztranscript_request.prints IS 'table recording each time the "Print" button is clicked in the APEX transcript Request form (1214).';
COMMENT ON COLUMN ztranscript_request.prints.prnt_id IS 'primary key';
COMMENT ON COLUMN ztranscript_request.prints.prnt_time IS 'time that the button was clicked';
COMMENT ON COLUMN ztranscript_request.prints.prnt_user IS 'user who clicked the print button';
COMMENT ON COLUMN ztranscript_request.prints.prnt_notes IS 'any notes for the print attempt (what happened)';

CALL sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','ztranscript_request.prints');

CREATE TABLE ztranscript_request.escrip_locations 
(el_id            INT GENERATED ALWAYS AS IDENTITY CONSTRAINT el_pk PRIMARY KEY,
 el_sbgi_code     VARCHAR2(6)                      CONSTRAINT el_sbgi_code_nn NOT NULL,
 --el_name          VARCHAR2(250)                    CONSTRAINT el_name_nn NOT NULL,
 el_department    VARCHAR2(250)                    CONSTRAINT el_department_nn NOT NULL,
 el_city          VARCHAR2(50),
 el_stat_code     VARCHAR2(3),
 el_natn_code     VARCHAR2(5),
 el_active_ind    VARCHAR2(1)                      CONSTRAINT el_active_ind_nn NOT NULL,
 CONSTRAINT       el_unq            UNIQUE (el_sbgi_code,el_department),       --previously was (el_sbgi_code,el_name,el_department), but that's no longer needed since I'm using sbgi_desc instead of el_name. Even if the name is stvsbgi is slightly different than in escrip, the student's won't know and the RO can make the connection.
 CONSTRAINT       el_sbgi_code_fk   FOREIGN KEY (el_sbgi_code) REFERENCES saturn.stvsbgi (stvsbgi_code),
 CONSTRAINT       el_stat_code_fk   FOREIGN KEY (el_stat_code) REFERENCES saturn.stvstat (stvstat_code),
 CONSTRAINT       el_natn_code_fk   FOREIGN KEY (el_natn_code) REFERENCES saturn.stvnatn (stvnatn_code),
 CONSTRAINT       el_active_ind_ck  CHECK (el_active_ind IN ('Y','N'))
);
 
CREATE INDEX ztranscript_request.el_sbgi_code_indx ON ztranscript_request.escrip_locations (el_sbgi_code);
CREATE INDEX ztranscript_request.el_stat_code_indx ON ztranscript_request.escrip_locations (el_stat_code);
CREATE INDEX ztranscript_request.el_natn_code_indx ON ztranscript_request.escrip_locations (el_natn_code);

COMMENT ON TABLE ztranscript_request.escrip_locations IS 'Holds all escrip sending locations. Maintained by the Registrar''s Office in the APEX Transcript Request form (app id 1214).';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_id IS 'primary key. generated by identity';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_sbgi_code IS 'identifies the institution/school/source code. foreign key to saturn.stvsbgi (stvsbgi_code)';
--COMMENT ON COLUMN ztranscript_request.escrip_locations.el_name IS 'Name of the institution as listed in Escrip';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_department IS 'Name of the department/specific location of the institution.';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_city IS 'City of the department/specific location within the institution';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_stat_code IS 'State code of the department/specific location within the institution. Foreign key to saturn.stvstat (stvstat_code)';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_natn_code IS 'Nation code of the department/specific location within the institution. Foreign key to saturn.stvnatn (stvnatn_code)';
COMMENT ON COLUMN ztranscript_request.escrip_locations.el_active_ind IS 'Active Indicator. Y means this location is currently available in the form, N (or any other value) means it is not available currently. This should correspond to the current list of options in Escrip.';

CALL sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT,INSERT,UPDATE,DELETE','ztranscript_request.escrip_locations');

/*
ALTER TABLE ztranscript_request.requests
      ADD rq_levl_code VARCHAR2(2);
COMMENT ON COLUMN ztranscript_request.requests.rq_levl_code IS 'indicates the level that the student wants. This is a pseudo-foreign key to saturn.stvlevl (stvlevl_code), but not a true foreign key because the value AL refers to all levels for which the student has a transcript.';
*/
ALTER TABLE ztranscript_request.requests
      ADD rq_prnt_id INT;
ALTER TABLE ztranscript_request.requests
      ADD CONSTRAINT rq_prnt_id_fk FOREIGN KEY (rq_prnt_id) REFERENCES ztranscript_request.prints (prnt_id);
COMMENT ON COLUMN ztranscript_request.requests.rq_prnt_id IS 'Foreign key to ztranscript_request.prints (prnt_id). Shows what time the transcripts for this request were actually printed/sent to escrip (and who printed it).';
ALTER TABLE ztranscript_request.destinations
      ADD ds_el_id INT;
ALTER TABLE ztranscript_request.destinations
      ADD CONSTRAINT ds_el_id_fk FOREIGN KEY (ds_el_id) REFERENCES ztranscript_request.escrip_locations (el_id);
COMMENT ON COLUMN ztranscript_request.destinations.ds_el_id IS 'identifies the escrip location they want the transcript sent to. foreign key to ztranscript_request.escrip_locations (el_id).';

ALTER TABLE ztranscript_request.delivery_method 
      ADD dm_tprt_code VARCHAR2(4);
ALTER TABLE ztranscript_request.delivery_method 
      ADD CONSTRAINT dm_tprt_code_fk FOREIGN KEY (dm_tprt_code) REFERENCES saturn.stvtprt (stvtprt_code);
COMMENT ON COLUMN ztranscript_request.delivery_method.dm_tprt_code IS 'the tprt code associated with this delivery method. foreign key to saturn.stvtprt (stvtprt_code). This isn''t always the tprt code used, as it can be overridden';
ALTER TABLE ztranscript_request.delivery_method 
      ADD dm_escrip_ind VARCHAR(1);
ALTER TABLE ztranscript_request.delivery_method 
      ADD CONSTRAINT dm_escrip_ind_ck CHECK (dm_escrip_ind IN ('Y','N'));
COMMENT ON COLUMN ztranscript_request.delivery_method.dm_escrip_ind IS 'indicates whether or not the section for escrip information should display when the school is selected.';

-- update the records with these columns above, and then create the escrip destination type
UPDATE ztranscript_request.delivery_method dm
   SET dm.dm_tprt_code = 'OT', 
       dm.dm_escrip_ind = 'N';
ALTER TABLE ztranscript_request.delivery_method MODIFY dm_tprt_code VARCHAR2(4) CONSTRAINT dm_tprt_code_nn NOT NULL;
ALTER TABLE ztranscript_request.delivery_method MODIFY dm_escrip_ind VARCHAR2(1) CONSTRAINT dm_escrip_ind_nn NOT NULL;
UPDATE ztranscript_request.delivery_method dm
   SET dm.dm_tprt_code = 'ES'
 WHERE dm.dm_code = 'EMAL';
UPDATE ztranscript_request.delivery_method dm
   SET dm.dm_display_order = 80
 WHERE dm.dm_code = 'LBTY';
INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest,
       dm_active_ind, dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind,
       dm_cost_override, dm_data_origin, dm_created_by, dm_created_on, dm_tprt_code, dm_escrip_ind)
VALUES ('ESCP','Escrip', 5, NULL, 1, 'Y', 'N', 'Y', 'N', 'N', 'Y', 'N',NULL, 'DEPLOY', USER, SYSDATE, 'ES', 'Y');
COMMIT;

--select * from ztranscript_request.text where tx_item = 'NULL_COPIES_REQUESTED_ERROR'
-- inserts for escrip dm type instructions, the specific escrip item, and the escrip available notification, and the error message if it isn't complete
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('DM_ESCP_INSTRUCTIONS','Instructions - Adding Escrip Destinations','<p> Please select the College/Institution that you want to receive your transcripts, and then the specific department therein. An official transcript will be sent electronically and securely to that institution. </p>', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P5_ESCRIP_HELP','Help Text for "Escrip Department" field','Please select the specific department within the College/Institution that you want to receive your transcripts.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P5_ESCRIP_AVAIL_NOTIF','Escrip is Available Notification','The College/Institution you selected participates in the Escrip transcript delivery system. We can send transcripts to them electronically, securely, and quickly via Escrip. If you would like us to send transcripts to them via Escrip, please change your "Destination Type" to "Escrip". When the page refreshes, select the department you want to receive them.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('NO_ESCRIP_DEPT_ERROR','Error Message for not selecting an Escrip department','Since you selected Escrip as the delivery method, please select a specific department', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('NON_ESCRIP_SCHOOL_ERROR','Error Message for selecting a school that does not use Escrip','Since you selected Escrip as the delivery method, please select a school that uses Escrip. You can do this by clicking the arrow in the field beside "College/Institution", and then searching and finding a school. If your school does not appear, change the "Destination Type" to "E-mail" and enter the school''s e-mail address.', USER, SYSDATE);
COMMIT;       

-- insert for the notification if a student enters one of their e-mail addresses
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P5_STU_EMAIL_UT_NOTIF','Sending to a Personal Email means Unofficial Transcripts Notification','Sending transcripts to a personal e-mail address means that they will be considered Unofficial Transcripts once you receive them. If you plan on using these as Official Transcripts, please have them mailed to yourself (and keep them inside the envelope) or sent directly to the intended destination. If you want to view your transcripts, you can do in ASIST. Please go to MyLu.com and login to ASIST from there.', USER, SYSDATE);
COMMIT; 

-- insert for ENGR/MD hold notification
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P2_MD_HOLD_MESSAGE','MD Hold Message','At this time, you cannot use this form to request your transcripts. Please contact the Registrar''s Office directly in order to do so. You can call at (434) 592-5100 from 8:30am-4:30pm EST Monday-Friday (closed from 9:30-11:30 for Convocation on Mondays, Wednesdays, and Fridays). You can also e-mail via <a href="mailto:registrar@liberty.edu?subject=Transcript Request' || CHR(38) || 'body=Hello, <br> I would like to request my transcripts. When I went into the Transcript Request form, I was told that I could not use the form, and I needed to contact the Registrar''s Office directly in order to request them. I would like them sent to:">registrar@liberty.edu</a>.', USER, SYSDATE);
COMMIT; 

-- insert for ENGR/MD hold notification
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_INSTRUCTIONS','Transcript Page Instructions','Please enter a student ID and then type "Enter" or click "Open Transcript".', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_INCORRECT_ID','Transcript Page Invalid ID Message','The ID number entered is invalid. Please enter a valid student ID.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_NOT_ACCEPTED','Transcript Page Not Accepted Message',' has not yet been accepted.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_NO_CLASSES','Transcript Page No Courses Message', ' has not yet taken courses.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_NO_COMPLETED_TERM','Transcript Page No Term Message',' has not yet completed a term.', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P200_NO_GRAD_APP','Transcript Page No Graduation Application Message',' has not yet submitted a graduation application or graduated.', USER, SYSDATE);
COMMIT; 

INSERT INTO ztranscript_request.statuses (st_code, st_desc, st_in_processing_ind, st_final_ind, 
            st_delete_ind, st_can_edit_ind, st_data_origin, st_created_by, st_created_on)
       VALUES ('WT','Waiting on Processing','Y','N','N','N','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('WT_STATUS_DEF','Status Definition for Waiting on Processing','You have completed your request, and the Registrar''s Office will begin processing it within 1-2 business days.', USER, SYSDATE);
UPDATE        ztranscript_request.text t
   SET t.tx_text = 'The Registrar''s Office is processing your request, which usually takes 1-2 business days.'
 WHERE t.tx_item = 'PC_STATUS_DEF';
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('PP','WT','Y','N','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('WT','WT','N','Y','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('WT','PC','N','Y','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('WT','WP','Y','Y','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('WT','CM','Y','Y','DEPLOY',USER,SYSDATE);
INSERT INTO ztranscript_request.status_changes (sc_prev_sc_code, sc_new_sc_code, sc_email_ind,
            sc_mark_processor_ind, sc_data_origin, sc_created_by, sc_created_on)
       VALUES ('WT','CN','N','Y','DEPLOY',USER,SYSDATE);
UPDATE ztranscript_request.text t 
   SET t.tx_item = 'WT_EMAIL_TEXT'
 WHERE t.tx_item = 'PC_EMAIL_TEXT';
UPDATE ztranscript_request.status_changes sc
   SET sc.sc_email_ind = 'N'
 WHERE sc.sc_prev_sc_code = 'PP'
   AND sc.sc_new_sc_code = 'PC';
COMMIT; 

-- other text table inserts/updates
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_number, tx_last_activity_user, tx_last_activity)
       VALUES ('PC_WAIT_HOURS','Hours Requests stay "In Processing" before auto-changing',3, USER, SYSDATE);
UPDATE ztranscript_request.text t
   SET t.tx_text = 'Please select the Destination Type, then fill out the required fields.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>'
WHERE t.tx_item = 'P5_INSTRUCTIONS';
DELETE ztranscript_request.text t WHERE t.tx_item = 'P5_DELIVERY_METHOD_HELP';
INSERT INTO ztranscript_request.text  (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('DM_NULL_INSTRUCTIONS','Instructions for Destination Type drop-down',
                   'The options for Destination Type are as follows: <br> <ul>' ||
                   '<li> <b> Escrip </b> - A program that thousands of schools participate in to send transcripts electronically, securely, and quickly. This is the fastest way to have transcripts delivered. </li> ' || 
                   '<li> <b> College via Mail </b> - Mailing to another College or Academic Institution. Commonly takes 1-2 weeks for the school to receive your transcripts after your request is paid for. Expedited shipping is available. </li> ' || 
                   '<li> <b> US Mail (States) </b> - Mailing to a location in the 50 United States that is not a College or Academic Institution. Commonly takes 1-2 weeks for the location to receive your transcripts after your request is paid for. Expedited shipping is available. </li> ' || 
                   '<li> <b> Non-LU E-mail </b> - Sending transcripts to a non-Liberty e-mail address. Liberty will use Escrip to send transcripts securely and quickly to the desired e-mail address. This is also a fast way to send transcripts. </li> ' || 
                   '<li> <b> International Mail </b> - Mailing to a location not in the United States and not a College or Academic Institution. Commonly takes 2-3 weeks for the school to receive your transcripts after your request is paid for. Expedited shipping is <b> NOT </b> available. </li> ' || 
                   '<li> <b> Pickup on Campus </b> - Transcripts will be left in the Student Service Center within 3 business days for you to pickup at your convenience. You will be required to show ID when picking them up.</li> ' || 
                   '<li> <b> US Mail (Non-States) </b> - Mailing to a location in the United States that is not a College or Academic Institution, and not one of the 50 States. Commonly takes 2-3 weeks for the school to receive your transcripts after your request is paid for. Expedited shipping is <b> NOT </b> available. </li> ' || 
                   '<li> <b> LU E-mail </b> - Sending transcripts as an attachment to a Liberty e-mail address. The transcripts will be considered Unofficial Transcripts once the e-mail is opened. This method is very rarely used. If faculty or staff have asked to see your transcripts, please have the contact the Registrar''s Office.  </li> ' || 
                   '</table>',USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P910_INSTRUCTIONS','"In Processing" page instructions','This page is designed to walk you through the entire process quickly and easily. <ul>' ||
               '<li> <b> Primary Report </b> - A list of all transcripts currently being processed or ready for it. This includes "Waiting on Processing", "In Processing", and "Waiting on Pickup". </li>' || 
               '<li> <b> Potential Escrip </b> - A list of all destinations that could be changed to an Escrip request. Please change them before printing if you want them to be sent through Escrip. You can do this by clicking the pencil, then finding the destination among the list, then clicking that pencil. On that page, you can edit the College/Institution and the Escrip Location </li>' || 
               '<li> <b> Transcript to Print </b> - A list of all requests not yet printed. Use this to print Transcripts. Select all the desired checkboxes, then click the "Print" button. If you want to print all at once, please change the rows to show a high number or all (be aware that this may slow the page down). </li>' || 
               '<li> <b> Addresses to Print </b> - A list of all recently printed requests that need to be put in an envelope. This allows easily getting all addresses to print. It shows the addressee (and address if necessary) in the "Print Envelope Address" column. This column formats correctly if you go to Actions -> Download -> CSV. </li>' || 
               '<li> <b> Attachments to Print </b> - A list of all recently printed requests with attachments, and a link to download their attachment.  </li>' || 
               '<li> <b> Stuffing Envelopes </b> - A list of all recently printed requests with the envelope address, number of copies to put inside, attachment name, and whether it has expedited shipping. </li>' || 
               '<li> <b> Transcripts for SSC </b> - A list of all recently printed requests that need to be taken to the Student Service Center for pickup. </li>' || 
               '<li> <b> Escrips to Send </b> - A list of all recently printed requests that need to be sent via Escrip. </li>' || 
               '<li> <b> LU E-mails to Send </b> - A list of all recently printed requests that need to be sent to a Liberty E-mail address. </li>' || 
               '<li> <b> Recently Printed </b> - A list of all recently printed requests. For final checking (if needed). Can select all and mark as "Complete". </li> </ul>', USER, SYSDATE);
COMMIT; 
 
 

-- try and edit the error handler to show associated items on the page in appdev
















/*
SELECT *
FROM shttran 
ORDER BY shttran_activity_date DESC
;

SELECT * 
FROM shttrtc
;

SELECT * 
FROM stvtprt
;

SELECT * 
FROM GENERAL.GTVPRNT
;

  \*  CURSOR c_prints IS*\ SELECT rq.rq_pidm pidm, 
                            ZEXEC.ZBTM_TERM.f_get_current_term_code(p_pidm => rq.rq_pidm) term
                       FROM TABLE(apex_string.split('24180,24168,24169,24171,24170,24177,24179,23596',',')) --TABLE(apex_string.split(p_request_ids,','))
                       JOIN ztranscript_request.destinations ds
                         ON ds.ds_id = COLUMN_VALUE
                       JOIN ztranscript_request.requests rq 
                         ON rq.rq_id = ds.ds_rq_id
;

*/
