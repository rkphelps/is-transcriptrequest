BEGIN
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.sfrstcr');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvrsts'); 
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','GENERAL.GTVZIPC');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','ZAPEX.WWV_FLOW_FILE_OBJECTS$');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zgeneral.zgrmime');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvlevl');
 -- sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','ZAPEX.WWV_FLOW_FILE_OBJECTS$'); 
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zgeneral.zgrmime');
 -- sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','');
END;
/
--inserts/updates for the delivery methods (to account for US non-states)
INSERT INTO ztranscript_request.delivery_method (dm_code,
                                                 dm_desc,
                                                 dm_display_order,
                                                 dm_active_ind,
                                                 dm_email_ind,
                                                 dm_college_ind,
                                                 dm_address_ind, 
                                                 dm_comments_ind,
                                                 dm_attachment_ind,
                                                 dm_exp_ship_ind,
                                                 dm_data_origin,
                                                 dm_created_by,
                                                 dm_created_on)
       VALUES ('USTR', 'US Mail (Non-States)', 70, 'Y', 'N', 'N', 'Y', 'N', 'Y', 'N', 'DEPLOY', USER, SYSDATE);

INSERT INTO ztranscript_request.text (tx_item,
                                      tx_description,
                                      tx_text,
                                      tx_last_activity_user,
                                      tx_last_activity)
       VALUES ('DM_USTR_INSTRUCTIONS', 'Istructions - Adding US Mail (Non-States) Destination', '<p>Please enter a complete and valid U.S. territoy or APO address. This type of request is not eligible for Expedited Shipping. </p>', USER, SYSDATE);

UPDATE ztranscript_request.delivery_method dm
   SET dm.dm_desc = 'US Mail (States)'
 WHERE dm.dm_code = 'USML';

UPDATE ztranscript_request.delivery_method dm
   SET dm.dm_desc = 'International Mail'
 WHERE dm.dm_code = 'INML';

--inserts for status definitions
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('PP_STATUS_DEF','Status Definition for Pending Payment','You have begun the request, but it is incomplete. Please view the request, edit it (if necessary), and complete it by going through all four steps. ',USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('PC_STATUS_DEF','Status Definition for In Processing','You have completed and paid for this request. The Registrar''s Office is processing your request, which usually takes 1-2 business days. ',USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('WP_STATUS_DEF','Status Definition for Waiting for Pickup','The Registrar''s Office has processed your request, and it is waiting to be picked up. Please visit the Student Service Center during business hours to pick up your transcript. ',USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('CN_STATUS_DEF','Status Definition for Cancelled','Your request was cancelled. It will not be processed. ',USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('CM_STATUS_DEF','Status Definition for Completed','Your request has been processed and sent to the destination(s) you selected. If you asked for a transcript to be mailed, please allow for shipping times. ',USER, SYSDATE);

--delete old logs above the threshold and old requests due to query changes
DELETE ztranscript_request.log log
   WHERE log.lg_activity < (SYSDATE - 28) --c_delete_log_days)
     AND log.lg_level > 10
     ;

DELETE ztranscript_request.requests r
 WHERE r.rq_id NOT IN (SELECT d.ds_rq_id
                         FROM ztranscript_request.destinations d)
   AND r.rq_st_code = 'PC';

--inserts for requiring a .pdf file for uploads
INSERT INTO ztranscript_request.text (tx_item,
                                      tx_description,
                                      tx_text,
                                      tx_last_activity_user,
                                      tx_last_activity)
       VALUES ('P5_ATTACHMENT_INLINE_HELP','Inline Help Text for "Attachment" field','All Uploads must be a .pdf',USER, SYSDATE);

UPDATE ztranscript_request.text t
   SET t.tx_text = 'If there is a document you need Liberty to send with your transcript, please upload it. You can only upload one document, and it must be a .pdf. If you need multiple items sent with your transcripts, please merge them into one .pdf file before uploading. You can search online for instructions on converting/printing a file into a .pdf. If you''ve already uploaded one document and you upload a second, the second upload will override the first, and the first will be deleted.'
 WHERE t.tx_item = 'P5_ATTACHMENT_HELP';

INSERT INTO ztranscript_request.text (tx_item,
                                      tx_description,
                                      tx_text,
                                      tx_last_activity_user,
                                      tx_last_activity)
       VALUES ('PDF_UPLOAD_ERROR','Error Message for requiring uploads to be a .pdf','All Uploads must be a .pdf. Please convert your file to a .pdf, then return here and upload it.',USER, SYSDATE);

UPDATE ztranscript_request.text t
   SET t.tx_item = 'P15_COMMENTS_HELP',
       t.tx_text = 'You may enter any final comments here.'
 WHERE t.tx_item = 'P3_COMMENTS_HELP';

--editing certain text items based on form updates
UPDATE ztranscript_request.text t
   SET t.tx_text = 'Please confirm that all of the below information is correct before finalizing your request. Due to a fee waiver, you do not have to pay for your transcripts. If anything needs to be edited, click "Step 2: Destinations" to return to the previous page, but make sure to come back here once you''ve added all destinations to send your request to the Registrar''s Office.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>'
 WHERE t.tx_item = 'P15_INSTRUCTIONS_NO_PAYMENT';
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P15_3_STEPS_TITLE','Title for Step 3 without payment','Step 3 of 3: Confirmation',USER,SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P15_4_STEPS_TITLE','Title for Step 3 with payment','Step 3 of 4: Confirmation',USER,SYSDATE);
UPDATE ztranscript_request.text t
   SET t.tx_text = 'Please enter a full mailing address, as well as a city, state, zip, and nation.'
 WHERE t.tx_item ='NULL_ADDRESS_ERROR';
UPDATE ztranscript_request.text t
   SET t.tx_text = 'Here you may enter additional comments about this request that you weren''t able to enter on a previous step. After you enter them, please click "Save Comments" before doing anything else.'
 WHERE t.tx_item ='P15_COMMENTS_HELP';
--allows toggling the fee waiver for military spouses on and off
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('MILITARY_SPOUSE_WAIVER','Transcript Fee Waiver for Military Spouses', 'N',USER,SYSDATE);
--document must be uploaded error
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('DOC_UPLOAD_NO_DOC','No Document Found', 'There was an attempt to upload a document, but no document was found. Please try again.',USER,SYSDATE);
--valid file types (for attachments)
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('VALID_FILE_TYPES','Valid File Types for Attachments', 'Please format any valid file types in upper-case, with a hyphen before and after the file type. -PDF-', USER, SYSDATE);
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P5_PDF_UPLOAD_ERROR','Invalid File Type Error', 'You have uploaded an invalid file type. Please upload a .pdf file. ', USER, SYSDATE);
--valid level codes for the form
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('INVALID_LEVEL_CODES','Invalid Level Codes for Students', 'Please format any invalid level codes (ones that the RO doesn''t process transcripts for) in upper-case, with a hyphen before and after the level code. -AC- -MD- -K8- -HS-', USER, SYSDATE);
UPDATE ztranscript_request.delivery_method dm 
   SET dm.dm_comments_ind = 'N';
COMMIT;

