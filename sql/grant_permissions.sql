
BEGIN
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC');
  --UPay grant permissions to package
  sys.dba_assist.GRANT_OBJ_PERMS(p_schema_name => 'ZUPAY_SVC', p_perm => 'EXECUTE',p_object => 'ZTRANSCRIPT_REQUEST.APEX_APP_LOGIC');
END;
/


