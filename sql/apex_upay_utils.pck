create or replace package zexec.apex_upay_utils is

  type upay_response_type is record(
    tpg_trans_id                 varchar2(1000),
    pmt_status                   varchar2(1000),
    pmt_amt                      varchar2(1000),
    pmt_date                     varchar2(1000),
    name_on_acct                 varchar2(1000),
    acct_addr                    varchar2(1000),
    acct_addr2                   varchar2(1000),
    acct_city                    varchar2(1000),
    acct_state                   varchar2(1000),
    acct_zip                     varchar2(1000),
    acct_country                 varchar2(1000),
    acct_email_address           varchar2(1000),
    acct_phone_day               varchar2(1000),
    acct_phone_night             varchar2(1000),
    acct_phone_mobile            varchar2(1000),
    ext_trans_id                 varchar2(1000),
    upay_site_id                 varchar2(1000),
    sys_tracking_id              varchar2(1000),
    card_type                    varchar2(1000),
    bank_name                    varchar2(1000),
    bank_addr1                   varchar2(1000),
    bank_addr2                   varchar2(1000),
    bank_routing_num             varchar2(1000),
    recurring_end_date           varchar2(1000),
    recurring_start_date         varchar2(1000),
    recurring_number_of_payments varchar2(1000));

  function f_encode(p_vk in varchar2, p_eti in varchar2, p_amt in number)
    return varchar2;
  --take this out after it's tested

  function f_build_upay_form(p_appid                  in number,
                             p_ext_trans_id           in varchar2,
                             p_email                  in varchar2,
                             p_amount                 in number default null,
                             p_bill_name              in varchar2 default null,
                             p_bill_street1           in varchar2 default null,
                             p_bill_street2           in varchar2 default null,
                             p_bill_city              in varchar2 default null,
                             p_bill_state             in varchar2 default null,
                             p_bill_postal_code       in varchar2 default null,
                             p_bill_country           in varchar2 default null,
                             p_recur_include          in varchar2 default 'N',
                             p_recur_user_can_change  in varchar2 default 'Y',
                             p_recur_change_end_to_cc in varchar2 default 'N',
                             p_recur_start_date       in date default null,
                             p_recur_end_date         in date default null,
                             p_recur_frequency        in varchar2 default null,
                             p_recur_num_payments     in number default null, 
                             p_detail_code_1          IN VARCHAR2 DEFAULT NULL,  --if either p_detail_code_1 or p_detail_code_1_amt is added, they must both be added. This is true for all of the parameters for detail codes. 
                             p_detail_code_1_amt      IN NUMBER   DEFAULT NULL,
                             p_detail_code_2          IN VARCHAR2 DEFAULT NULL, 
                             p_detail_code_2_amt      IN NUMBER   DEFAULT NULL, 
                             p_detail_code_3          IN VARCHAR2 DEFAULT NULL, 
                             p_detail_code_3_amt      IN NUMBER   DEFAULT NULL)       
                             --further detail codes and amounts can be added as form parameters for the upay form, but are not accounted for in this function at this time. 
  
   return varchar2;

  function f_new_external_tran_id return varchar2;

  function f_upay_response(p_response_in in varchar2)
    return upay_response_type;

end apex_upay_utils;
/
create or replace package body zexec.apex_upay_utils is

  -- stuff local to this package

  -- this function encodes a key for the transaction.  it is coded to follow the specification outlined in touchnet's
  -- documentation on their UPAY API. Don't mess with it unless touchnet has specified that it needs to change 
  -- which i can't imagine would ever happen.
  function f_encode(p_vk in varchar2, p_eti in varchar2, p_amt in number)
    return varchar2 is
  
    l_hash    varchar2(100);
    l_raw     raw(100);
    l_encoded varchar2(100);
  
  begin
  
    select standard_hash(p_vk || p_eti || to_char(p_amt), 'MD5')
      into l_hash
      from dual;
  
    select hextoraw(l_hash) into l_raw from dual;
  
    l_encoded := utl_raw.cast_to_varchar2(utl_encode.base64_encode(l_raw));
  
    return l_encoded;
  
  end;

  --------------------------------------------------------------
  --this function simply returns the proper UPAY url - test or prod - depending on whic banner instance we're on.
  function f_upay_url return varchar2 is
  
    --created august 2017 by ben moomaw
    --determines high order part of the url for static files, based on which database we're running on.
  
    global_name varchar2(4000);
  
  begin
  
    select global_name into global_name from global_name;
  
    if global_name like '%PROD%' then
      return 'https://secure.touchnet.net:443/C20965_upay/web/index.jsp';
    
    else
      return 'https://test.secure.touchnet.net:8443/C20965test_upay/web/index.jsp';
    
    end if;
  
  end;

  -- exposed stuff
  ----------------------------------------------------------------
  function f_new_external_tran_id return varchar2 is
  begin
    return to_char(zexec.apex_upay_utils_seq.nextval);
  end;

  function f_build_upay_form(p_appid                  in number,
                             p_ext_trans_id           in varchar2,
                             p_email                  in varchar2,
                             p_amount                 in number   default null,
                             p_bill_name              in varchar2 default null,
                             p_bill_street1           in varchar2 default null,
                             p_bill_street2           in varchar2 default null,
                             p_bill_city              in varchar2 default null,
                             p_bill_state             in varchar2 default null,
                             p_bill_postal_code       in varchar2 default null,
                             p_bill_country           in varchar2 default null,
                             p_recur_include          in varchar2 default 'N',
                             p_recur_user_can_change  in varchar2 default 'Y',
                             p_recur_change_end_to_cc in varchar2 default 'N',
                             p_recur_start_date       in date     default null,
                             p_recur_end_date         in date     default null,
                             p_recur_frequency        in varchar2 default null,
                             p_recur_num_payments     in number   default null, 
                             p_detail_code_1          IN VARCHAR2 DEFAULT NULL,  --if either p_detail_code_1 or p_detail_code_1_amt is added, they must both be added. This is true for all of the parameters for detail codes. 
                             p_detail_code_1_amt      IN NUMBER   DEFAULT NULL,
                             p_detail_code_2          IN VARCHAR2 DEFAULT NULL, 
                             p_detail_code_2_amt      IN NUMBER   DEFAULT NULL, 
                             p_detail_code_3          IN VARCHAR2 DEFAULT NULL, 
                             p_detail_code_3_amt      IN NUMBER   DEFAULT NULL)       
                             --further detail codes and amounts can be added as form parameters for the upay form, but are not accounted for in this function at this time. 
  
   return varchar2 is
  
    l_upay_config zupay.upay_application%rowtype;
  
    l_upay_url    varchar2(100) := f_upay_url;
    l_upay_form   varchar2(4000);
    l_encoded_key varchar2(100);
  
  begin
  
    select *
      into l_upay_config
      from zupay.upay_application a
     where a.app_id = p_appid;
  
    l_upay_form := '<form id="upay_form" action="' || l_upay_url ||
                   '" method="post">'; -- will come from a function for test vs prod.
  
    --add the amount and the validation key if it has been provided.  
    if p_amount is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="AMT" value="' ||
                     to_char(p_amount) || '" />'; -- will be passed in by the calling apex app.
      --encode a validation key, per marketplace users guide.
      l_encoded_key := f_encode(p_vk  => l_upay_config.validation_key,
                                p_eti => p_ext_trans_id,
                                p_amt => p_amount);
      l_upay_form   := l_upay_form ||
                       '<input type="hidden" name="VALIDATION_KEY" value="' ||
                       l_encoded_key || '" />'; -- will be passed in by the calling apex app.
    end if;
  
    --put required stuff on
    l_upay_form := l_upay_form ||
                   '<input type="hidden" name="UPAY_SITE_ID" value="' ||
                   l_upay_config.upay_site_id || '" />'; -- will be passed in by the calling apex app.
    l_upay_form := l_upay_form ||
                   '<input type="hidden" name="EXT_TRANS_ID_LABEL" value="Liberty Tran ID" />';
    l_upay_form := l_upay_form ||
                   '<input type="hidden" name="EXT_TRANS_ID" value="' ||
                   p_ext_trans_id || '" />'; -- comes from caller.
    l_upay_form := l_upay_form ||
                   '<input type="hidden" name="BILL_EMAIL_ADDRESS" value="' ||
                   p_email || '" />';
  
    --now, add stuff that can be added from the pay_application configuration
    if l_upay_config.success_link_text is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="SUCCESS_LINK_TEXT" value="' ||
                     l_upay_config.success_link_text || '" />'; --comes from the config for the app
    end if;
  
    if l_upay_config.success_link is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="SUCCESS_LINK" value="' ||
                     l_upay_config.success_link || '" />';
    end if;
  
    if l_upay_config.cancel_link_text is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="CANCEL_LINK_TEXT" value="' ||
                     l_upay_config.cancel_link_text || '" />';
    end if;
  
    if l_upay_config.cancel_link is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="CANCEL_LINK" value="' ||
                     l_upay_config.cancel_link || '" />';
    end if;
  
    --now, add recurring payment stuff if necessary
    if upper(p_recur_include) = 'Y' then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_USER_CAN_CHANGE" value="' || case
                       when coalesce(p_recur_user_can_change, 'Y') = 'Y' then
                        'True'
                       else
                        'False'
                     end || '" />'; -- comes from caller.
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_CAN_CHANGE_END_DATE_TO_CC_EXP_DATE" value="' || case
                       when coalesce(p_recur_change_end_to_cc, 'Y') = 'Y' then
                        'Yes'
                       else
                        'No'
                     end || '" />'; -- comes from caller.
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_START_DATE" value="' ||
                     to_char(p_recur_start_date, 'mm/dd/yyyy') || '" />'; -- comes from caller.
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_END_DATE" value="' ||
                     to_char(p_recur_end_date, 'mm/dd/yyyy') || '" />'; -- comes from caller.
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_FREQUENCY" value="' ||
                     p_recur_frequency || '" />'; -- comes from caller.
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="RECURRING_NUMBER_OF_PAYMENTS" value="' ||
                     p_recur_num_payments || '" />'; -- comes from caller.
    
    end if;
  
    --add additional optional billing stuff if passed by the caller
  
    if p_bill_name is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_NAME" value="' ||
                     p_bill_name || '" />';
    end if;
  
    if p_bill_street1 is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_STREET1" value="' ||
                     p_bill_street1 || '" />';
    end if;
  
    if p_bill_street2 is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_STREET2" value="' ||
                     p_bill_street2 || '" />';
    end if;
  
    if p_bill_city is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_CITY" value="' ||
                     p_bill_city || '" />';
    end if;
  
    if p_bill_state is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_STATE" value="' ||
                     p_bill_state || '" />';
    end if;
  
    if p_bill_postal_code is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_POSTAL_CODE" value="' ||
                     p_bill_postal_code || '" />';
    end if;
  
    if p_bill_country is not null then
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="BILL_COUNTRY" value="' ||
                     p_bill_country || '" />';
    end if;

    --if additional information is needed to pass through (to separate payments into different accounts), add it here

    IF p_detail_code_1 IS NOT NULL AND p_detail_code_1_amt IS NOT NULL THEN
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="CREDIT_ACCT_AMT" value="' ||
                     p_detail_code_1_amt || '" />' ||
                     '<input type="hidden" name="CREDIT_ACCT_CODE" value="' ||
                     p_detail_code_1 || '" />';
    END IF;

    IF p_detail_code_2 IS NOT NULL AND p_detail_code_2_amt IS NOT NULL THEN
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="CREDIT_ACCT_AMT_2" value="' ||
                     p_detail_code_2_amt || '" />' ||
                     '<input type="hidden" name="CREDIT_ACCT_CODE_2" value="' ||
                     p_detail_code_2 || '" />';
    END IF;

    IF p_detail_code_3 IS NOT NULL AND p_detail_code_3_amt IS NOT NULL THEN
      l_upay_form := l_upay_form ||
                     '<input type="hidden" name="CREDIT_ACCT_AMT_2" value="' ||
                     p_detail_code_3_amt || '" />' ||
                     '<input type="hidden" name="CREDIT_ACCT_CODE_2" value="' ||
                     p_detail_code_3 || '" />';
    END IF;
  
    l_upay_form := l_upay_form || '</form>';
  
    --return null;
    return l_upay_form;
    
  end;

  function f_upay_response(p_response_in in varchar2)
    return upay_response_type is
  
    l_response upay_response_type;
    l_id       number;
  
    --pragma autonomous_transaction;
  
  begin
    insert into zupay.upay_json
      (json)
    values
      (p_response_in)
    returning id into l_id;
    select json_value(json, '$.TPG_TRANS_ID'),
           json_value(json, '$.PMT_STATUS'),
           json_value(json, '$.PMT_AMT'),
           json_value(json, '$.PMT_DATE'),
           json_value(json, '$.NAME_ON_ACCT'),
           json_value(json, '$.ACCT_ADDR'),
           json_value(json, '$.ACCT_ADDR2'),
           json_value(json, '$.ACCT_CITY'),
           json_value(json, '$.ACCT_STATE'),
           json_value(json, '$.ACCT_ZIP'),
           json_value(json, '$.ACCT_COUNTRY'),
           json_value(json, '$.ACCT_EMAIL_ADDRESS'),
           json_value(json, '$.ACCT_PHONE_DAY'),
           json_value(json, '$.ACCT_PHONE_NIGHT'),
           json_value(json, '$.ACCT_PHONE_MOBILE'),
           json_value(json, '$.EXT_TRANS_ID'),
           json_value(json, '$.UPAY_SITE_ID'),
           json_value(json, '$.SYS_TRACKING_ID'),
           json_value(json, '$.CARD_TYPE'),
           json_value(json, '$.BANK_NAME'),
           json_value(json, '$.BANK_ADDR1'),
           json_value(json, '$.BANK_ADDR2'),
           json_value(json, '$.BANK_ROUTING_NUM'),
           json_value(json, '$.RECURRING_SETUP_END_DATE'),
           json_value(json, '$.RECURRING_SETUP_START_DATE'),
           json_value(json, '$.RECURRING_SETUP_NUMBER_OF_PAYMENTS')
    
      into l_response.tpg_trans_id,
           l_response.pmt_status,
           l_response.pmt_amt,
           l_response.pmt_date,
           l_response.name_on_acct,
           l_response.acct_addr,
           l_response.acct_addr2,
           l_response.acct_city,
           l_response.acct_state,
           l_response.acct_zip,
           l_response.acct_country,
           l_response.acct_email_address,
           l_response.acct_phone_day,
           l_response.acct_phone_night,
           l_response.acct_phone_mobile,
           l_response.ext_trans_id,
           l_response.upay_site_id,
           l_response.sys_tracking_id,
           l_response.card_type,
           l_response.bank_name,
           l_response.bank_addr1,
           l_response.bank_addr2,
           l_response.bank_routing_num,
           l_response.recurring_end_date,
           l_response.recurring_start_date,
           l_response.recurring_number_of_payments
    
      from zupay.upay_json j
     where j.id = l_id;
  
    delete from zupay.upay_json x where x.id = l_id;
  
    return l_response;
  
  end;

end apex_upay_utils;
/
