SET DEFINE OFF;
ALTER SESSION SET PLSQL_WARNINGS='DISABLE:ALL';
set serveroutput on
--run the updates to be able to print
@printing_object_changes.sql;
@escrip_inserts.sql;
SHOW ERRORS;
--rerun the package
@app_logic.pck;
SHOW ERRORS;
PROMPT
PROMPT --------------- sql deploy complete --------------
PROMPT
