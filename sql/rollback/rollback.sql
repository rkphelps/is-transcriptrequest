
INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
       VALUES ('P5_DELIVERY_METHOD_HELP','Destination Type Instructions','Please select a Destination Type and wait for the page to reload. You will then be prompted to fill in other fields based on this selection.',USER,SYSDATE);

UPDATE ztranscript_request.status_changes sc
   SET sc.sc_email_ind = 'Y'
 WHERE sc.sc_prev_sc_code = 'PP'
   AND sc.sc_new_sc_code = 'PC';
   
UPDATE ztranscript_request.text t
   SET t.tx_item = 'PC_EMAIL_TEXT'
 WHERE t.tx_item = 'WT_EMAIL_TEXT';
 
DELETE ztranscript_request.status_changes sc
 WHERE sc.sc_prev_sc_code = 'WT'
    OR sc.sc_new_sc_code = 'WT';

DELETE ztranscript_request.statuses s
 WHERE s.st_code = 'WT';
 
DELETE ztranscript_request.text t
 WHERE t.tx_item IN ('Primary Printer','Secondary Printer','Tertiary Printer','P5_STU_EMAIL_UT_NOTIF','NON_ESCRIP_SCHOOL_ERROR',
                     'NO_ESCRIP_DEPT_ERROR','P5_ESCRIP_AVAIL_NOTIF','P5_ESCRIP_HELP','DM_ESCP_INSTRUCTIONS','P2_MD_HOLD_MESSAGE',
                     'P200_INSTRUCTIONS','P200_INCORRECT_ID','P200_NOT_ACCEPTED','P200_NO_CLASSES','P200_NO_COMPLETED_TERM',
                     'P200_NO_GRAD_APP','WT_STATUS_DEF','PC_WAIT_HOURS','DM_NULL_INSTRUCTIONS','P910_INSTRUCTIONS','',
                     '','','','','','','','','','','','');

DELETE ztranscript_request.delivery_method
 WHERE dm_code = 'ESCP';
 
ALTER TABLE ztranscript_request.delivery_method DROP CONSTRAINT dm_escrip_ind_ck;
ALTER TABLE ztranscript_request.delivery_method DROP COLUMN dm_escrip_ind;
ALTER TABLE ztranscript_request.delivery_method DROP CONSTRAINT dm_tprt_code_fk;
ALTER TABLE ztranscript_request.delivery_method DROP COLUMN dm_tprt_code;
ALTER TABLE ztranscript_request.destinations DROP CONSTRAINT ds_el_id_fk;
ALTER TABLE ztranscript_request.destinations DROP COLUMN ds_el_id;
ALTER TABLE ztranscript_request.requests DROP CONSTRAINT rq_prnt_id_fk;
ALTER TABLE ztranscript_request.requests DROP COLUMN rq_prnt_id;
--ALTER TABLE ztranscript_request.requests DROP COLUMN rq_levl_code;

DROP TABLE ztranscript_request.escrip_locations;
DROP TABLE ztranscript_request.prints;

COMMIT;
