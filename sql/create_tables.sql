

BEGIN
  sys.dba_assist.GRANT_OBJ_PERMS('ZSATURN','REFERENCES','saturn.stvnatn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZSATURN','REFERENCES','saturn.stvstat');
END;
/

--create a crosswalk table between stvstat and stvnatn
CREATE TABLE zsaturn.zsttntn
(zsttntn_surrogate_id   INT             GENERATED ALWAYS AS IDENTITY,
 zsttntn_stat_code      VARCHAR2(3)     CONSTRAINT  zsttntn_stat_code_nn NOT NULL,
 zsttntn_natn_code      VARCHAR2(5)     CONSTRAINT  zsttntn_natn_code_nn NOT NULL,
 zsttntn_state_ind      CHAR(1)         CONSTRAINT  zsttntn_state_ind_nn NOT NULL,
 zsttntn_data_origin    VARCHAR2(50)    CONSTRAINT  zsttntn_data_origin_nn NOT NULL,
 zsttntn_created_by     VARCHAR2(30)    CONSTRAINT  zsttntn_created_by_nn NOT NULL,
 zsttntn_created_on     TIMESTAMP       CONSTRAINT  zsttntn_created_on_nn NOT NULL,
 zsttntn_modified_by    VARCHAR2(30),
 zsttntn_modified_on    TIMESTAMP,
 CONSTRAINT zsttntn_stat_natn_pk        PRIMARY KEY (zsttntn_stat_code,zsttntn_natn_code),
 CONSTRAINT zsttntn_surrogate_id_unq    UNIQUE      (zsttntn_surrogate_id),
 CONSTRAINT zsttntn_stat_code_fk        FOREIGN KEY (zsttntn_stat_code) REFERENCES saturn.stvstat(stvstat_code),
 CONSTRAINT zsttntn_natn_code_fk        FOREIGN KEY (zsttntn_natn_code) REFERENCES saturn.stvnatn(stvnatn_code), 
 CONSTRAINT zsttntn_state_ind_ck        CHECK       (zsttntn_state_ind IN ('Y','N')));

BEGIN
--US
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AK','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AL','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AR','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AZ','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('CA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('CO','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('CT','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('DC','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('DE','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('FL','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('GA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('HI','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('IA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('ID','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('IL','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('IN','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('KS','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('KY','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('LA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MD','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('ME','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MI','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MN','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MO','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MS','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MT','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NC','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('ND','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NE','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NH','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NJ','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NM','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NV','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NY','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('OH','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('OK','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('OR','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('PA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('RI','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('SC','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('SD','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('TN','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('TX','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('UT','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('VA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('VT','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('WA','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('WV','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('WI','US','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('WY','US','Y','DEPLOY',USER,SYSDATE);
      -- US territories
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AS','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('GU','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MP','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('CM','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('PR','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('VI','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('CZ','US','N','DEPLOY',USER,SYSDATE);
      -- Military
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AA','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AE','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AP','US','N','DEPLOY',USER,SYSDATE);
      --Other US Associated locations
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MH','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('PW','US','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('FM','US','N','DEPLOY',USER,SYSDATE);
--canadian
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('AB','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('BC','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('MB','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NB','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NF','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NS','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('ON','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('PE','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('PQ','CA','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('SK','CA','Y','DEPLOY',USER,SYSDATE);
      --canadian territories
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NT','CA','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NU','CA','N','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('YT','CA','N','DEPLOY',USER,SYSDATE);
      
--australia
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('NSW','AU','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('QLD','AU','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('SA', 'AU','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('TAS','AU','Y','DEPLOY',USER,SYSDATE);
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('VIC','AU','Y','DEPLOY',USER,SYSDATE);
      --western australia isn't listed in stvstat?
      --australian territories
      INSERT INTO zsaturn.zsttntn (zsttntn_stat_code, zsttntn_natn_code, zsttntn_state_ind, zsttntn_data_origin, zsttntn_created_by, zsttntn_created_on)
             VALUES ('ACT','AU','N','DEPLOY',USER,SYSDATE);
  COMMIT;
END;
/

BEGIN
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select','zsaturn.zsttntn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','select','zsaturn.zsttntn');
END;
/

BEGIN
INSERT INTO saturn.stvsbgi (stvsbgi_code, stvsbgi_type_ind, stvsbgi_srce_ind, stvsbgi_desc, stvsbgi_activity_date, stvsbgi_admr_code)
       VALUES ('111111','C','Y','Not Listed',SYSDATE, 'CT1');
END;
/

BEGIN  
  UPDATE sobsbgi
     set sobsbgi_natn_code = 'US'
       , sobsbgi_user_id = 'APEX'  -- in case we need to identify the ones we updated
   where sobsbgi_stat_code in ('AE','AP','GU','PR','VI','AK','AL','AR','AZ','CA','CO','CT','DC','DE','FL','GA','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MI','MN','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VA','VT','WA','WI','WV','WY') 
     and (sobsbgi_natn_code is null 
      or sobsbgi_natn_code <> 'US') ;
END;
/

CREATE TABLE ZTRANSCRIPT_REQUEST.statuses
(st_code                 VARCHAR2(4)        CONSTRAINT st_code_pk              PRIMARY KEY,
 st_desc                 VARCHAR2(30)       CONSTRAINT st_desc_nn              NOT NULL,
 st_in_processing_ind    VARCHAR2(1)        CONSTRAINT st_in_processing_ind_nn NOT NULL,
 st_final_ind            VARCHAR2(1)        CONSTRAINT st_final_ind_nn         NOT NULL,
 st_delete_ind           VARCHAR2(1)        CONSTRAINT st_delete_ind_nn        NOT NULL,
 st_can_edit_ind         VARCHAR2(1)        CONSTRAINT st_can_edit_ind_nn      NOT NULL,
 st_data_origin          VARCHAR2(50)       CONSTRAINT st_data_origin_nn       NOT NULL,
 st_created_by           VARCHAR2(30)       CONSTRAINT st_created_by_nn        NOT NULL,
 st_created_on           TIMESTAMP          CONSTRAINT st_created_on_nn        NOT NULL,
 st_modified_by          VARCHAR2(30),
 st_modified_on          TIMESTAMP,
 CONSTRAINT st_in_processing_ind_ck         CHECK (st_in_processing_ind IN ('Y','N')),
 CONSTRAINT st_final_ind_ck                 CHECK (st_final_ind IN ('Y','N')),
 CONSTRAINT st_delete_ind_ck                CHECK (st_delete_ind IN ('Y','N'))
);

--insert trigger (for created by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.statuses_insert_trigger
BEFORE INSERT ON ZTRANSCRIPT_REQUEST.statuses
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.st_created_by,USER),
                SYSDATE
           INTO :new.st_created_by,
                :new.st_created_on
           FROM dual;
       END;
/

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.statuses_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.statuses
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.st_modified_by,USER),
                SYSDATE
           INTO :new.st_modified_by,
                :new.st_modified_on
           FROM dual;
       END;
/

BEGIN
--inserts for the status table
  INSERT INTO ZTRANSCRIPT_REQUEST.statuses (ST_CODE, ST_DESC, ST_IN_PROCESSING_IND, ST_FINAL_IND, ST_DELETE_IND,
                                            ST_CAN_EDIT_IND, ST_DATA_ORIGIN, ST_CREATED_BY, ST_CREATED_ON) 
         VALUES ('PP','Pending Payment','N','N','Y','Y','DEPLOYMENT',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.statuses (ST_CODE, ST_DESC, ST_IN_PROCESSING_IND, ST_FINAL_IND, ST_DELETE_IND,
                                            ST_CAN_EDIT_IND, ST_DATA_ORIGIN, ST_CREATED_BY, ST_CREATED_ON) 
         VALUES ('PC','In Processing','Y','N','N','N','DEPLOYMENT',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.statuses (ST_CODE, ST_DESC, ST_IN_PROCESSING_IND, ST_FINAL_IND, ST_DELETE_IND,
                                            ST_CAN_EDIT_IND, ST_DATA_ORIGIN, ST_CREATED_BY, ST_CREATED_ON) 
         VALUES ('WP','Waiting for Pickup','Y','N','N','N','DEPLOYMENT',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.statuses (ST_CODE, ST_DESC, ST_IN_PROCESSING_IND, ST_FINAL_IND, ST_DELETE_IND,
                                            ST_CAN_EDIT_IND, ST_DATA_ORIGIN, ST_CREATED_BY, ST_CREATED_ON) 
         VALUES ('CM','Completed','N','Y','N','N','DEPLOYMENT',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.statuses (ST_CODE, ST_DESC, ST_IN_PROCESSING_IND, ST_FINAL_IND, ST_DELETE_IND,
                                            ST_CAN_EDIT_IND, ST_DATA_ORIGIN, ST_CREATED_BY, ST_CREATED_ON) 
         VALUES ('CN','Cancelled','N','Y','N','N','DEPLOYMENT',USER,SYSDATE);
END;
/

CREATE TABLE ZTRANSCRIPT_REQUEST.status_changes
(sc_id                   INT                GENERATED ALWAYS AS IDENTITY,
 sc_prev_sc_code         VARCHAR2(4)        CONSTRAINT sc_prev_sc_code_nn      NOT NULL,
 sc_new_sc_code          VARCHAR2(4)        CONSTRAINT sc_new_sc_code_nn       NOT NULL,
 sc_email_ind            VARCHAR2(1)        CONSTRAINT sc_email_ind_nn         NOT NULL,
 sc_mark_processor_ind   VARCHAR2(1)        CONSTRAINT sc_mark_processor_ind_nn NOT NULL,
 sc_data_origin          VARCHAR2(50)       CONSTRAINT sc_data_origin_nn       NOT NULL,
 sc_created_by           VARCHAR2(30)       CONSTRAINT sc_created_by_nn        NOT NULL,
 sc_created_on           TIMESTAMP          CONSTRAINT sc_created_on_nn        NOT NULL,
 sc_modified_by          VARCHAR2(30),
 sc_modified_on          TIMESTAMP,
 CONSTRAINT sc_pk                           PRIMARY KEY (sc_id),
 CONSTRAINT sc_prev_sc_code_fk              FOREIGN KEY (sc_prev_sc_code)      REFERENCES ZTRANSCRIPT_REQUEST.statuses (st_code),
 CONSTRAINT sc_new_sc_code_fk               FOREIGN KEY (sc_new_sc_code)       REFERENCES ZTRANSCRIPT_REQUEST.statuses (st_code),
 CONSTRAINT sc_email_ind_ck                 CHECK (sc_email_ind IN ('Y','N')),
 CONSTRAINT sc_prev_new_sc_code_unq         UNIQUE (sc_prev_sc_code, sc_new_sc_code),
 CONSTRAINT sc_mark_processor_ind_ck        CHECK (sc_mark_processor_ind IN ('Y','N'))
);

--insert trigger (for created by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.status_changes_insert_trigger
BEFORE INSERT ON ZTRANSCRIPT_REQUEST.status_changes
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.sc_created_by,USER),
                SYSDATE
           INTO :new.sc_created_by,
                :new.sc_created_on
           FROM dual;
       END;
/

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.status_changes_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.status_changes
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.sc_modified_by,USER),
                SYSDATE
           INTO :new.sc_modified_by,
                :new.sc_modified_on
           FROM dual;
       END;
/

BEGIN
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PP','PC','Y','N','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PP','PP','N','N','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PC','WP','Y','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PC','CM','Y','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PC','PC','N','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('PC','CN','N','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('WP','CM','N','N','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('WP','WP','N','N','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('CM','PC','N','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('CM','CM','N','N','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('CN','PC','N','Y','DEPLOY',USER,SYSDATE);
  INSERT INTO ZTRANSCRIPT_REQUEST.status_changes (SC_PREV_SC_CODE, SC_NEW_SC_CODE, SC_EMAIL_IND, SC_MARK_PROCESSOR_IND, SC_DATA_ORIGIN, SC_CREATED_BY, SC_CREATED_ON)
         VALUES ('CN','CN','N','N','DEPLOY',USER,SYSDATE);
END;
/

CREATE TABLE ZTRANSCRIPT_REQUEST.delivery_method
(dm_code                 VARCHAR2(4)        CONSTRAINT dm_code_pk              PRIMARY KEY,
 dm_desc                 VARCHAR2(30)       CONSTRAINT dm_desc_nn              NOT NULL,
 dm_display_order        INT,
 dm_limit_per_request    INT,
 dm_copies_per_dest      INT,
 dm_active_ind           VARCHAR2(1)        CONSTRAINT dm_active_ind_nn        NOT NULL,
 dm_email_ind            VARCHAR2(1)        CONSTRAINT dm_email_ind_nn         NOT NULL,
 dm_college_ind          VARCHAR2(1)        CONSTRAINT dm_college_ind_nn       NOT NULL,
 dm_address_ind          VARCHAR2(1)        CONSTRAINT dm_address_ind_nn       NOT NULL,
 dm_comments_ind         VARCHAR2(1)        CONSTRAINT dm_comments_ind_nn      NOT NULL,
 dm_attachment_ind       VARCHAR2(1)        CONSTRAINT dm_attachment_ind_nn    NOT NULL,
 dm_exp_ship_ind         VARCHAR2(1)        CONSTRAINT dm_exp_ship_ind_nn      NOT NULL,
 dm_cost_override        NUMBER(4,2),
 dm_data_origin          VARCHAR2(50)       CONSTRAINT dm_data_origin_nn       NOT NULL,
 dm_created_by           VARCHAR2(30)       CONSTRAINT dm_created_by_nn        NOT NULL,
 dm_created_on           TIMESTAMP          CONSTRAINT dm_created_on_nn        NOT NULL,
 dm_modified_by          VARCHAR2(30),
 dm_modified_on          TIMESTAMP,
 CONSTRAINT dm_email_ind_ck                 CHECK (dm_email_ind IN ('Y','N')),
 CONSTRAINT dm_college_ind_ck               CHECK (dm_college_ind IN ('Y','N')),
 CONSTRAINT dm_address_ind_ck               CHECK (dm_address_ind IN ('Y','N')),
 CONSTRAINT dm_comments_ind_ck              CHECK (dm_comments_ind IN ('Y','N')),
 CONSTRAINT dm_attachment_ind_ck            CHECK (dm_attachment_ind IN ('Y','N')),
 CONSTRAINT dm_exp_ship_ind_ck              CHECK (dm_exp_ship_ind IN ('Y','N'))
);

--insert trigger (for created by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.dm_insert_trigger
BEFORE INSERT ON ZTRANSCRIPT_REQUEST.delivery_method
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.dm_created_by,USER),
                SYSDATE
           INTO :new.dm_created_by,
                :new.dm_created_on
           FROM dual;
       END;
/

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.dm_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.delivery_method
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.dm_modified_by,USER),
                SYSDATE
           INTO :new.dm_modified_by,
                :new.dm_modified_on
           FROM dual;
       END;
/

BEGIN
--inserts for the delivery methods table
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('COLL','College via Mail',10,NULL,NULL,'Y','N','Y','Y','Y','Y','Y',NULL, 'DEPLOY');
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('USML','US Mail',20,NULL,NULL,'Y','N','N','Y','Y','Y','Y',NULL, 'DEPLOY');
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('EMAL','Non-LU E-mail',30,NULL,1,'Y','Y','Y','N','Y','Y','N',NULL, 'DEPLOY');
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('INML','International Mail',50,NULL,NULL,'Y','N','N','Y','Y','Y','N',NULL, 'DEPLOY');
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('PKUP','Pickup on Campus',60,1,NULL,'Y','N','N','N','Y','N','N',NULL, 'DEPLOY');
  INSERT INTO ztranscript_request.delivery_method (dm_code, dm_desc, dm_display_order, dm_limit_per_request, dm_copies_per_dest, dm_active_ind, 
              dm_email_ind, dm_college_ind, dm_address_ind, dm_comments_ind, dm_attachment_ind, dm_exp_ship_ind, dm_cost_override, dm_data_origin)
         VALUES ('LBTY','LU E-mail',40,NULL,1,'Y','Y','N','N','Y','N','N',0,'DEPLOY');
END;
/

CREATE TABLE ZTRANSCRIPT_REQUEST.requests
(rq_id                   INT                GENERATED ALWAYS AS IDENTITY,
 rq_pidm                 NUMBER(8)          CONSTRAINT rq_pidm_nn NOT NULL,
 rq_seq_no               INT                CONSTRAINT rq_seq_no_nn NOT NULL,
 rq_st_code              VARCHAR2(50)       CONSTRAINT rq_status_nn NOT NULL,
 rq_description          VARCHAR2(100)      CONSTRAINT rq_description_nn NOT NULL,
 rq_email                VARCHAR2(100)      CONSTRAINT rq_email_nn NOT NULL,
 rq_comments             VARCHAR2(1000),
 rq_cost                 NUMBER(10,2),
 rq_cost_override_ind    VARCHAR2(1),
 rq_upay_trans_id        VARCHAR2(100)      CONSTRAINT rq_upay_trans_id_unq UNIQUE,
 rq_processor            VARCHAR2(50),
 rq_ro_comments          VARCHAR2(1000),
 rq_data_origin          VARCHAR2(50)       CONSTRAINT rq_data_origin_nn NOT NULL,
 rq_created_by           VARCHAR2(30)       CONSTRAINT rq_created_by_nn NOT NULL,
 rq_created_on           TIMESTAMP          CONSTRAINT rq_created_on_nn NOT NULL,
 rq_modified_by          VARCHAR2(30),
 rq_modified_on          TIMESTAMP,
 CONSTRAINT rq_pk                           PRIMARY KEY (rq_id),
 CONSTRAINT rq_st_code_fk                   FOREIGN KEY (rq_st_code) REFERENCES ZTRANSCRIPT_REQUEST.statuses (st_code),
 CONSTRAINT rq_cost_override_ind_ck         CHECK (rq_cost_override_ind IN ('Y','N'))
);

CREATE INDEX ZTRANSCRIPT_REQUEST.rq_pidm_indx ON ZTRANSCRIPT_REQUEST.requests (rq_pidm);
CREATE UNIQUE INDEX ZTRANSCRIPT_REQUEST.rq_pidm_seq_no_indx ON ZTRANSCRIPT_REQUEST.requests (rq_pidm, rq_seq_no);
CREATE UNIQUE INDEX ZTRANSCRIPT_REQUEST.rq_pidm_desc_indx ON ZTRANSCRIPT_REQUEST.requests (rq_pidm, rq_description);


--insert trigger (for created by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.requests_insert_trigger
BEFORE INSERT ON ZTRANSCRIPT_REQUEST.requests
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.rq_created_by,USER),
                SYSDATE
           INTO :new.rq_created_by,
                :new.rq_created_on
           FROM dual;
       END;
/

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.requests_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.requests
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.rq_modified_by,USER),
                SYSDATE
           INTO :new.rq_modified_by,
                :new.rq_modified_on
           FROM dual;
       END;
/

CREATE TABLE ztranscript_request.destinations
(ds_id                   INT                GENERATED ALWAYS AS IDENTITY,
 ds_rq_id                INT                CONSTRAINT ds_rq_id_nn NOT NULL,
 ds_seq_no               INT                CONSTRAINT ds_seq_no_nn NOT NULL,
 ds_dm_code              VARCHAR2(100)      CONSTRAINT ds_dm_code_nn NOT NULL,
 ds_email_address        VARCHAR2(128),
 ds_sbgi_code            VARCHAR2(6),
 ds_addr_name            VARCHAR2(125),
 ds_addr_line1           VARCHAR2(75),
 ds_addr_line2           VARCHAR2(75),
 ds_addr_city            VARCHAR2(50),
 ds_addr_stat_code       VARCHAR2(3),
 ds_addr_zip             VARCHAR2(30),
 ds_addr_natn_code       VARCHAR2(5),
 ds_copies_requested     INT                CONSTRAINT ds_copies_requested_nn NOT NULL,
 ds_doc_id               INT,
 ds_comments             VARCHAR2(1000),
 ds_exp_ship_ind         VARCHAR2(1),
 ds_data_origin          VARCHAR2(50)       CONSTRAINT ds_data_origin_nn NOT NULL,
 ds_created_by           VARCHAR2(30)       CONSTRAINT ds_created_by_nn NOT NULL,
 ds_created_on           TIMESTAMP          CONSTRAINT ds_created_on_nn NOT NULL,
 ds_modified_by          VARCHAR2(30),
 ds_modified_on          TIMESTAMP,
 CONSTRAINT ds_pk                           PRIMARY KEY (ds_id),
 CONSTRAINT ds_rq_id_fk                     FOREIGN KEY (ds_rq_id)          REFERENCES ZTRANSCRIPT_REQUEST.requests (rq_id),
 CONSTRAINT ds_dm_code_fk                   FOREIGN KEY (ds_dm_code)        REFERENCES ZTRANSCRIPT_REQUEST.DELIVERY_METHOD (DM_CODE),
 CONSTRAINT ds_sbgi_code_fk                 FOREIGN KEY (ds_sbgi_code)      REFERENCES saturn.stvsbgi (stvsbgi_code),
 CONSTRAINT ds_addr_stat_code_fk            FOREIGN KEY (ds_addr_stat_code) REFERENCES saturn.stvstat (stvstat_code),
 CONSTRAINT ds_addr_natn_code_fk            FOREIGN KEY (ds_addr_natn_code) REFERENCES saturn.stvnatn (stvnatn_code),
 CONSTRAINT ds_copies_requested_ck          CHECK (ds_copies_requested BETWEEN 1 AND 1000),
 CONSTRAINT ds_doc_id_fk                    FOREIGN KEY (ds_doc_id)         REFERENCES zgeneral.zgrdocm (zgrdocm_id),
 CONSTRAINT ds_exp_ship_ind_ck              CHECK (ds_exp_ship_ind IN ('Y','N'))
);

CREATE INDEX ZTRANSCRIPT_REQUEST.ds_rq_id_indx ON ZTRANSCRIPT_REQUEST.destinations (ds_rq_id);
CREATE UNIQUE INDEX ZTRANSCRIPT_REQUEST.ds_rq_id_seq_no_indx ON ZTRANSCRIPT_REQUEST.destinations (ds_rq_id,ds_seq_no);
CREATE INDEX ZTRANSCRIPT_REQUEST.ds_sbgi_code_indx ON ZTRANSCRIPT_REQUEST.destinations (ds_sbgi_code);
CREATE INDEX ZTRANSCRIPT_REQUEST.ds_addr_stat_code_indx ON ZTRANSCRIPT_REQUEST.destinations (ds_addr_stat_code);
CREATE INDEX ZTRANSCRIPT_REQUEST.ds_addr_natn_code_indx ON ZTRANSCRIPT_REQUEST.destinations (ds_addr_natn_code);

--insert trigger (for created by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.ds_insert_trigger
BEFORE INSERT ON ZTRANSCRIPT_REQUEST.destinations
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.ds_created_by,USER),
                SYSDATE
           INTO :new.ds_created_by,
                :new.ds_created_on
           FROM dual;
       END;
/

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.ds_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.destinations
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.ds_modified_by,USER),
                SYSDATE
           INTO :new.ds_modified_by,
                :new.ds_modified_on
           FROM dual;
       END;
/ 

CREATE TABLE ztranscript_request.text
(tx_item                 VARCHAR2(50)       CONSTRAINT tx_item_nn NOT NULL,
 tx_description          VARCHAR2(1000)     CONSTRAINT tx_description_nn NOT NULL,
 tx_text                 VARCHAR2(4000),
 tx_number               NUMBER(10,2),
 tx_last_activity_user   VARCHAR2(50)       CONSTRAINT tx_last_activity_user_nn NOT NULL,
 tx_last_activity        TIMESTAMP          CONSTRAINT tx_last_activity_nn NOT NULL,
 CONSTRAINT tx_pk                           PRIMARY KEY (tx_item),
 CONSTRAINT tx_value_ck                     CHECK (tx_text IS NOT NULL OR tx_number IS NOT NULL) --one of these values must be there
);

--update trigger (for modified by/on)
CREATE OR REPLACE TRIGGER ZTRANSCRIPT_REQUEST.tx_update_trigger
BEFORE UPDATE ON ZTRANSCRIPT_REQUEST.text
   FOR EACH ROW
       BEGIN 
         SELECT nvl(:new.tx_last_activity_user,USER),
                SYSDATE
           INTO :new.tx_last_activity_user,
                :new.tx_last_activity
           FROM dual;
       END;
/

COMMENT ON TABLE ztranscript_request.text IS 'Table that holds the text for apex form 1214.';
COMMENT ON COLUMN ztranscript_request.text.tx_item IS 'Column that stores the item name wihtin the APEX application that will hold the text/value when the app is loaded. If the item name starts with P1000, that''s an piece of text that is not used within the form, but in other places throughout the workflow (like an e-mail).';

CREATE TABLE ztranscript_request.log
(lg_id                   INT                GENERATED ALWAYS AS IDENTITY,
 lg_level                INT,
 lg_action               VARCHAR2(4000),
 lg_session              NUMBER,
 lg_user                 VARCHAR2(50),
 lg_activity             TIMESTAMP,
 CONSTRAINT lg_pk                           PRIMARY KEY (lg_id)
);

BEGIN
--item value inserts
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P1_INSTRUCTIONS','Instructions - Home Page','Welcome to the Transcript Request Form. please select from the menu options at the left. If you go into the actual transcript request form, the side menu will disappear. Nonetheless, you can always get back to the Home Page by clicking the "Transcript Request" logo in the top-left corner.', USER, SYSDATE);
  
  --student home (2)
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P2_INSTRUCTIONS','Instructions - Student Home Page','Welcome to the Transcript Request Form. If this is your first time in the form, you can click "New Request" to start a new transcript request form. You can return to this page at any time by clicking "Transcript Request" in the upper-left corner. <p> After creating a request, you will be able to see it on this page, and you can view the status of your request at any time by returning to this page. <p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P2_NO_REQUESTS_MESSAGE','Unable to Request Message','You are unable to request transcripts at this time. In order to request a transcript, you must have earned a grade in at least one class (excluding Medical School and Academy courses), and you must not have an transcript hold on your account. Please contact the Registrar''s Office if you have any questions.', USER, SYSDATE);
  
  --create new request page (3)
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P3_INSTRUCTIONS','Instructions - Create Request','This is the first of four steps. Please give your request a title, confirm/enter your e-mail address, and add any comments that the Registrar''s Office needs to see (if any). Then click "Step 2: Destinations" to continue. If you have any questions, please click the question mark in a circle beside any field that needs to be completed. <p> Please note that Liberty University requires payment for the transcript(s) before the Registrar''s Office begins processing your request.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P3_REQ_NAME_HELP','Help Text for "Request Name" field','Please enter a name for your request, such as "Law School Applications" or "Job Interview". If you have completed a request previously, please note that this name must be unique. The automated notification e-mails will use this name when notifying you. If you wish to leave it blank, it will default to "Request 1", or whatever number request this is for you.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P3_COMMENTS_HELP','Help Text for "Comments" field','If needed, please enter any comments for the Registrar''s Office.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P3_EMAIL_HELP','Help Text for "E-mail" field','This e-mail address will be used to update you as your transcripts are being processed, and send you a receipt of your payment. This defaults to your Liberty e-mail address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P3_STU_NAME_HELP','Help Text for "Name to Appear on Transcript" field','This is the name that will appear on the transcript. If this needs to be changed, please send Liberty legal documentation that your name has changed and then return here to submit your request.', USER, SYSDATE);
  
  --list of destinations (4)
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P4_INSTRUCTIONS','Instructions - Destination List Page','Any destination(s) you have added will appear below. To add a destination for this request, please click the "Add Destination" button. You can add as many destinations as you would like. <p> Please note that your first transcript will cost $10, and any subsequent transcripts cost $1 apiece.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>', USER, SYSDATE);
  
  --add/edit destination (5)
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_INSTRUCTIONS','Instructions - Add Destination page','Please select the Delivery Method, then fill out the required fields.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>', USER, SYSDATE);
-----------which of these five is used is determined by the Delivery Method selected. Only one will populate
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_EMAL_INSTRUCTIONS','Instructions - Adding E-mail Destination','Please enter an e-mail address. The Registrar''s Office will send a secure e-mail to this address with your official transcripts attached. If the e-mail is forwarded or if the attachment is printed, the transcript will be considered unofficial from that point forward. If this e-mail address ends in ".edu", you will be required to select a college.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_PKUP_INSTRUCTIONS','Instructions - Adding Pickup Copy','Please enter the number of transcripts you would like for the Registrar''s Office to print for you to pickup. When this is complete, you will receive an e-mail to your Liberty e-mail address notifying you that they are ready for pickup. You will be able to pick them up in the Student Service Center on campus between 8 am and 5pm, Monday through Friday (closed Wednesdays for convocation from 10:15 am to noon).', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_COLL_INSTRUCTIONS','Instructions - Adding College Destination','Please select the state your college is in, then select your college. They are listed alphabetically, and once in the drop-down, you can start typing your college''s name to jump to your college. After that, the college''s address will auto-populate. The parts of it that populate are not editable. If Liberty University doesn''t have the college''s address on file, or only part of it, please enter it.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_USML_INSTRUCTIONS','Instructions - Adding US Mail Destination','Please enter a complete and valid US mailing address. Please note that Liberty University cannot sent transcripts to an APO or PO Box address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_INML_INSTRUCTIONS','Instructions - Adding International Mail Destination','Please enter a complete and valid International mailing address. Please note that Liberty University cannot sent transcripts to an APO or P.O. Box address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DM_LBTY_INSTRUCTIONS','Instructions - Adding Liberty - Internal Destination','Please enter a complete and valid e-mail address ending with "@liberty.edu". Since this is an internal request, there will be no charge for this destination.', USER, SYSDATE);
---------------------------------- 
 INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_EXP_SHIP','Expedited Shipping Explanation','Expedited Shipping is an additional fee of $20 per mailing address in addition to the transcript fee.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_EXP_SHIP_LABEL','Expedited Shipping Label','Expedited FedEx Shipping', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_INVALID_ADDRESS_MESSAGE','Message displayed when Invalid Address entered','You have entered either an APO or a PO Box address. Liberty''s transcript request policy does not allow sending to these types of addresses. Please enter a different address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_DELIVERY_METHOD_HELP','Help Text for "Delivery Method" field','Please select a Delivery Method. You will then be prompted to complete more fields based on what you selected.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_EMAIL_ADDR_HELP','Help Text for "E-mail Address" field','Please enter a valid e-mail address. Liberty will send an e-mail to this address with an official transcript attached to the e-mail. Please note that if the e-mail is forwarded, the transcript will no longer be considered official.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_SBGI_CODE_HELP','Help Text for "College" field','This is the College that you want to receive the transcript. This list is in alphabetical order. Additionally, after you''ve opened the drop-down menu, you can start typing to jump to a specific school.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ADDR_NAME_HELP','Help Text for "Address Name" field','Name of the person or entity to whom the transcript is being sent.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ADDR_LINE1_HELP','Help Text for "Address Line 1" field','Please enter the house number and street name. Note that Liberty cannot send transcripts to APO of PO Box addresses.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ADDR_LINE2_HELP','Help Text for "Address Line 2" field','Please enter any additional part of the address, such as an apartment number. Note that Liberty cannot send transcripts to APO of PO Box addresses.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_CITY_HELP','Help Text for "City" field','Please enter the City of the Address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_STATE_HELP','Help Text for "State" field','If this address is associated with the US, Canada, or Australia, please select the state/province from the drop-down menu. Otherwise, please use "International Address".', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ZIP_HELP','Help Text for "Zip" field','Please enter either the five digit zip code or the nine-digit zip code of the address.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_NATION_HELP','Help Text for "Nation" field','Please select a nation from the drop-down list. If you selected a US mailing address as the Delivery Method, this will be auto-selected for you. If you do not know what nation this is being sent to, you can select "Unknown", but Liberty may or may not be able to complete your request.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_COPIES_HELP','Help Text for "Copies Requested" field','Please enter the amount of copies you want to be delivered to this location. You must enter at least 1. If this field is not editable, that means this delivery method has a set amount of copies that must be requested for it.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_COMMENTS_HELP','Help Text for "Comments" field','If there are any additional stipulations on how the transcript should be delivered, please enter them.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ATTACHMENT_HELP','Help Text for "Attachment" field','If there is an additional document you need Liberty to send with your transcript, please attach it. You can only attach one document, so if you need multiple items sent, please merge them into one before uploading. If you''ve already uploaded one document and you upload a second, the second upload will override the first, and the first will be deleted.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_EXP_SHIP_HELP','Help Text for "Expedited Shipping" field','If you would like for Liberty to use Expedited Shipping to send the transcripts, please select that. Further, expedited shipping is only available for US street addresses since we use FedEx to ship. Lastly, FedEx shipping is not available for APO or P.O. Box addresses.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_COLL_STAT_HELP','Help Text for "College State" field','Please select the State of your college here to filter down the "College" drop-down menu below.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P5_ADD_ANOTHER_DEST_HELP','Help Text for "Add Another Destination" field','If you are adding another destination after this one, please click this box. When you create/save this destination, you''ll be prompted to enter another. If you''re not entering another destination, leave this unchecked. You can always return to this page by clicking the "Add Destination" button on step 2.', USER, SYSDATE);

  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('EMAIL_ADDR_ERROR','Error Message for Invalid "E-mail Address" field','You must enter an e-mail address and it must be valid. It should be in the format of "XXX@XXX.XXX".', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('NULL_SBGI_CODE_ERROR','Error Message for "SBGI Code" field','You must select a college from the drop-down menu.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('EXP_SHIP_ERROR','Error Message for Expedited Shipping - Invalid Address','Liberty cannot send transcripts via expedited shipping to a non-US address, nor can we send them to a PO Box or an APO address. Please change your address or deselect the expedited shipping option.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('NULL_ADDRESS_ERROR','Error Message for entering an APO or PO Box Address (unless it''s a college)','Please enter at least one line for your mailing address, as well as a city, state, zip, and nation.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('NULL_COPIES_REQUESTED_ERROR','Error Message for not entering a value for "Copies Requested"','You must enter the amount of copies you would like to be sent.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('NO_COLL_EDU_EMAIL','Error Message for .edu e-mail without college','Since you indicated that you''re e-mailing another University, please select the University from the drop-down menus (listed alphabetically). If your school isn''t listed below, please select "Not Listed".', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('LIBERTY_EMAIL_ADDR_ERROR','Error Message for incorrect Liberty E-mail','Since you are requesting Liberty move transcripts from one department to another, please enter a valid Liberty e-mail address. It will end in "@liberty.edu".', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('LIBERTY_EMAIL_WRONG_DM_ERROR','Error Message for entering a Liberty E-mail and not selecting Liberty E-mail as the delivery Method','Since you entered a Liberty e-mail address, please copy the e-mail address you entered, select "Liberty E-mail" for your destination method, then paste the e-mail back into the e-mail field.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('COLL_ADDR_AND_DM_NOT_COLL','Error Message for Entering a College address but not selecting a College','Since you entered a college''s address, please select "College" as the Destination Type, then select your college. This will allow Liberty to ensure the correct address is listed and therefore that the transcript gets to the correct location. If your school isn''t listed in the "College" drop-down, please select "Not Listed".', USER, SYSDATE);

  --confirmation (15)
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P15_INSTRUCTIONS_PAYMENT','Instructions - Confirmation Page with Payment','Please confirm that all of the below information is correct before proceeding to pay for your request. If anything needs to be edited, click "Back to Destination" to return to the previous page.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P15_INSTRUCTIONS_NO_PAYMENT','Instructions - Confirmation Page with no Payment','Please confirm that all of the below information is correct before finalizing your request. Due to a fee waiver, you do not have to pay for your transcripts. If anything needs to be edited, click "Step 2: Destinations" to return to the previous page, but make sure to come back here and click "Finalize" once you''ve added all destinations to send your request to the Registrar''s Office.<p> <a href ="http://www.liberty.edu/transcripts" target="_blank"> Questions? </a>', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P15_BILL_INSTRUCTIONS','Instructions - Address on Confirmation Page','If you want to use the address listed below as your billing address, select "Yes" from the drop-down menu, and it will be passed through to the next page. If not, you''ll simply need to enter your address on the next page. Regardless, you''ll be able to edit this on the payment page.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('P15_REQ_COMPLT','Confirmation - Request Complete','Thank you for submitting your request. The Registrar''s Office will begin processing your request within 1-2 business days.', USER, SYSDATE);

--other text inserts
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('PC_EMAIL_TEXT','E-mail when Request is Paid','Thank you for fully submitting the Transcript Request Form. The Registrar''s Office will begin processing your request within one business day.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('WP_EMAIL_TEXT','E-mail when Request is ready for pickup','Your transcript request has been processed, and your transcripts are waiting to be picked up at the Student Service Center in Green Hall between 8:00 am and 5:00 pm, Monday-Friday (closed on Wednesdays for Convocation from 10:15 am to 12:00 pm). Additionally, if you requested delivery via another method, those have been processed as well.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('CM_EMAIL_TEXT','E-mail when Request is complete/all documents sent.','Your transcript request has been fully processed.', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('CALC_COST_ERROR_MESSAGE','Cost Calculation Error Message','There was an error calculating the cost of your transcripts. Please contact the Registrar''s Office as soon as possible to inform them and to order your transcripts. You can call them between 8 am and 5 pm Monday-Friday at (434) 592-5100, or e-mail them (registrar@liberty.edu).', USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_text, tx_last_activity_user, tx_last_activity)
         VALUES ('DUPLICATE_DESC_MESSAGE','Duplicate Description Error Message','You have already created a request with this description. Please enter a different description.', USER, SYSDATE);

--number inserts
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_number, tx_last_activity_user, tx_last_activity)
         VALUES ('EXPEDITED_SHIPPING_COST','Cost of Expedited Shipping',20, USER, SYSDATE); 
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_number, tx_last_activity_user, tx_last_activity)
         VALUES ('FIRST_TRANSCRIPT_COST','Cost of First Transcript',10, USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_number, tx_last_activity_user, tx_last_activity)
         VALUES ('SUBSEQUENT_TRANSCRIPTS_COST','Cost of Subsequent Transcripts',1, USER, SYSDATE);
  INSERT INTO ztranscript_request.text (tx_item, tx_description, tx_number, tx_last_activity_user, tx_last_activity)
         VALUES ('DELETE_UNPAID_REQSTS_DAYS','Days Unpaid Requests go before being deleted',7, USER, SYSDATE);
  COMMIT;
END;
/
--delete ztranscript_request.text
BEGIN
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ztranscript_request.requests');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ztranscript_request.statuses');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ztranscript_request.destinations');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ztranscript_request.delivery_method');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ZTRANSCRIPT_REQUEST.text');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select, insert, update, delete','ztranscript_request.status_changes');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','select','ztranscript_request.log');
END;
/

COMMENT ON TABLE ZTRANSCRIPT_REQUEST.DELIVERY_METHOD IS 'Validation table for the delivery methods that are available and the parts of the destination form that should be filled out for each type..';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.DESTINATIONS IS 'Repeating table listing the destinations that are part of a request. one row per destination.';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.LOG IS 'Table that logs all actions taken throughout the form.';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.REQUESTS IS 'Base table that contains one row per request.';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.STATUSES IS 'Validation table that shows the different statuses and the significance of each.';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.STATUS_CHANGES IS 'Crosswalk table for showing which status changes are valid. Also contains actions that occur during those status changes.';
COMMENT ON TABLE ZTRANSCRIPT_REQUEST.TEXT IS 'Text that appears throughout the form.';

COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_ACTIVE_IND IS 'indicates whether or not this delivery method is currently an option in the form.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_ADDRESS_IND IS 'indicates whether or not this delivery method should require an address and therefore whether or not it appears for the user.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_ATTACHMENT_IND IS 'indicates whether or not this delivery method should allow an attachment.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_CODE IS 'primary key for this delivery method';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_COLLEGE_IND IS 'indicates whether or not this delivery method prompts the user to select a college.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_COMMENTS_IND IS 'indicates whether or not this delivery method prompts the user to enter comments.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_COPIES_PER_DEST IS 'indicates whether or not this delivery method has a set amount of copies. For instance if a transcript is being e-mailed, it only needs 1 copy because an attachment can be duplicated as needed.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_COST_OVERRIDE IS 'if the cost for this destination has been overridden by the RO, the cost is listed here. It will be a flat rate (if 5 is entered here and the student requests 3 copies be sent, it will be $15).';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_CREATED_BY IS 'user who created this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_CREATED_ON IS 'when this record was created.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_DATA_ORIGIN IS 'source of this record''s origination';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_DESC IS 'description/name of this delivery method (this is what appears in drop-downs in the form)';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_DISPLAY_ORDER IS 'order that this will be displayed in drop-downs (incremented by 10, lower numbers higher up).';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_EMAIL_IND IS 'indicates whether or not this delivery method requires an e-mail address be entered.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_EXP_SHIP_IND IS 'indicates whether or not this delivery method allows selecting expedited shipping.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_LIMIT_PER_REQUEST IS 'the maximum times this delivery method can be selected as part of 1 request.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_MODIFIED_BY IS 'user who last modified this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DELIVERY_METHOD.DM_MODIFIED_ON IS 'time this record was last modified.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_CITY IS 'City of the Address';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_LINE1 IS 'line 1 of the Address';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_LINE2 IS 'line 2 of the Address';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_NAME IS 'recipient of the transcript (for addresses only)';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_NATN_CODE IS 'nation code of the address. foreign key to saturn.stvnatn';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_STAT_CODE IS 'state code of the address. foreign key to saturn.stvstat';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ADDR_ZIP IS 'zip code of the address.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_COMMENTS IS 'any comments that the student entered for this destination, such as instructions or notes for how to send it.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_COPIES_REQUESTED IS 'number of copies to send.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_CREATED_BY IS 'user who created this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_CREATED_ON IS 'when this record was created.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_DATA_ORIGIN IS 'source of this record''s origination.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_DM_CODE IS 'delivery method code for this destination. foreign key to ztranscript_request.delivery_method.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_DOC_ID IS 'document ID of any attachment that should be sent along with the transcript. foreign key to zgeneral.zgrdocm.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_EMAIL_ADDRESS IS 'e-mail address that the transcript should be sent to.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_EXP_SHIP_IND IS 'indicator for whether or not expedited shipping should be used to send the transcript. Only available for US-based, non-PO box and non-APO addresses.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_ID IS 'primary key.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_MODIFIED_BY IS 'user that last modified this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_MODIFIED_ON IS 'time this record was last modified.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_RQ_ID IS 'request which this destination is a part of. foreign key to ztranscript_request.request.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_SBGI_CODE IS 'college code to which this transcript is being sent. foreign key to saturn.stvsbgi.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.DESTINATIONS.DS_SEQ_NO IS 'sequence number as related to the request (DS_RQ_ID). If this is the first destination added to this request, this will be sequence number 1. etc.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_ACTION IS 'description of the action that happened.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_ACTIVITY IS 'date this action occurred.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_ID IS 'primary key for this action.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_LEVEL IS 'level for this action. errors are 10. functions and procedures are 30. sub-processes of functions and procedures are 40. page processes are 60. page loads are 80';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_SESSION IS 'session in which this action occurred.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.LOG.LG_USER IS 'user who performed this action.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_COMMENTS IS 'comments that the student entered for this request.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_COST IS 'cost of this request. Only calculated once someone goes to pay for the transcript. Can be overridden (see rq_cost_override_ind)';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_COST_OVERRIDE_IND IS 'if the cost has been overridden (by the RO), this will be ''Y''.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_CREATED_BY IS 'user who created this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_CREATED_ON IS 'date this record was created.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_DATA_ORIGIN IS 'source of this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_DESCRIPTION IS 'description that the student entered for this request. Must be unique to the student (a person can enter the same description as someone else but they can''t enter the same description twice.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_EMAIL IS 'e-mail address that will be used to communicate with the student while their request is being processed.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_ID IS 'primary key';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_MODIFIED_BY IS 'user who last modified the record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_MODIFIED_ON IS 'when this record was last modified.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_PIDM IS 'pidm of the student requesting their transcript.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_PROCESSOR IS 'username of the person processing the record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_RO_COMMENTS IS 'comments that the RO (normally the processor) entered concerning this request.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_SEQ_NO IS 'the student''s request number. If this is the student''s first request, this will be 1. If their second, it will be 2.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_ST_CODE IS 'status code indicating the current status of this request. Foreign key to ztranscript_request.statuses.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.REQUESTS.RQ_UPAY_TRANS_ID IS 'primary key for the upay_transaction_id';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_CAN_EDIT_IND IS 'indicator for whether or not the student can edit a request while it is in this status.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_CODE IS 'primary key for the status.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_CREATED_BY IS 'user who created this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_CREATED_ON IS 'when this record was created.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_DATA_ORIGIN IS 'source of this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_DELETE_IND IS 'whether or not documents will be deleted in this request. If this in Y, any requests in this status will be deleted after the amount of days indicated in ztranscript_request.text.tx_number where tx_item = ''DELETE_UNPAID_REQSTS_DAYS''.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_DESC IS 'description of this status. This is what appears in the drop-down menus throughout the form. ';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_FINAL_IND IS 'indicates whether or not requests in this status are finished being processed.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_IN_PROCESSING_IND IS 'indicates whether or not requests in this status are currently being processed.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_MODIFIED_BY IS 'user who last modified this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUSES.ST_MODIFIED_ON IS 'when this record was last modified.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_CREATED_BY IS 'user who created this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_CREATED_ON IS 'when this record was created.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_DATA_ORIGIN IS 'source of this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_EMAIL_IND IS 'indicates whether or not this status change should fire off an e-mail to the student. If this status change has already happened for this request, the e-mail will not be sent again.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_ID IS 'primary key';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_MARK_PROCESSOR_IND IS 'indicates whether or not this status change should result in the processor being marked if the processor has not yet been marked.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_MODIFIED_BY IS 'user who last modified this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_MODIFIED_ON IS 'when this record was last modified.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_NEW_SC_CODE IS 'the status code that will be the result of this change. foreign key to ZTRANSCRIPT_REQUEST.REQUESTS.RQ_ST_CODE.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.STATUS_CHANGES.SC_PREV_SC_CODE IS 'the status code that existed before the change. foreign key to ZTRANSCRIPT_REQUEST.REQUESTS.RQ_ST_CODE.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_DESCRIPTION IS 'description of the location where this text is used in the form.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_ITEM IS 'item in the form that stores the text. There isn''t always an item that stores the text, but this is also a primary key.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_LAST_ACTIVITY IS 'when this record was last edited.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_LAST_ACTIVITY_USER IS 'user who last edited this record.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_NUMBER IS 'if a number is being stored instead of text, it is stored in this column instead of tx_text.';
COMMENT ON COLUMN ZTRANSCRIPT_REQUEST.TEXT.TX_TEXT IS 'The text that is used in the form is stored in this column.';

COMMENT ON TABLE ZSATURN.ZSTTNTN IS 'crosswalk table between saturn.stvstat and stvnatn. shows which nation the state is in. only one record per unique combination of state and nation code. ';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_CREATED_BY IS 'user who created this record';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_CREATED_ON IS 'date this record was created';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_DATA_ORIGIN IS 'method through which this record was inserted';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_MODIFIED_BY IS 'user who last modified this record';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_MODIFIED_ON IS 'date this record was last modified. ';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_NATN_CODE IS 'Nation code. Foreign key to saturn.stvnatn';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_STATE_IND IS 'State/Province Indicator. If Y, that means the state/province is recognized as such by the nation it is a part of.';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_STAT_CODE IS 'State Code. Foreign key to saturn.stvstat';
COMMENT ON COLUMN ZSATURN.ZSTTNTN.ZSTTNTN_SURROGATE_ID IS 'unique identifier';


/*

SELECT DISTINCT 'COMMENT ON TABLE ' || OWNER || '.' || TABLE_NAME || ' IS '''';'
FROM SYS.ALL_TAB_COLS 
WHERE OWNER = 'ZSATURN'
  and TABLE_NAME = 'ZSTTNTN'
UNION
SELECT 'COMMENT ON COLUMN ' || OWNER || '.' || TABLE_NAME || '.' || COLUMN_NAME || ' IS '''';'
FROM SYS.ALL_TAB_COLS 
WHERE OWNER = 'ZSATURN'
  and TABLE_NAME = 'ZSTTNTN'
ORDER BY column_id

*/
