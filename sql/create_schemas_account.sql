BEGIN
  zexec.apex_utils.basic_sql_deployment_setup(1214,'TRANSCRIPT_REQUEST','ISDEV',TRUE);
--  call apex_instance_admin.add_schema('ISDEV', 'ZTRANSCRIPT_REQUEST_APX');
END;
/

BEGIN
--app permissions
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','ZEXEC.APEX_CAS_REQUEST_SENTRY_F_URL');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.spriden');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.sgbstdn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.spbpers');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvnatn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvstat');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.spraddr');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.sprtele'); 
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','general.goremal');   
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','general.gobtpac');   
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','zgeneral.zgrdocm');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.sobsbgi');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','saturn.stvsbgi');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT, INSERT, UPDATE, DELETE','zgeneral.zgrtext');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.apex_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.apex_global_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.gz_student_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.apex_session_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zluwebapp.lu_web_api');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.gz_documents');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','EXECUTE','zexec.apex_upay_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST_APX','SELECT','ZAPEX.WWV_FLOW_FILE_OBJECTS$');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','REFERENCES','saturn.stvstat');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','REFERENCES','saturn.stvnatn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','REFERENCES','saturn.stvsbgi');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.spriden');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT,INSERT','zcampent.zcrmque');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','general.goremal');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','general.gobtpac');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.sprhold');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.sgrsatt');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.shrtckn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.shrtckg');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.sgbstdn');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvsbgi');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.sobsbgi');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zsaturn.szriext');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.stvstat');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zexec.zsavaddr');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','saturn.spraddr');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zgeneral.activefacultystaff');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zgeneral.currentemployees');  
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','zbtm.terms_by_group_v');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zexec.apex_global_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zluwebapp.college_record_table_type');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zluwebapp.college_record_type');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zluwebapp.lu_web_api');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zexec.gz_documents');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','EXECUTE','zexec.apex_upay_utils');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT, DELETE, UPDATE, INSERT','zgeneral.zgrdocm');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','REFERENCES','zgeneral.zgrdocm');
  sys.dba_assist.GRANT_OBJ_PERMS('ZTRANSCRIPT_REQUEST','SELECT','ZAPEX.WWV_FLOW_FILE_OBJECTS$');
END;
/


/* QUestions
   -Should I hide the expedited shipping option for US addresses only, and not leet them proceed if they enter an APO or PO Box address?
        -what's an APO address (can I see an example)?
   -what holds prevent a transcript request?
   -what holds prevent a transcript from being sent?
   -explain relationship of requests to destinations. should each request have a status, each destination, or both? destinations make more work for me & you, but if it's needed, so be it.
   -are there ever cases where you need to edit the request, such as if the requester entered obviously wrong information and you contacted them before updating it?  
   -do we need to have a status of waiting on pickup for those left for pickup?
   -are admins (who can change the text that appears in the form) and processors two different groups of people?
   -
*/

/*
Notes from meeting with Ben:
Take a look at this https://atlassian.liberty.edu/jira/secure/RapidBoard.jspa?rapidView=713&view=planning&selectedIssue=TR-3 

We�ll go through this so ryan can get started.

Seems like the overview is to have these pages:
1.  Let requesters see all their requests/start a new one. 
2.  Let requesters make a request (multiple pages)
3.  Let Admins see the text that appears in the form
4.  Let Admins edit the text that appears in the form.
 
What about the following pages: 
5.  Let Processors see requests
a.  For diploma reissue, there are three queues: 
i.  in-process (pending-process on home page that deletes any pending request if pending for 7 days)(button to mark a request as paid that does exactly what the postback API does when it marks something as complete)
ii. ready to be fulfilled (paid and we need to send it)
iii.  completed
iv. Ask RO about a �Cancelled� status instead of deleting (and for anything miscellansou)
v. ask RO about a "Waiting for pickup" status for those that need pickup
6.  Let Processors mark requests as complete. 

And the following steps in the workflow
7.  Let the student know once a transcript has been sent
8.  Let the student know where/how to pick up a transcript once it is ready for pickup.

What I�m saying is that it seems incomplete. 

TR-2. What do we need to do as far as confirming/selecting their e-mail address if they want to pick the transcript up?
TR-17/18. Difference b/t US and International Address? We don�t know
TR-27. Do we want to include previous requests? No
TR-8. I don�t quite get this. Can it be fleshed out more. Ben said it�s just a list of the destinations for the request they are viewing. 

*/

/* Plan
   -page 1 stu enters info
         -list of completed and incomplete degrees that will be included
   -page 900 edit text
   -page 910 edit delivery methods
*/
 
